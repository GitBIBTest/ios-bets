//
//  UserSettingsVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "UserSettingsVC.h"

@implementation UserSettingsVC
{
    NSArray *autoLogoutOptions;
    NSUserDefaults *userDefaults;
}

@synthesize oddsDisplayControl, notificationsType1Control, notificationsType2Control, minutesTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    autoLogoutOptions = [userDefaults objectForKey:@"AutoLogoutOptions"];
    
    //configure the pickerview
    minutesPickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    minutesPickerView.delegate = self;
    minutesPickerView.dataSource = self;
    minutesPickerView.showsSelectionIndicator = YES;
    
    UIToolbar *toolBarMinutesPickerView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarMinutesPickerView.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *minutesDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                       target:self
                                                                                       action:@selector(minutesPickerViewDoneTouched:)];
    //place the done button
    [toolBarMinutesPickerView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                                               target:nil
                                                                                                               action:nil],
                                        minutesDoneButton, nil]];
    minutesTextField.inputAccessoryView = toolBarMinutesPickerView;
    
    //configure labels and other views
    self.oddsDisplayLabel.text = LocalizedString(@"odds_display", nil);
    [oddsDisplayControl setTitle:LocalizedString(@"fraction", nil) forSegmentAtIndex:0];
    [oddsDisplayControl setTitle:LocalizedString(@"decimal", nil) forSegmentAtIndex:1];
    [oddsDisplayControl addTarget:self
                                  action:@selector(oddsDisplayControlValueChanged:)
                        forControlEvents:UIControlEventValueChanged];
    self.notificationsLabel.text = LocalizedString(@"notifications", nil);
    [notificationsType1Control setTitle:LocalizedString(@"off", nil) forSegmentAtIndex:0];
    [notificationsType1Control setTitle:LocalizedString(@"on", nil) forSegmentAtIndex:1];
    [notificationsType1Control addTarget:self
                                  action:@selector(notificationsType1ControlValueChanged:)
                        forControlEvents:UIControlEventValueChanged];
    [notificationsType2Control setTitle:LocalizedString(@"off", nil) forSegmentAtIndex:0];
    [notificationsType2Control setTitle:LocalizedString(@"on", nil) forSegmentAtIndex:1];
    [notificationsType2Control addTarget:self
                                  action:@selector(notificationsType2ControlValueChanged:)
                        forControlEvents:UIControlEventValueChanged];
    self.autoLogoutAfterLabel.text = LocalizedString(@"auto-logout_after", nil);
    self.minutesLabel.text = LocalizedString(@"minutes", nil);
    [minutesTextField setInputView:minutesPickerView];
    
    //set views based on user preference
    [minutesPickerView selectRow:[autoLogoutOptions indexOfObject:[userDefaults objectForKey:@"AutoLogoutMinutes"]] inComponent:0 animated:NO];
    minutesTextField.text = [autoLogoutOptions objectAtIndex:[minutesPickerView selectedRowInComponent:0]];
    [oddsDisplayControl setSelectedSegmentIndex:[userDefaults integerForKey:@"OddsDisplay"]];
    [notificationsType1Control setSelectedSegmentIndex:[userDefaults integerForKey:@"NotificationsType1"]];
    [notificationsType2Control setSelectedSegmentIndex:[userDefaults integerForKey:@"NotificationsType2"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //top space
    if (indexPath.row == 0)
    {
        return 12;
    }
    //"notifications" row is just a title without controls
    else if (indexPath.row == 1 || indexPath.row == 3)
    {
        return 30;
    }
    else
    {
        return 50;
    }
}

- (void) oddsDisplayControlValueChanged:(id)sender
{
    if ([userDefaults integerForKey:@"OddsDisplay"] == 1)
    {
        [userDefaults setInteger:0 forKey:@"OddsDisplay"];
    }
    else
    {
        [userDefaults setInteger:1 forKey:@"OddsDisplay"];
    }
}

- (void) notificationsType1ControlValueChanged:(id)sender
{
    if ([userDefaults integerForKey:@"NotificationsType1"] == 1)
    {
        [userDefaults setInteger:0 forKey:@"NotificationsType1"];
    }
    else
    {
        [userDefaults setInteger:1 forKey:@"NotificationsType1"];
    }
}

- (void) notificationsType2ControlValueChanged:(id)sender
{
    if ([userDefaults integerForKey:@"NotificationsType2"] == 1)
    {
        [userDefaults setInteger:0 forKey:@"NotificationsType2"];
    }
    else
    {
        [userDefaults setInteger:1 forKey:@"NotificationsType2"];
    }
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [autoLogoutOptions count];
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [autoLogoutOptions objectAtIndex:row];
}

- (void)minutesPickerViewDoneTouched:(UIBarButtonItem *)sender
{
    minutesTextField.text = [autoLogoutOptions objectAtIndex:[minutesPickerView selectedRowInComponent:0]];
    
    [userDefaults setObject:minutesTextField.text forKey:@"AutoLogoutMinutes"];
    
    //save and dismiss the picker view
    [self.tableView reloadData];
    [minutesPickerView resignFirstResponder];
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

@end
