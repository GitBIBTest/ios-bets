//
//  UserBetsDataArray.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 29/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "BetsModel.h"
#import "UserBetsData.h"

@interface UserBetsDataArray : NSObject

@property (nonatomic,retain) NSMutableArray *userBetsDataArray;

-(void)addUserBetsArray:(UserBetsData *)userBetsDataArray;

@end
