//
//  EditCreditCardVC.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 07/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Macros.h"
#import "UserData.h"

@interface RemoveCreditCardVC : UITableViewController

@property (nonatomic,retain) NSObject *creditCardObject;

@property (nonatomic,retain) IBOutlet UILabel *cardTypeLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardTypeDisplay;
@property (nonatomic,retain) IBOutlet UILabel *cardNumberLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardNumberDisplay;
@property (nonatomic,retain) IBOutlet UILabel *cardCodeLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardCodeDisplay;
@property (nonatomic,retain) IBOutlet UILabel *expirationLabel;
@property (nonatomic,retain) IBOutlet UILabel *expirationDisplay;
@property (nonatomic,retain) IBOutlet UILabel *cardHolderLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardHolderDisplay;
@property (nonatomic,retain) IBOutlet UIButton *deleteCreditCardButton;

@end
