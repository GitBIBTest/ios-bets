//
//  PlaceAllBetsCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 17/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "Localization.h"

@interface PlaceAllBetsCell : UITableViewCell

@property (nonatomic,retain) UILabel *profitLabel;
@property (nonatomic,retain) UILabel *stakeLabel;

@property (nonatomic,retain) UIButton *placeAllBetsButton;

-(void)setTotalPotentialProfit:(NSString *)potentialProfit forStake:(NSString *)totalStake;

@end
