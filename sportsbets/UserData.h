//
//  UserData.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 05/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject

@property (nonatomic,retain) NSString *userName;
@property (nonatomic,retain) NSString *password;
@property (nonatomic,retain) NSString *passwordEncrypted;
@property (nonatomic,retain) NSString *userId;

+(UserData*)getInstance;
+(void)resetUserData;

@end