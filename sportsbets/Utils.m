//
//  Utils.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 21/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "Utils.h"

@implementation Utils

-(int)lineCountForText:(NSString *)text andFont:(UIFont *)font lineWidth:(CGFloat)lineWidth;
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(lineWidth, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    
    return ceil(rect.size.height / font.lineHeight);
}

-(NSDictionary *)urlPathToDictionary:(NSString *)path
{
    //Get the string everything after the :// of the URL.
    NSString *stringNoPrefix = [[path componentsSeparatedByString:@"://"] lastObject];
    //Get all the parts of the url
    NSMutableArray *parts = [[stringNoPrefix componentsSeparatedByString:@"/"] mutableCopy];
    //Make sure the last object isn't empty
    if([[parts lastObject] isEqualToString:@""])[parts removeLastObject];
    
    if([parts count] % 2 != 0)//Make sure that the array has an even number
        return nil;
    
    //We already know how many values there are, so don't make a mutable dictionary larger than it needs to be.
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:([parts count] / 2)];
    
    //Add all our parts to the dictionary
    for (int i=0; i<[parts count]; i+=2) {
        [dict setObject:[parts objectAtIndex:i+1] forKey:[parts objectAtIndex:i]];
    }
    
    //Return an NSDictionary, not an NSMutableDictionary
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end
