//
//  BasketballBetsVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 11/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "BasketballBetsVC.h"

@implementation BasketballBetsVC
{
    NSArray *categories;
    NSArray *tournaments;
    NSString *category;
    NSString *category_uid;
    NSString *tournament_uid;
    NSTimer *basketballBetsTimer;
    NSInteger betsToLoad;
    MBProgressHUD *progressHud;
    BOOL isFirstRun;
    BBBadgeBarButtonItem *badgeBarButton;
}

-(void)viewDidLoad
{
    sport_identifier = 2;
    [[FilterMaster sharedFilterMaster] loadFilterForSport:sport_identifier];
    
    betsToLoad = 10;
    
    self.burgerButton.target = self.revealViewController;
    self.burgerButton.action = @selector(revealToggle:);
    
    self.secondBarButtonItem.target = self;
    self.filterButton.target = self.revealViewController;
    self.filterButton.action = @selector(rightRevealToggle:);
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName:BIBredcolor,
                                                                       NSFontAttributeName:[UIFont fontWithName:@"Montserrat-bold" size:16.0]
                                                                       }];
    
    //show BurgerMenu on the left side after start of the app
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (appDelegate.showBurgerMenu)
    {
        [self.revealViewController revealToggleAnimated:YES];
        appDelegate.showBurgerMenu = NO;
    }
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //initialize pickerView betType
    pickerViewBetType = [[UIPickerView alloc] initWithFrame:CGRectZero];
    pickerViewBetType.tag = 1;
    pickerViewBetType.showsSelectionIndicator = YES;
    pickerViewBetType.dataSource = self;
    pickerViewBetType.delegate = self;
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //set the estimated row height for the tableview
    [self.tableView setEstimatedRowHeight:75.0];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    
    //check for data to show in view
    data_retrievable = 1;
    NSUInteger counter = [BetsObject sharedBetsObject].tournamentArray.count;
    if (counter == 0)
    {
        data_retrievable = 0;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveReloadTableViewNotification:)
                                                 name:@"ReloadTableView"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (IsConnected())
    {
        if (isFirstRun)
        {
            progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            progressHud.labelText = @"Loading matches";
        }
        else
        {
            progressHud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
            progressHud.labelText = @"Loading matches";
        }
    }
    
    //Resize width of Tableview
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.width = [self widthForTableView];
    self.tableView.frame = tableViewFrame;
    
    //load the betslip image with badge
    UIImage *betslipImg = [UIImage imageNamed:@"wettschein"];
    betslipImg = [betslipImg imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 34, 34)];
    [customButton setBackgroundImage:betslipImg forState:UIControlStateNormal];
    [customButton setTintColor:BIBredcolor];
    [customButton addTarget:self action:@selector(bettingSlipPressed:) forControlEvents:UIControlEventTouchUpInside];
    badgeBarButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BettingSlipMatches"];
    NSError *fetchError = nil;
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    badgeBarButton.badgeValue = F(@"%d", (int)[fetchResult count]);
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.filterButton, nil];
    
    //check if user is logged in
    //user is logged in when there are userinformations in the local store
    //for every log out the user informations have to be deleted from the local store
    fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Login"];
    NSArray *login = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    UIButton *secondBarButtonItemView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 35)];
    [secondBarButtonItemView setTitleColor:BIBredcolor forState:UIControlStateNormal];
    secondBarButtonItemView.titleLabel.font = [UIFont fontWithName:@"Montserrat" size:13.0];
    secondBarButtonItemView.titleLabel.numberOfLines = 0;
    secondBarButtonItemView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    UIButton *centerBarButtonItemView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 35)];
    [centerBarButtonItemView setTitleColor:BIBredcolor forState:UIControlStateNormal];
    centerBarButtonItemView.titleLabel.font = [UIFont fontWithName:@"Montserrat" size:13.0];
    
    if (login.count == 0)
    {
        userLoggedIn = NO;
        [secondBarButtonItemView setFrame:CGRectMake(0, 0, 80, 35)];
        [secondBarButtonItemView setTitle:LocalizedString(@"register", nil) forState:UIControlStateNormal];
        [secondBarButtonItemView addTarget:self action:@selector(registerPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [centerBarButtonItemView setTitle:LocalizedString(@"login", nil) forState:UIControlStateNormal];
        [centerBarButtonItemView addTarget:self action:@selector(loginPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (login.count == 1)
    {
        userLoggedIn = YES;
        UserData *userData =[UserData getInstance];
        userData.userName = [[login objectAtIndex:0] valueForKey:@"username"];
        userData.password = [[login objectAtIndex:0] valueForKey:@"password"];
        userData.passwordEncrypted = [[login objectAtIndex:0] valueForKey:@"password_encrypted"];
        userData.userId = [[login objectAtIndex:0] valueForKey:@"id"];
        
        [secondBarButtonItemView setTitle:LocalizedString(@"current_bets", nil) forState:UIControlStateNormal];
        [secondBarButtonItemView addTarget:self action:@selector(currentBetsPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [centerBarButtonItemView setTitle:LocalizedString(@"my_account", nil) forState:UIControlStateNormal];
        [centerBarButtonItemView addTarget:self action:@selector(myAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        NSLog(@"Error: more than 1 users simultaneously logged in");
    }
    self.secondBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:secondBarButtonItemView];
    
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:self.burgerButton, badgeBarButton, nil];
    //self.navigationItem.titleView = centerBarButtonItemView;
    
    //initialize betsService
    betsService = [[BetsService alloc] init];
    betsService.delegate = self;
    betsService.numberOfMatches = betsToLoad;
    [self loadBets];
    
    //reload bets every x seconds
    [self startBetsTimer];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self stopBetsTimer];
}

-(void)loadBets
{
    if (IsConnected())
    {
        if (category_uid == nil)
        {
            [betsService loadBetsForSport:sport_identifier];
        }
        else if (tournament_uid == nil)
        {
            [betsService loadBetsForSport:sport_identifier andForCategory:category_uid];
        }
        else
        {
            [betsService loadBetsForSport:sport_identifier andForTournament:tournament_uid inCategory:category_uid];
        }
    }
    else
    {
        [self.tableView reloadData];
    }
}

-(void)startBetsTimer
{
    [self stopBetsTimer];
    basketballBetsTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(loadBets) userInfo:nil repeats:YES];
}

-(void)stopBetsTimer
{
    [basketballBetsTimer invalidate];
    basketballBetsTimer = nil;
}

-(void)receiveReloadTableViewNotification:(id)sender
{
    category_uid = [FilterMaster sharedFilterMaster].filter_category_uid;
    tournament_uid = [FilterMaster sharedFilterMaster].filter_tournament_uid;
    
    [self loadBets];
    [self startBetsTimer];
}

//called when BetsService finished data load
-(void)betsDownloaded
{
    NSLog(@"basketball betsDownloaded");
    data_retrievable = 1;
    NSUInteger counter = [BetsObject sharedBetsObject].tournamentArray.count;
    if (counter == 0)
    {
        NSLog(@"no bets downloaded");
        data_retrievable = 0;
    }
    [progressHud hide:YES];
    //reload table view to show newly downloaded bets
    [self.tableView reloadData];
}

//------------------------------------------------------------------------------------
//TableView functions
//------------------------------------------------------------------------------------

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger rowsToReturn = 0;
    if (IsConnected())
    {
        rowsToReturn = [BetsObject sharedBetsObject].tournamentArray.count + 2;
        if (!data_retrievable)
        {
            rowsToReturn += 0;
        }
    }
    else
    {
        rowsToReturn = 3;
    }
    return rowsToReturn;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //first section (0) for sport logos and bet type pickerview
    if (section == 0)
    {
        return 2;
    }
    //other cells for viewing bets, with category/tournaments in headercell
    else if (section != [tableView numberOfSections]-1)
    {
        if (IsConnected())
        {
            section = section -1;
            TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[section];
            return [tournamentModel.betsArray count];
        }
        return 1;
    }
    else
    {
        if (IsConnected())
        {
            return 1;
        }
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 0:
                return 50.0;
            case 1:
                return 35.0;
            default:
                return 40.0;
        }
    }
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    //Resize width of Tableview
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.width = [self widthForTableView];
    self.tableView.frame = tableViewFrame;
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            cellIdentifier = @"AllSportsInARowCell";
            AllSportsInARowCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell highlightImageWithTag:sport_identifier];
            
            cell.delegate = self;
            
            return cell;
        }
        else //if (indexPath.row == 1)
        {
            cellIdentifier = @"BetTypeCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (self.pickerViewBetTypeField.text == nil)
            {
                //add another textfield to cell
                //click on textfield shows pickerView for bet types
                //use a textfield of custom class TextFieldNoCursor, to hide the cursor and make it non-editable
                self.pickerViewBetTypeField = [[TextFieldNoCursor alloc]initWithFrame:CGRectMake(WIDTH(self.view)/2 - 60.0f, 10.0f, 120.0f, 20.0f)];
                self.pickerViewBetTypeField.textAlignment = NSTextAlignmentCenter;
                self.pickerViewBetTypeField.text = LocalizedString(@"bet_type0", nil);
                self.pickerViewBetTypeField.backgroundColor = [UIColor lightGrayColor];
                self.pickerViewBetTypeField.textColor = BIBredcolor;
                [self.pickerViewBetTypeField setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
                [cell addSubview:self.pickerViewBetTypeField];
                
                UIToolbar *toolBarBetTypePickerView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
                toolBarBetTypePickerView.barStyle = UIBarStyleBlackOpaque;
                UIBarButtonItem *betTypeDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                   target:self
                                                                                                   action:@selector(pickerViewBetTypeDoneTouched:)];
                //place the done button
                [toolBarBetTypePickerView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                                                           target:nil
                                                                                                                           action:nil],
                                                    betTypeDoneButton, nil]];
                self.pickerViewBetTypeField.inputAccessoryView = toolBarBetTypePickerView;
                
                //show pickerView instead of keyboard
                self.pickerViewBetTypeField.inputView = pickerViewBetType;
            }
            return cell;
        }//shows bets cells
    }
    //shows bets cells
    else if (indexPath.section != [tableView numberOfSections]-1)
    {
        if (IsConnected())
        {
            cellIdentifier = @"BetsCell";
            SportBetsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            //call data from model classes
            TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[indexPath.section -1];
            BetsModel *bets = [[BetsModel alloc]init];
            bets = tournamentModel.betsArray[indexPath.row];
            
            //check if this bet is present on the betslip, and in case highlight the buttons
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BettingSlipMatches"];
            NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"match_id == %@",bets.match_uid];
            [fetchRequest setPredicate:fetchPredicate];
            NSError *fetchError = nil;
            
            NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
            
            [cell.winner1Button setBackgroundColor:BIBredcolor];
            [cell.winner2Button setBackgroundColor:BIBredcolor];
            
            for (NSManagedObject *k in fetchResult) {
                if ([[k valueForKey:@"selected_rate"] isEqualToString:@"1"])
                {
                    [cell.winner1Button setBackgroundColor:[UIColor blackColor]];
                }
                else if ([[k valueForKey:@"selected_rate"] isEqualToString:@"2"])
                {
                    [cell.winner2Button setBackgroundColor:[UIColor blackColor]];
                }
            }
            
            [cell setHasDraw:NO];
            
            [cell.team1Label setText:bets.hometeam];
            [cell.team2Label setText:bets.awayteam];
            
            [cell.team1Label setFont:[UIFont fontWithName:@"Montserrat" size:13.0]];
            [cell.team2Label setFont:[UIFont fontWithName:@"Montserrat" size:13.0]];
            
            [cell.matchdateLabel setText:bets.date];
            
            [cell.winner1Button setTitle:bets.rate1 forState:UIControlStateNormal];
            [cell.winner1Button addTarget:self
                                   action:@selector(winner1ButtonPressed:)
                         forControlEvents:UIControlEventTouchUpInside];
            [cell.winner_noButton setTitle:bets.ratex forState:UIControlStateNormal];
            [cell.winner_noButton addTarget:self
                                     action:@selector(winner_noButtonPressed:)
                           forControlEvents:UIControlEventTouchUpInside];
            [cell.winner2Button setTitle:bets.rate2 forState:UIControlStateNormal];
            [cell.winner2Button addTarget:self
                                   action:@selector(winner2ButtonPressed:)
                         forControlEvents:UIControlEventTouchUpInside];
            
            UIView *separatorLineView = [cell viewWithTag:200];
            if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1)
            {
                [separatorLineView setHidden:YES];
            }
            else
            {
                [separatorLineView setHidden:NO];
            }
            
            return cell;
        }
        else
        {
            cellIdentifier = @"NotReachableCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            [progressHud hide:YES];
            
            return cell;
        }
    }
    else
    {
        if (data_retrievable)
        {
            cellIdentifier = @"LoadMoreCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            UIButton *loadMoreButton = (UIButton *)[cell viewWithTag:10];
            [loadMoreButton setTitle:LocalizedString(@"load_more", nil) forState:UIControlStateNormal];
            [loadMoreButton addTarget:self action:@selector(loadMoreBets) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else
        {
            cellIdentifier = @"NoBetsToDisplayCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            return cell;
        }
    }
}

-(void)loadMoreBets
{
    if (IsConnected())
    {
        betsToLoad += 10;
        betsService.numberOfMatches = betsToLoad;
        
        progressHud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
        progressHud.labelText = @"Loading matches";
        [self loadBets];
    }
    else
    {
        [self.tableView reloadData];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (data_retrievable == 0 || !IsConnected())
        return nil;
    if (section == 0 || section == [tableView numberOfSections]-1)
    {
        return nil;
    }
    
    //show category and tournament in header
    if (category_uid == nil)
    {
        TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[section-1];
        
        NSString *fetched_category_name = tournamentModel.category_name;
        
        // Set label for the header
        UILabel *categoryTournamentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, WIDTH(self.view)-20.0, 20.0)];
        [categoryTournamentLabel setText:F(@"%@: %@", fetched_category_name, tournamentModel.tournament_name)];
        [categoryTournamentLabel setFont:[UIFont fontWithName:@"Montserrat" size:11.0]];
        [categoryTournamentLabel setTextAlignment:NSTextAlignmentCenter];
        [categoryTournamentLabel setBackgroundColor:[UIColor lightGrayColor]];
        
        return categoryTournamentLabel;
    }
    //show just tournament in header
    else
    {
        UILabel *tournamentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, WIDTH(self.view)-20.0, 20.0)];
        TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[section-1];
        [tournamentLabel setText:F(@"%@", tournamentModel.tournament_name)];
        [tournamentLabel setFont:[UIFont fontWithName:@"Montserrat" size:11.0]];
        [tournamentLabel setTextAlignment:NSTextAlignmentCenter];
        [tournamentLabel setBackgroundColor:[UIColor lightGrayColor]];
        
        return tournamentLabel;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 || !IsConnected())
    {
        return 0;
    }
    else if (section != [tableView numberOfSections]-1)
    {
        return 20;
    }
    else
    {
        return 0;
    }
}

//------------------------------------------------------------------------------------
//Picker View Category/Tournament
//------------------------------------------------------------------------------------

#pragma mark - UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger components;
    if (pickerView.tag == 1)
    {
        //download number of components
        components = 3;
    }
    return components;
}

#pragma mark - UIPickerViewDelegate
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //objectAtIndex:row-1 must be picked, accounting for the "All categories" row
    NSString *chosenComponent;
    if (pickerView.tag == 1)
    {
        //download components
        chosenComponent = LocalizedString(F(@"bet_type%d", (int)row), nil);
    }
    return chosenComponent;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 1)
    {
        self.pickerViewBetTypeField.text = [self pickerView:pickerViewBetType titleForRow:row forComponent:0];
    }
}

-(void)pickerViewBetTypeDoneTouched:(UIBarButtonItem *)sender
{
    //dismiss the picker view
    [self.pickerViewBetTypeField resignFirstResponder];
}

#pragma mark - Text Field delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self stopBetsTimer];
    return YES;
}

//------------------------------------------------------------------------------------
//  Actions for Buttons in (Not)LoggedInCell and Header
//------------------------------------------------------------------------------------

-(IBAction)registerPressed:(id)sender
{
    //show RegisterVC
    [self performSegueWithIdentifier:@"registerVC" sender:sender];
}

-(IBAction)loginPressed:(id)sender
{
    //show LoginVC
    [self performSegueWithIdentifier:@"loginVC" sender:sender];
}

-(IBAction)myAccountPressed:(id)sender
{
    //show MyAccountVC
    [self performSegueWithIdentifier:@"myAccountSegue" sender:sender];
}

-(IBAction)currentBetsPressed:(id)sender
{
    //show CurrentBetsVC
    [self performSegueWithIdentifier:@"currentBetsSegue" sender:sender];
}

-(IBAction)bettingSlipPressed:(id)sender
{
    [self performSegueWithIdentifier:@"bettingSlipVC" sender:sender];
}

//------------------------------------------------------------------------------------
//  Actions for Buttons in SportBetsCell
//------------------------------------------------------------------------------------

-(void)winner1ButtonPressed:(UIButton *)button
{
    //get the indexPath to readout the needed data
    UITableViewCell *cell = (UITableViewCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[indexPath.section -1];
    BetsModel *bets = [[BetsModel alloc]init];
    bets = tournamentModel.betsArray[indexPath.row];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BettingSlipMatches"];
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"match_id == %@ AND selected_rate == \"1\"",bets.match_uid];
    [fetchRequest setPredicate:fetchPredicate];
    NSError *fetchError = nil;
    
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    if ([fetchResult count] == 0)
    {
        //store matchdata local in "BettingSlipMatches"
        NSEntityDescription *bettingSlip = [NSEntityDescription entityForName:@"BettingSlipMatches" inManagedObjectContext:self.managedObjectContext];
        NSManagedObject *newBettingSlipMatch = [[NSManagedObject alloc]initWithEntity:bettingSlip insertIntoManagedObjectContext:self.managedObjectContext];
        
        //set attributes for match in betting slip
        [newBettingSlipMatch setValue:bets.match_uid forKey:@"match_id"];
        [newBettingSlipMatch setValue:@"1" forKey:@"selected_rate"];
        [newBettingSlipMatch setValue:bets.rate1 forKey:@"selected_rate_value"];
        // [newBettingSlipMatch setValue: forKey:@"bet_type"];
        [newBettingSlipMatch setValue:bets.awayteam forKey:@"awayteam"];
        [newBettingSlipMatch setValue:bets.hometeam forKey:@"hometeam"];
        [newBettingSlipMatch setValue:bets.date forKey:@"match_date"];
        [self.managedObjectContext save:nil];
        
        //call animation function
        CGFloat xCenterOfButton = button.frame.origin.x + button.frame.size.width/2.0;
        CGFloat yCenterOfButton = [self.tableView rectForRowAtIndexPath:indexPath].origin.y + button.frame.origin.y + button.frame.size.height/2.0;
        [self betButtonAnimateFromPosition:CGPointMake(xCenterOfButton, yCenterOfButton)];
    }
    else
    {
        [self.managedObjectContext deleteObject:[fetchResult firstObject]];
        badgeBarButton.badgeValue = F(@"%d", [badgeBarButton.badgeValue intValue]-1);
    }
    [self.tableView reloadData];
}

-(void)winner_noButtonPressed:(UIButton *)button
{
    //get the indexPath to read out the needed data
    UITableViewCell *cell = (UITableViewCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[indexPath.section -1];
    BetsModel *bets = [[BetsModel alloc]init];
    bets = tournamentModel.betsArray[indexPath.row];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BettingSlipMatches"];
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"match_id == %@ AND selected_rate == \"x\"",bets.match_uid];
    [fetchRequest setPredicate:fetchPredicate];
    NSError *fetchError = nil;
    
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    if ([fetchResult count] == 0)
    {
        //store matchdata local in "BettingSlipMatches"
        NSEntityDescription *bettingSlip = [NSEntityDescription entityForName:@"BettingSlipMatches" inManagedObjectContext:self.managedObjectContext];
        NSManagedObject *newBettingSlipMatch = [[NSManagedObject alloc]initWithEntity:bettingSlip insertIntoManagedObjectContext:self.managedObjectContext];
        
        //set attributes for match in betting slip
        [newBettingSlipMatch setValue:bets.match_uid forKey:@"match_id"];
        [newBettingSlipMatch setValue:@"x" forKey:@"selected_rate"];
        [newBettingSlipMatch setValue:bets.ratex forKey:@"selected_rate_value"];
        // [newBettingSlipMatch setValue: forKey:@"bet_type"];
        [newBettingSlipMatch setValue:bets.awayteam forKey:@"awayteam"];
        [newBettingSlipMatch setValue:bets.hometeam forKey:@"hometeam"];
        [newBettingSlipMatch setValue:bets.date forKey:@"match_date"];
        [self.managedObjectContext save:nil];
        
        //call animation function
        CGFloat xCenterOfButton = button.frame.origin.x + button.frame.size.width/2.0;
        CGFloat yCenterOfButton = [self.tableView rectForRowAtIndexPath:indexPath].origin.y + button.frame.origin.y + button.frame.size.height/2.0;
        [self betButtonAnimateFromPosition:CGPointMake(xCenterOfButton, yCenterOfButton)];
    }
    else
    {
        [self.managedObjectContext deleteObject:[fetchResult firstObject]];
        badgeBarButton.badgeValue = F(@"%d", [badgeBarButton.badgeValue intValue]-1);
    }
    [self.tableView reloadData];
}

-(void)winner2ButtonPressed:(UIButton *)button
{
    //get the indexPath to readout the needed data
    UITableViewCell *cell = (UITableViewCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    TournamentModel *tournamentModel = [BetsObject sharedBetsObject].tournamentArray[indexPath.section -1];
    BetsModel *bets = [[BetsModel alloc]init];
    bets = tournamentModel.betsArray[indexPath.row];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BettingSlipMatches"];
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"match_id == %@ AND selected_rate == \"2\"",bets.match_uid];
    [fetchRequest setPredicate:fetchPredicate];
    NSError *fetchError = nil;
    
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    if ([fetchResult count] == 0)
    {
        //store matchdata local in "BettingSlipMatches"
        NSEntityDescription *bettingSlip = [NSEntityDescription entityForName:@"BettingSlipMatches" inManagedObjectContext:self.managedObjectContext];
        NSManagedObject *newBettingSlipMatch = [[NSManagedObject alloc]initWithEntity:bettingSlip insertIntoManagedObjectContext:self.managedObjectContext];
        
        //set attributes for match in betting slip
        [newBettingSlipMatch setValue:bets.match_uid forKey:@"match_id"];
        [newBettingSlipMatch setValue:@"2" forKey:@"selected_rate"];
        [newBettingSlipMatch setValue:bets.rate2 forKey:@"selected_rate_value"];
        // [newBettingSlipMatch setValue: forKey:@"bet_type"];
        [newBettingSlipMatch setValue:bets.awayteam forKey:@"awayteam"];
        [newBettingSlipMatch setValue:bets.hometeam forKey:@"hometeam"];
        [newBettingSlipMatch setValue:bets.date forKey:@"match_date"];
        [self.managedObjectContext save:nil];
        
        //call animation function
        CGFloat xCenterOfButton = button.frame.origin.x + button.frame.size.width/2.0;
        CGFloat yCenterOfButton = [self.tableView rectForRowAtIndexPath:indexPath].origin.y + button.frame.origin.y + button.frame.size.height/2.0;
        [self betButtonAnimateFromPosition:CGPointMake(xCenterOfButton, yCenterOfButton)];
    }
    else
    {
        [self.managedObjectContext deleteObject:[fetchResult firstObject]];
        badgeBarButton.badgeValue = F(@"%d", [badgeBarButton.badgeValue intValue]-1);
    }
    [self.tableView reloadData];
}

- (void)betButtonAnimateFromPosition:(CGPoint)senderPosition {
    // update badge
    badgeBarButton.badgeValue = F(@"%d", [badgeBarButton.badgeValue intValue]+1);
    
    // create new duplicate image
    UIImageView *starView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wettschein"]];
    [starView setFrame:CGRectMake(senderPosition.x, senderPosition.y, 30, 30)];
    [starView setTintColor:BIBredcolor];
    starView.layer.cornerRadius=5;
    [self.view addSubview:starView];
    
    // begin ---- apply position animation
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.duration=0.3;
    pathAnimation.delegate=self;
    
    // end point is top right of the tableview
    CGPoint endPoint = CGPointMake(WIDTH(self.view)-70, 0);
    
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, starView.frame.origin.x, starView.frame.origin.y);
    CGPathAddCurveToPoint(curvedPath, NULL, endPoint.x, starView.frame.origin.y, endPoint.x, starView.frame.origin.y, endPoint.x, endPoint.y);
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    // end ---- apply position animation
    
    // apply transform animation
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1, 1, 1)]];
    [basic setAutoreverses:NO];
    [basic setDuration:0.3];
    
    [starView.layer addAnimation:pathAnimation forKey:@"curveAnimation"];
    [starView.layer addAnimation:basic forKey:@"transform"];
    
    [starView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.3];
}

//------------------------------------------------------------------------------------
// misc functions
//------------------------------------------------------------------------------------

-(void)sportLogoHasBeenTouched:(NSString *)sportIdentifier
{
    [self performSegueWithIdentifier:F(@"sport%@",sportIdentifier) sender:self];
}

-(CGFloat)widthForTableView
{
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

-(void)tappedOutside:(id)sender
{
    //dismiss the keyboard
    [self startBetsTimer];
    [self.view endEditing:YES];
}

//to retrieve managed object context
-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end