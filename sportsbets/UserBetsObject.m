//
//  UserBetsObject.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserBetsObject.h"

@implementation UserBetsObject

+ (instancetype)sharedUserBetsObject
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^
    {
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

@end
