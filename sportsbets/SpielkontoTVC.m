//
//  SpielkontoViewController.m
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 20/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "SpielkontoTVC.h"

@interface SpielkontoTVC ()
@end

@implementation SpielkontoTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.paymentsSectionLabel.text = LocalizedString(@"payments", nil);
    self.depositLabel.text = LocalizedString(@"deposit", nil);
    self.cashOutLabel.text = LocalizedString(@"cash_out", nil);
    self.depositLimitsLabel.text = LocalizedString(@"limits", nil);
    self.myAccountSectionLabel.text = LocalizedString(@"my_account", nil);
    self.userDataLabel.text = LocalizedString(@"user_data", nil);
    self.changePasswordLabel.text = LocalizedString(@"change_password", nil);
    self.ageVerificationLabel.text = LocalizedString(@"age_verification", nil);
    self.betLimitsLabel.text = LocalizedString(@"bet_limits", nil);
    self.blacklistLabel.text = LocalizedString(@"blacklist_entries", nil);
    self.myBetsSectionLabel.text = LocalizedString(@"my_bets", nil);
    self.currentBetsLabel.text = LocalizedString(@"current_bets", nil);
    self.myBetsLabel.text = LocalizedString(@"all_bets", nil);
    [self.logOutButton setTitle:LocalizedString(@"logout", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    UserData *userData = [UserData getInstance];
    self.nameLabelText.text = userData.userName;

    AccountOverviewService *accountOverviewService = [[AccountOverviewService alloc] init];
    accountOverviewService.delegate = self;

    [accountOverviewService loadAccountBalanceForUser:userData.userId andForPassword:userData.passwordEncrypted];
}

- (void)accountBalanceDownloaded:(NSString *)balance
{
    NSString *currency = @"€";
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setGroupingSeparator:[[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator]];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMinimumFractionDigits:2];
    
    NSDecimalNumber *balanceNumber = [NSDecimalNumber decimalNumberWithString:balance];
    NSDecimalNumber *roundedBalance = [balanceNumber decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
    
    //account balance
    self.balanceLabel.text = F(@"%@ %@", [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[roundedBalance doubleValue]]], currency);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MeinKontoHeaderCell *headerCell = [self.tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];

    return headerCell;
}

- (IBAction)logOutButtonPressed:(id)sender
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Login"];
    NSArray *login = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];

    if (login.count != 0)
    {
        NSLog(@"logging out");
        [self.managedObjectContext deleteObject:login[0]];
        [self.managedObjectContext save:nil];
        [UserData resetUserData];
    }
    else
    {
        NSLog(@"error: user was already logged out");
    }
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    [tableView deselectRowAtIndexPath:newIndexPath animated:YES];
}

@end
