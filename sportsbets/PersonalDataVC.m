//
//  MyAccountVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 14/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "PersonalDataVC.h"

@implementation PersonalDataVC
{
    MBProgressHUD *progressHud;
    UserData *userData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = LocalizedString(@"my_account", nil);

    //configuration of the tableview
    self.username.placeholder = LocalizedString(@"username", nil);
    self.nameTextField.placeholder = LocalizedString(@"name", nil);
    self.surnameTextField.placeholder = LocalizedString(@"surname", nil);
    self.birthdayTextField.placeholder = LocalizedString(@"birthday", nil);
    self.countryTextField.placeholder = LocalizedString(@"country", nil);
    self.cityTextField.placeholder = LocalizedString(@"city", nil);
    self.plzTextField.placeholder = LocalizedString(@"plz", nil);
    self.streetTextField.placeholder = LocalizedString(@"address_street", nil);
    self.houseNrTextField.placeholder = LocalizedString(@"address_housnr", nil);
    self.emailTextField.placeholder = LocalizedString(@"email", nil);
    self.phoneTextField.placeholder = LocalizedString(@"phone", nil);

    //set the datepicker for selecting a birthdate
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];

    [self.birthdayTextField setInputView:datePicker];

    //create a toolbar with a done Button which will be used on all the relative fields
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    doneBarButton.tintColor = BIBredcolor;
    keyboardToolbar.items = @[flexBarButton, doneBarButton];

    //make the "done toolbar" appear above all
    self.birthdayTextField.inputAccessoryView = keyboardToolbar;
    self.plzTextField.inputAccessoryView = keyboardToolbar;
    self.houseNrTextField.inputAccessoryView = keyboardToolbar;
    self.phoneTextField.inputAccessoryView = keyboardToolbar;

    self.nameTextField.delegate = self;
    self.surnameTextField.delegate = self;
    self.streetTextField.delegate = self;
    self.cityTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.phoneTextField.delegate = self;

    //create a new back button named cancel to make it clear to the User that he is not saving the changes when he returns to the previous view
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem = newBackButton;

    //create a right bar button item to save changes and push them in the database
    self.saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save Changes" style:UIBarButtonItemStyleDone target:self action:@selector(Save:)];
    self.saveButton.enabled = true;
    self.navigationItem.rightBarButtonItem = self.saveButton;
    self.navigationItem.title = false;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)yourTextViewDoneButtonPressed
{
    [self.birthdayTextField resignFirstResponder];
    [self.plzTextField resignFirstResponder];
    [self.houseNrTextField resignFirstResponder];
    [self.phoneTextField resignFirstResponder];
    [self.cityTextField resignFirstResponder];
}

-(void)updateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    self.birthdayTextField.text = [dateFormatter stringFromDate:sender.date];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    userData = [UserData getInstance];
    self.username.text = userData.userName;

    UserInfoService *userInfoService = [[UserInfoService alloc] init];
    userInfoService.delegate = self;

    [userInfoService loadUserInfoForUser:userData.userId andForPassword:userData.passwordEncrypted];

    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHud.labelText = @"Loading user info";
}

- (void)userInfoDownloaded:(NSArray *)userInfo
{
    NSLog(@"%@", userInfo);
    self.nameTextField.placeholder = [userInfo valueForKey:@"name"];
    self.surnameTextField.placeholder = [userInfo valueForKey:@"surname"];
    self.birthdayTextField.placeholder = F(@"%@.%@.%@", [userInfo valueForKey:@"birth_day"], [userInfo valueForKey:@"birth_month"], [userInfo valueForKey:@"birth_year"]);
    self.countryTextField.placeholder = [userInfo valueForKey:@"country"];
    self.cityTextField.placeholder = [userInfo valueForKey:@"city"];
    self.plzTextField.placeholder = [userInfo valueForKey:@"postalcode"];
    self.streetTextField.placeholder = [userInfo valueForKey:@"street"];
    self.houseNrTextField.placeholder = [userInfo valueForKey:@"house_number"];
    self.emailTextField.placeholder = [userInfo valueForKey:@"email"];
    self.phoneTextField.placeholder = [userInfo valueForKey:@"phone"];

    [progressHud setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [progressHud setHidden:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

//the function called to push the data with the API
-(IBAction)Save:(id)sender
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"toDo: integrate web service in order to push changes"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];

    [self.navigationController popViewControllerAnimated:YES];
}


//this method is called when the user presses the cancel Button
- (void) back:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Your changes will not be saved"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
