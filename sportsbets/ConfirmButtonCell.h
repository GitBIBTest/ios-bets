//
//  ConfirmButtonCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 06/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmButtonCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UIButton *confirmButton;

@end
