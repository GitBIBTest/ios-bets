//
//  Localization.m
//  Sports Bets by Bet IT Best
//
//  Created by Dummy on 14.07.15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Localization.h"

@implementation Localization

+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^
    {
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}


//init-method sets the country code for the app
//uses the language of the device of the user
//the needed dictionary file (JSON-file) is named like Dictionary_"country code"
//so with getting the country code the app can chose the needed JSON-file for the translation
-(id)init
{
    self = [super init];
    
    //with iOS 9 Apple changed the semantic of the countrycode (first "en" now "en-DE")
   /* NSString *version = [[UIDevice currentDevice] systemVersion];
    BOOL isAtLeast9 = [version floatValue] >= 9.0;
    
    NSString *defaultLanguage = @"en";
    NSString * preferredlanguage;
    if (isAtLeast9)
    {
        NSLog(@"Version > iOS 9");
        preferredlanguage = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
    }
    else
    {
        NSLog(@"kein ios 9");
         preferredlanguage = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
    }*/
    
    NSString *defaultLanguage = @"en";
    //change preferredlanguage for testing, but then restore it to [[[NSLocale etc.
//    NSString *preferredlanguage = @"de";
    NSString *preferredlanguage = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
    
    NSFileManager *manager = [[NSFileManager alloc]init];
    NSError *error;
    if([manager fileExistsAtPath:[[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"Dictionary_%@",preferredlanguage] ofType:@"json"]])
    {
        NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"Dictionary_%@",preferredlanguage] ofType:@"json"] encoding:NSUTF8StringEncoding error:&error];

        NSError *error;
        if (error)
        {
           // NSLog(@"%@",error.userInfo);
        }
        else
        {
            _dictionary = [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
        }
        
    }
    else
    {
        NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"Dictionary_%@",defaultLanguage] ofType:@"json"] encoding:NSUTF8StringEncoding error:&error];
        
        NSError *error;
        
        if (error)
        {
           //NSLog(@"%@",error.userInfo);
        }
        else
        {
            _dictionary = [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
        }
    }
    return self;
}

-(NSString *)localizedStringForKey:(NSString *)key comment:(NSString *)comment
{
    if (_dictionary[key])
    {
        return _dictionary[key];
    }
    else if(comment)
    {
        return comment;
    }
    else
    {
        return key;
    }
}

@end
