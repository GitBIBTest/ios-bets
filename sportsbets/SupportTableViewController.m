//
//  SupportViewController.m
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 25/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "SupportTableViewController.h"

@implementation SupportTableViewController
{
    MBProgressHUD *progressHud;
    bool optionsDownloaded;
    //NSMutableString *selectedCategoryUID;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];

    self.categorypicker = [[UIPickerView alloc] init];
    self.categorypicker.dataSource = self;
    self.categorypicker.delegate = self;
    self.categorypicker.showsSelectionIndicator = YES;
    self.categorypicker.tag = 1;
    self.categoryTextField.inputView = self.categorypicker;

    self.reasonPicker = [[UIPickerView alloc] init];
    self.reasonPicker.dataSource = self;
    self.reasonPicker.delegate = self;
    self.reasonPicker.showsSelectionIndicator = YES;
    self.reasonPicker.tag = 2;
    self.reasonTextField.inputView = self.reasonPicker;

    //create a toolbar with a done Button which will be used on all the relative fields
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(pickerViewDoneButtonPressed)];
    doneBarButton.tintColor = BIBredcolor;
    keyboardToolbar.items = @[flexBarButton, doneBarButton];

    self.categoryTextField.inputAccessoryView = keyboardToolbar;
    self.reasonTextField.inputAccessoryView = keyboardToolbar;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    if (pickerView.tag == 1) // this is category pickerView
    {
        SupportObject *supportObjectInrow = [self.downloadedSupportObjects objectAtIndex:row];
        self.categoryTextField.text = supportObjectInrow.categoryName;
        self.selectedCategoryUID = [supportObjectInrow.categoryUid mutableCopy];
    }
    else if (pickerView.tag == 2) // this is the reason picker view
    {
        SupportObject *supportObjectInCategoryRow = [self.downloadedSupportObjects objectAtIndex:[self.categorypicker selectedRowInComponent:0]];
        NSArray *supportObjectReasonKeys = [supportObjectInCategoryRow.reasons allKeys];
        self.reasonTextField.text = [supportObjectInCategoryRow.reasons valueForKey:[supportObjectReasonKeys objectAtIndex:row]];
        self.selectedReasonUID = [supportObjectReasonKeys objectAtIndex:row];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    optionsDownloaded = NO;
    
    SupportService *supportService = [[SupportService alloc] init];
    supportService.delegate = self;

    [supportService downloadSupportOptions];
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)supportOptionsDownloaded:(NSArray *)supportObjectsArray
{
    [progressHud hide:YES];
    optionsDownloaded = YES;
    self.downloadedSupportObjects = [[NSArray alloc] initWithArray:supportObjectsArray];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (!optionsDownloaded)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

- (void)sendSupportRequest
{
    SupportService *supportService = [[SupportService alloc] init];
    supportService.delegate = self;
    
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)supportRequestSent:(NSArray *)serverReponse
{
    [progressHud hide:YES];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

    if (pickerView.tag == 1) {
        return self.downloadedSupportObjects.count;
    } else {
        for (SupportObject *obj in self.downloadedSupportObjects)
        {
            if ([obj.categoryUid isEqualToString:self.selectedCategoryUID]) {
                return obj.reasons.count;
            }
        }
        return 0;
    }
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    NSString *title;

    if (pickerView.tag == 1) // this is category pickerView
    {
        SupportObject *supportObjectInrow = self.downloadedSupportObjects[row];
        title = supportObjectInrow.categoryName;
    }
    else if (pickerView.tag == 2) // this is the reason picker view
    {
        for (SupportObject *object in self.downloadedSupportObjects) {
            if ([object.categoryUid isEqualToString:self.selectedCategoryUID]) {
                NSArray *reasons = object.reasons.allValues;
                title = reasons[row];
            }
        }
    }
    return title;
}

//dismiss the keyboard
-(void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

-(void)pickerViewDoneButtonPressed
{
    [self.view endEditing:YES];
}


@end
