//
//  CashOutHistoryVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Macros.h"
#import "MBProgressHUD.h"
#import "UserData.h"
#import "PaymentsHistoryService.h"

@interface CashOutHistoryVC : UITableViewController <UITableViewDataSource, UITableViewDelegate, PaymentsHistoryServiceProtocol>

@end
