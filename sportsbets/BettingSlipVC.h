//
//  BettingSlipVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 02/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Reachability.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Localization.h"
#import "BetTypeHeader.h"
#import "MatchesHeader.h"
#import "VendorBetsService.h"
#import "VendorBetsObject.h"
#import "MBProgressHUD.h"
#import "BettingSlipMatchInfoCell.h"
#import "BettingSlipVendorCell.h"
#import "BetStakeCell.h"
#import "BetProfitCell.h"
#import "PlaceBetButtonCell.h"
#import "PlaceAllBetsCell.h"
#import "UserData.h"
#import "PlaceBetsService.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface BettingSlipVC : UITableViewController <UITableViewDelegate, UITableViewDataSource, VendorBetsServiceProtocol, betTypeHeaderProtocol, UITextFieldDelegate, PlaceBetsServiceProtocol>
{
    VendorBetsService *vendorBetsService;
}

@property (nonatomic,retain) IBOutlet UIBarButtonItem *burgerButton;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *closeButton;
@property (nonatomic,retain) NSMutableArray *matches;

@property (nonatomic,retain) IBOutlet UILabel *vendor;
@property (nonatomic,retain) IBOutlet UILabel *rate;

@end