//
//  NSString+UrlEncoding.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 21/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UrlEncoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

@end
