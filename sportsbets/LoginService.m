//
//  LoginService.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 24.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "LoginService.h"
@interface LoginService ()
{
    NSMutableData *_downloadedData;
    NSString *given_username;
    NSString *given_password;
}

@end

@implementation LoginService

-(void)checkLoginForUsername:(NSString *)username andForPassword:(NSString *)password
{
    given_username = username;
    given_password = password;
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/authentication/login";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&username=%@&password=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], [username urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    NSLog(@"%@", topLevelArray);
    
    //Here a delegate would be better to send the message back to LoginVC
    if (topLevelArray == nil)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"login_error", nil)
                                              message:@"Bad response"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self.loginVC presentViewController:alertController animated:YES completion:nil];
    }
    else if ([[topLevelArray valueForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"login_error", nil)
                                              message:[topLevelArray valueForKey:@"message"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self.loginVC presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        //if no errormessage occurs set login data
        NSEntityDescription *login = [NSEntityDescription entityForName:@"Login" inManagedObjectContext:self.managedObjectContext];
        NSManagedObject *newLogin = [[NSManagedObject alloc]initWithEntity:login insertIntoManagedObjectContext:self.managedObjectContext];
        //set attributes for entity Sport
        [newLogin setValue:[[topLevelArray valueForKey:@"result"] valueForKey:@"username"] forKey:@"username"];
        [newLogin setValue:given_password forKey:@"password"];
        [newLogin setValue:[[topLevelArray valueForKey:@"result"] valueForKey:@"password"] forKey:@"password_encrypted"];
        [newLogin setValue:[[topLevelArray valueForKey:@"result"] valueForKey:@"id"] forKey:@"id"];
        [self.managedObjectContext save:nil];
        
        NSLog(@"%@", newLogin);
        
        [self.loginVC.navigationController popViewControllerAnimated:YES];
    }
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end