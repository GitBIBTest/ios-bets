//
//  TextFieldNoCursor.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 28/11/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "TextFieldNoCursor.h"

@implementation TextFieldNoCursor

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect 
{
    // Drawing code
}
*/

- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    return CGRectZero;
}

@end
