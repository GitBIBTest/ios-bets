//
//  VendorBetstData.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VendorBetsModel.h"

@interface VendorBetsData : NSObject

@property (nonatomic,retain) NSMutableArray *vendorArray;

-(void)addVendorBets:(VendorBetsModel *)vendorBetsData;

@end