//
//  DepositHistoryVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "DepositHistoryVC.h"

@implementation DepositHistoryVC
{
    NSMutableArray *previousPayments;
    MBProgressHUD *progressHud;
    NSDateFormatter *dateFormatter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    previousPayments = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    
    UserData *userData = [UserData getInstance];
    
    PaymentsHistoryService *paymentsHistoryService = [[PaymentsHistoryService alloc] init];
    paymentsHistoryService.delegate = self;
    [paymentsHistoryService downloadPaymentHistoryOfType:@"in" ForUser:userData.userId andPassword:userData.passwordEncrypted];
    
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHud.labelText = @"Loading...";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [progressHud hide:YES];
}

- (void)historyDownloaded:(NSArray *)historyArray
{
    for (NSObject *k in historyArray)
    {
        double unixTimeStamp = [[k valueForKey:@"processed"] doubleValue];
        NSTimeInterval _interval=unixTimeStamp;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateFormat:LocalizedString(@"date_no_hour", nil)];
        
        NSString *dateString = [dateFormatter stringFromDate:date];
        NSString *paymentType;
        switch ([[k valueForKey:@"paymentservice_uid"] intValue]) {
            case 1:
                paymentType = @"PayPal";
                break;
            case 2:
                paymentType = @"SOFORT Überweisung";
                break;
            case 3:
                paymentType = LocalizedString(@"credit_card_payment", nil);
                break;
            default:
                paymentType = LocalizedString(@"error", nil);
                break;
        }
        
        NSMutableDictionary *kPayment = [[NSMutableDictionary alloc] init];
        [kPayment setValue:[k valueForKey:@"total"] forKey:@"amount"];
        [kPayment setValue:dateString forKey:@"date"];
        [kPayment setValue:paymentType forKey:@"payment_type"];
        
        [previousPayments addObject:kPayment];
    }
    
    [progressHud hide:YES];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MAX(1, [previousPayments count]);
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([previousPayments count] == 0)
    {
        return 90;
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    UITableViewCell *cell;
    
    if ([previousPayments count] == 0)
    {
        cellIdentifier = @"NoPreviousPaymentsCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        UILabel *noPreviousPaymentsLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 20.0, WIDTH(self.view)-30.0, 50.0)];
        [noPreviousPaymentsLabel setFont:[UIFont fontWithName:@"Montserrat" size:17.0]];
        [noPreviousPaymentsLabel setTextAlignment:NSTextAlignmentCenter];
        noPreviousPaymentsLabel.text = LocalizedString(@"no_previous_deposits", nil);
        noPreviousPaymentsLabel.numberOfLines = 2;
        
        [cell addSubview:noPreviousPaymentsLabel];
    }
    else
    {
        cellIdentifier = @"PreviousPaymentCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                
        NSObject *currentPayment = [previousPayments objectAtIndex:indexPath.row];
        
        UILabel *paymentAmountLabel = (UILabel *)[cell viewWithTag:10];
        UILabel *paymentDateLabel = (UILabel *)[cell viewWithTag:20];
        UILabel *paymentTypeLabel = (UILabel *)[cell viewWithTag:30];
        
        paymentDateLabel.text = [currentPayment valueForKey:@"date"];
        paymentAmountLabel.text = F(@"%@ €",[currentPayment valueForKey:@"amount"]);
        paymentTypeLabel.text = LocalizedString([currentPayment valueForKey:@"payment_type"], nil);
    }
    return cell;
}

@end