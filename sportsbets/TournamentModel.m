//
//  TournamentModel.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "TournamentModel.h"

@implementation TournamentModel

- (id) initWithName:(NSString*)tournamentName andWithID:(NSString*)tournamentID forCategoryName:(NSString *)categoryName
{
    self.category_name = categoryName;
    self.tournament_name = tournamentName;
    self.tournament_uid = tournamentID;
    self.betsArray = [[NSMutableArray alloc] init];
    return self;
}

@end