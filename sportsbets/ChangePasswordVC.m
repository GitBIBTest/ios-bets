//
//  ChangePasswordVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 14/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "ChangePasswordVC.h"

@implementation ChangePasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //load image logo
    self.navigationItem.title = LocalizedString(@"change_password", nil);
    
    // initialization of labels and text fields
    self.oldPasswordTextField.placeholder = LocalizedString(@"old_password", nil);
    self.passwordTextField.placeholder = LocalizedString(@"new_password", nil);
    self.confirmPasswordTextField.placeholder = LocalizedString(@"password_confirm", nil);
    
    [self.confirmPasswordButton setTitle:LocalizedString(@"confirm", nil) forState:UIControlStateNormal];
}

- (IBAction)confirmButtonPressed:(id)sender
{
    /*---------------------------------------------------------------------------------------------------
     TODO the password change has to happen first on the server and then, if successful, on the database
    ---------------------------------------------------------------------------------------------------*/
    
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:@"The password confirmation doesn't match"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        NSString *newPassword = self.passwordTextField.text;
        //update the local store
        NSPredicate *predicatePassword = [NSPredicate predicateWithFormat:@"password = %@", self.oldPasswordTextField.text];
        NSError *fetchError = nil;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Login"];
        [fetchRequest setPredicate:predicatePassword];
        
        NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
        
        if (fetchResult.count == 0)
        {
            NSLog(@"inserted wrong password %@", self.oldPasswordTextField.text);
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"Error", nil)
                                                  message:@"The current password is wrong"
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"Ok",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"OK action");
                                           }];
            
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (fetchResult.count > 1)
        {
            NSLog(@"Error, this definitely shouldn't happen");
        }
        else
        {
//            NSManagedObject *passwordToChange = [fetchResult objectAtIndex:0];
//            NSLog(@"password changed from %@ to %@", [passwordToChange valueForKey:@"password"], newPassword);
//            [passwordToChange setValue:newPassword forKey:@"password"];
//            
//            [self.managedObjectContext save:nil];
//            
//            UIAlertController *alertController = [UIAlertController
//                                                  alertControllerWithTitle:LocalizedString(@"Error", nil)
//                                                  message:@"Your password was succesfully changed"
//                                                  preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction *cancelAction = [UIAlertAction
//                                           actionWithTitle:LocalizedString(@"Ok",nil)
//                                           style:UIAlertActionStyleDefault
//                                           handler:^(UIAlertAction *action)
//                                           {
//                                               NSLog(@"OK action");
//                                               [self.navigationController popViewControllerAnimated:YES];
//                                           }];
//            
//            [alertController addAction:cancelAction];
//            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
