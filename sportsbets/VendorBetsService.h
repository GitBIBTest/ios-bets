//
//  VendorBetsService.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VendorBetsModel.h"
#import "VendorBetsObject.h"
#import "Utils.h"
#import "NSString+UrlEncoding.h"
#import "Macros.h"

@protocol VendorBetsServiceProtocol <NSObject>

- (void)vendorBetsDownloadedForMatch:(VendorBetsData*)vendorBetsDataForMatch;

@end

@interface VendorBetsService : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, weak) id <VendorBetsServiceProtocol> delegate;

-(void)loadVendorBetsForMatch:(NSString *)match_id andSelectedTeam:(NSString *)selected_team;
-(void)loadVendorBetsForMatch:(NSString *)match_id andSelectedTeam:(NSString *)selected_team forUser:(NSString *)user;
-(void)loadVendorBetsForCombi:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams;
-(void)loadVendorBetsForCombi:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams forUser:(NSString *)user;
-(void)loadVendorBetsForSystem:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams ignoreKBets:(NSArray *)ignoreList;
-(void)loadVendorBetsForSystem:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams ignoreKBets:(NSArray *)ignoreList forUser:(NSString *)user;

@end
