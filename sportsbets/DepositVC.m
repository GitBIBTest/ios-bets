//
//  DepositVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "DepositVC.h"

@implementation DepositVC
{
    BOOL skipSaveCard;
    NSArray *creditCardsArray;
    NSArray *paymentMethods;
    NSArray *acceptedCardTypes;
    bool hideSection1;
    bool hideSection2;
    NSInteger thisYear;
    NSInteger thisMonth;
}

@synthesize methodTextField, amountTextField, ibanTextField, bicTextField, accountHolderTextField, cardTypeTextField, cardNumberTextField, cardCodeTextField, cardExpirationPicker, cardOwnerTextField;

- (void)viewWillAppear:(BOOL)animated
{
    /*-------------------------------------------------------------------
     TODO
     -payments methods have to be also retrieved from the remote database
     -------------------------------------------------------------------*/
    
    //download data about the credit cards
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CreditCard"];
    NSError *fetchError = nil;
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat:@"toLogin.username LIKE[c] %@", [UserData getInstance].userName]];
    creditCardsArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    NSMutableArray *creditCardPickerViewIDArray = [[NSMutableArray alloc] init];
    
    for (NSObject *k in creditCardsArray) {
        NSString *creditCardType = [k valueForKey:@"type"];
        NSString *encryptedCreditCardNumber = F(@"*...*%@", [[k valueForKey:@"number"] substringFromIndex:12]);
        NSString *creditCardPickerViewID = F(@"%@ %@", creditCardType, encryptedCreditCardNumber);
        [creditCardPickerViewIDArray addObject:creditCardPickerViewID];
    }
    
    //define the accepted payment methods
    NSMutableArray *mutablePaymentMethods = [[NSMutableArray alloc] init];
    [mutablePaymentMethods addObject:F(@"%s%@%s","-",LocalizedString(@"choose_method", nil),"-")];
    [mutablePaymentMethods addObject:LocalizedString(@"bank_transfer", nil)];
    [mutablePaymentMethods addObjectsFromArray:creditCardPickerViewIDArray];
    [mutablePaymentMethods addObject:LocalizedString(@"new_credit_card", nil)];
    
    paymentMethods = [NSArray arrayWithArray:mutablePaymentMethods];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //define the accepted credit cards
    acceptedCardTypes = @[F(@"%s%@%s","-",LocalizedString(@"card_type", nil),"-"), @"VISA", @"MasterCard", @"Maestro"];
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    //place the done button on the "amount" textfield
    UIToolbar *toolBarAmountTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarAmountTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *amountTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                               target:self
                                                                                               action:@selector(amountTextFieldDoneTouched:)];
    [toolBarAmountTextField setItems:[NSArray arrayWithObjects:
                                      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                    target:nil
                                                                                    action:nil],
                                      amountTextFieldDoneButton, nil]];
    amountTextField.inputAccessoryView = toolBarAmountTextField;
    
    //place the done button on the "security code" textfield
    UIToolbar *toolBarCVVTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarCVVTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *CVVTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                               target:self
                                                                                               action:@selector(CVVTextFieldDoneTouched:)];
    [toolBarCVVTextField setItems:[NSArray arrayWithObjects:
                                      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                    target:nil
                                                                                    action:nil],
                                      CVVTextFieldDoneButton, nil]];
    cardCodeTextField.inputAccessoryView = toolBarCVVTextField;
    
    //configure the pickerviews
        //payment method
    methodPickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    methodPickerView.delegate = self;
    methodPickerView.dataSource = self;
    methodPickerView.showsSelectionIndicator = YES;
    methodPickerView.tag = 0;
    
        //place the done button on the "method" pickerview
    UIToolbar *toolBarAmountPickerView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarAmountPickerView.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *amountPickerViewDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                target:self
                                                                                                action:@selector(methodPickerViewDoneTouched:)];
    [toolBarAmountPickerView setItems:[NSArray arrayWithObjects:
                                       [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                                              target:nil
                                                                                                              action:nil],
                                       amountPickerViewDoneButton, nil]];
    self.methodTextField.inputAccessoryView = toolBarAmountPickerView;
    
        //card type
    cardTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    cardTypePickerView.delegate = self;
    cardTypePickerView.dataSource = self;
    cardTypePickerView.showsSelectionIndicator = YES;
    cardTypePickerView.tag = 1;
    
        //place the done button on the "card type" pickerview
    UIToolbar *toolBarCardTypePickerView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarCardTypePickerView.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *cardTypePickerViewDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                target:self
                                                                                                action:@selector(cardTypePickerViewDoneTouched:)];
    [toolBarCardTypePickerView setItems:[NSArray arrayWithObjects:
                                       [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                     target:nil
                                                                                     action:nil],
                                       cardTypePickerViewDoneButton, nil]];
    self.cardTypeTextField.inputAccessoryView = toolBarCardTypePickerView;
    
    //configure labels and other views
        // section 0
    self.amountLabel.text = LocalizedString(@"amount", nil);
    amountTextField.placeholder = LocalizedString(@"amount", nil);
    methodTextField.text = F(@"%s%@%s","-",LocalizedString(@"choose_method", nil),"-");
    [methodTextField setInputView:methodPickerView];
    hideSection1 = YES;
    hideSection2 = YES;
    
        //section 1
    self.accountHolderLabel.text = LocalizedString(@"account_holder", nil);
    accountHolderTextField.placeholder = F(@"%@ %@", LocalizedString(@"name", nil), LocalizedString(@"surname", nil));
    [self.continue1Button setTitle:LocalizedString(@"continue", nil) forState:UIControlStateNormal];
    
        //section 2
    self.cardTypeLabel.text = LocalizedString(@"card_type", nil);
    cardTypeTextField.placeholder = LocalizedString(@"card_type", nil);
    [cardTypeTextField setInputView:cardTypePickerView];
    self.cardNumberLabel.text = LocalizedString(@"card_number", nil);
    cardNumberTextField.placeholder = LocalizedString(@"card_number", nil);
    self.cardCodeLabel.text = LocalizedString(@"card_security_code", nil);
    cardCodeTextField.placeholder = LocalizedString(@"card_security_code", nil);
    self.cardExpirationLabel.text = LocalizedString(@"expiration_date", nil);
    //get current year and set it as minimum card's expiration date
    NSDateComponents* components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:[NSDate date]];
    thisYear = [components year];
    thisMonth = [components month];
    [cardExpirationPicker setupMinYear:thisYear maxYear:thisYear + 10];
    self.cardOwnerLabel.text = LocalizedString(@"card_owner", nil);
    cardOwnerTextField.placeholder = LocalizedString(@"card_owner", nil);
    [self.continue2Button setTitle:LocalizedString(@"continue", nil) forState:UIControlStateNormal];
}

//------------------------------------------------------------------------------------
//Picker View Category/Tournament
//------------------------------------------------------------------------------------

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 0)
    {
        return [paymentMethods count];
    }
    else
    {
        return [acceptedCardTypes count];
    }
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag == 0)
    {
        return [paymentMethods objectAtIndex:row];
    }
    else
    {
        return [acceptedCardTypes objectAtIndex:row];
    }
}

- (void)methodPickerViewDoneTouched:(UIBarButtonItem *)sender
{
    self.methodTextField.text = [paymentMethods objectAtIndex:[methodPickerView selectedRowInComponent:0]];
    if ([methodPickerView selectedRowInComponent:0] == 0)
    {
        self.methodTextField.font = [UIFont fontWithName:@"Montserrat-Regular" size:15.0];
        hideSection1 = YES;
        hideSection2 = YES;
    }
    else
    {
        NSInteger selectedRow = [methodPickerView selectedRowInComponent:0];
        self.methodTextField.font = [UIFont fontWithName:@"Montserrat-Bold" size:15.0];
        if (selectedRow == 1)
        {
            hideSection1 = NO;
            hideSection2 = YES;
        }
        else if (selectedRow >= 2)
        {
            hideSection1 = YES;
            hideSection2 = NO;
            skipSaveCard = NO;
            //auto fill each field if a known credit card is selected
            if (selectedRow != [paymentMethods count] -1)
            {
                skipSaveCard = YES;
                cardTypeTextField.text = [[creditCardsArray objectAtIndex:selectedRow-2] valueForKey:@"type"];
                //select the correct row in the cardType pickerView
                int index;
                for (index = 1; index < [cardTypePickerView numberOfRowsInComponent:0]; index++)
                {
                    if ([[self pickerView:cardTypePickerView titleForRow:index forComponent:0] isEqualToString:cardTypeTextField.text])
                    {
                        break;
                    }
                }
                [cardTypePickerView selectRow:index inComponent:0 animated:YES];
                cardNumberTextField.text = [[creditCardsArray objectAtIndex:selectedRow-2] valueForKey:@"number"];
                cardCodeTextField.text = [[creditCardsArray objectAtIndex:selectedRow-2] valueForKey:@"cvc"];
                NSArray *expiration_date = [[[creditCardsArray objectAtIndex:selectedRow-2] valueForKey:@"expiration_date"] componentsSeparatedByString:@" "];
                NSString *expiration_month = [expiration_date objectAtIndex:0];
                NSString *expiration_year = [expiration_date objectAtIndex:1];
                [cardExpirationPicker selectRow:[expiration_month intValue]-1 inComponent:0 animated:YES];
                [cardExpirationPicker selectRow:[expiration_year intValue]-thisYear inComponent:1 animated:YES];
                cardOwnerTextField.text = [[creditCardsArray objectAtIndex:selectedRow-2] valueForKey:@"owner"];
            }
            else
            {
                cardTypeTextField.text = @"";
                cardNumberTextField.text = @"";
                cardCodeTextField.text = @"";
                [cardExpirationPicker selectRow:0 inComponent:0 animated:YES];
                [cardExpirationPicker selectRow:0 inComponent:1 animated:YES];
                cardOwnerTextField.text = @"";
            }
        }
    }
    [self.tableView reloadData];
}

- (void)cardTypePickerViewDoneTouched:(UIBarButtonItem *)sender
{
    if ([cardTypePickerView selectedRowInComponent:0] == 0)
    {
        self.cardTypeTextField.text = @"";
    }
    else
    {
        self.cardTypeTextField.text = [acceptedCardTypes objectAtIndex:[cardTypePickerView selectedRowInComponent:0]];
    }
    [self.view endEditing:YES];
}

//------------------------------------------------------------------------------------
//TableView Functions
//------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Bank transfer
    if (section == 1)
    {
        if(hideSection1)
        {
            return 0;
        }
        else
        {
            return 7;
        }
    }
    //Credit cards
    if (section == 2)
    {
        if (hideSection2)
        {
            return 0;
        }
        else
        {
            return 11;
        }
    }
    //section 0 always visible
    else
    {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Bank transfer
    if (indexPath.section == 1)
    {
        switch (indexPath.row) {
            case 0:
                return 30;
            case 1:
                return 44;
            case 2:
                return 30;
            case 3:
                return 44;
            case 4:
                return 30;
            case 5:
                return 44;
            case 6:
                return 60;
            default:
                break;
        }
    }
    //Credit cards
    else if (indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
                return 30;
            case 1:
                return 44;
            case 2:
                return 30;
            case 3:
                return 44;
            case 4:
                return 30;
            case 5:
                return 44;
            case 6:
                return 30;
            case 7:
                return 60;
            case 8:
                return 30;
            case 9:
                return 44;
            case 10:
                return 60;
            default:
                break;
        }
    }
    return 44;
}

//------------------------------------------------------------------------------------
//Actions for buttons and misc functions
//------------------------------------------------------------------------------------

- (IBAction)confirmButtonVC1Pressed:(id)sender
{
    [self.view endEditing:YES];
    BOOL showAlert = NO;
    
    //sanitize the strings by removing (or trimming) spaces
    ibanTextField.text = [ibanTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    bicTextField.text = [bicTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    accountHolderTextField.text = [accountHolderTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    
    //in any case, an amount has to be inserted
    if ([amountTextField.text floatValue] == 0)
    {
        showAlert = YES;
        self.amountLabel.textColor = [UIColor redColor];
        amountTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.amountLabel.textColor = [UIColor blackColor];
    }
    //german IBANs have always 22 digits
    if ([ibanTextField.text length] != 22)
    {
        showAlert = YES;
        self.ibanLabel.textColor = [UIColor redColor];
        ibanTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.ibanLabel.textColor = [UIColor blackColor];
    }
    //if bic has 8 digits, it's the bank's main office, so add 3 Xs
    if ([bicTextField.text length] == 8)
    {
        [self.bicTextField.text stringByAppendingString:@"XXX"];
        self.bicLabel.textColor = [UIColor blackColor];
    }
    else if ([bicTextField.text length] != 11)
    {
        showAlert = YES;
        self.bicLabel.textColor = [UIColor redColor];
        bicTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.bicLabel.textColor = [UIColor blackColor];
    }
    //name needs at least one space and no punctuation, symbols or numbers
    if ([accountHolderTextField.text length] < 3 ||
        [[accountHolderTextField.text componentsSeparatedByString:@" "] count] - 1 < 1 ||
        [accountHolderTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound ||
        [accountHolderTextField.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound ||
        [accountHolderTextField.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.accountHolderLabel.textColor = [UIColor redColor];
        accountHolderTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.accountHolderLabel.textColor = [UIColor blackColor];
    }
    
    if (showAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Errors in the input", nil)
                                              message:@"Please correct the highlighted fields and retry"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self performSegueWithIdentifier:@"depositConfirmVC1" sender:sender];
    }
}

-(IBAction)confirmButtonVC2Pressed:(id)sender
{
    [self.view endEditing:YES];
    BOOL showAlert = NO;
    
    //sanitize the strings by removing (or trimming) spaces
    cardNumberTextField.text = [cardNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    cardCodeTextField.text = [cardCodeTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    cardOwnerTextField.text = [cardOwnerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    
    //in any case, an amount has to be inserted
    if ([amountTextField.text floatValue] == 0)
    {
        showAlert = YES;
        self.amountLabel.textColor = [UIColor redColor];
        amountTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.amountLabel.textColor = [UIColor blackColor];
    }
    //card type must be set to something
    if ([cardTypePickerView selectedRowInComponent:0] == 0)
    {
        showAlert = YES;
        self.cardTypeLabel.textColor = [UIColor redColor];
    }
    else
    {
        self.cardTypeLabel.textColor = [UIColor blackColor];
    }
    //card number must be 16 digits long and just decimal
    if ([cardNumberTextField.text length] != 16 || [cardNumberTextField.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.cardNumberLabel.textColor = [UIColor redColor];
        cardNumberTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.cardNumberLabel.textColor = [UIColor blackColor];
    }
    //security code must 3 digits long and just decimal
    if ([cardCodeTextField.text length] != 3 || [cardCodeTextField.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.cardCodeLabel.textColor = [UIColor redColor];
        cardCodeTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.cardCodeLabel.textColor = [UIColor blackColor];
    }
    //the card needs not to be expired
    if ([[cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:1] forComponent:1] integerValue] == thisYear &&
        [[cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:0] forComponent:0] integerValue] < thisMonth)
    {
        showAlert = YES;
        self.cardExpirationLabel.textColor = [UIColor redColor];
    }
    else
    {
        self.cardExpirationLabel.textColor = [UIColor blackColor];
    }
    //name needs at least one space and no punctuation, symbols or numbers
    if ([cardOwnerTextField.text length] < 3 ||
        [[cardOwnerTextField.text componentsSeparatedByString:@" "] count] - 1 < 1 ||
        [cardOwnerTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound ||
        [cardOwnerTextField.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound ||
        [cardOwnerTextField.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.cardOwnerLabel.textColor = [UIColor redColor];
        cardOwnerTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.cardOwnerLabel.textColor = [UIColor blackColor];
    }
    
    if (showAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Errors in the input", nil)
                                              message:@"Please correct the highlighted fields and retry"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        if (skipSaveCard)
        {
            [self pushToDetailAndSaveCreditCard:NO withSender:sender];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"Save", nil)
                                                  message:@"Do you want the application to remember this credit card's data?"
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *noAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"no", nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self pushToDetailAndSaveCreditCard:NO withSender:sender];
                                       }];
            UIAlertAction *yesAction = [UIAlertAction
                                        actionWithTitle:LocalizedString(@"yes", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action)
                                        {
                                            [self pushToDetailAndSaveCreditCard:YES withSender:sender];
                                        }];
            
            [alertController addAction:noAction];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)pushToDetailAndSaveCreditCard:(BOOL)saveCreditCard withSender:(id)sender
{
    if (saveCreditCard)
    {
        NSString *expiration_month = [cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:0] forComponent:0];
        NSString *expiration_year = [cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:1] forComponent:1];
        NSManagedObject *newCreditCard = [NSEntityDescription insertNewObjectForEntityForName:@"CreditCard" inManagedObjectContext:self.managedObjectContext];
        [newCreditCard setValue:cardTypeTextField.text forKey:@"type"];
        [newCreditCard setValue:cardNumberTextField.text forKey:@"number"];
        [newCreditCard setValue:cardCodeTextField.text forKey:@"cvc"];
        [newCreditCard setValue:F(@"%@ %@", expiration_month, expiration_year) forKey:@"expiration_date"];
        [newCreditCard setValue:cardOwnerTextField.text forKey:@"owner"];
        [newCreditCard setValue:[self fetchLoggedInUser] forKeyPath:@"toLogin"];
        
        [self.managedObjectContext save:nil];
    }
    [self performSegueWithIdentifier:@"depositConfirmVC2" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DepositConfirmVC *depositConfirmVC = [segue destinationViewController];
    //Bank transfer
    if ([[segue identifier] isEqualToString:@"depositConfirmVC1"])
    {
        depositConfirmVC.passedPaymentType = @"type1";
        depositConfirmVC.passedData = [NSArray arrayWithObjects: amountTextField.text, ibanTextField.text, bicTextField.text, accountHolderTextField.text, nil];
    }
    //Credit cards
    else if ([[segue identifier] isEqualToString:@"depositConfirmVC2"])
    {
        depositConfirmVC.passedPaymentType = @"type2";
        NSString *expiration_month = [cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:0] forComponent:0];
        NSString *expiration_year = [cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:1] forComponent:1];
        depositConfirmVC.passedData = [NSArray arrayWithObjects: amountTextField.text, cardTypeTextField.text, cardNumberTextField.text, cardCodeTextField.text, expiration_month, expiration_year, cardOwnerTextField.text, nil];
    }
}

- (void)amountTextFieldDoneTouched:(UIBarButtonItem *)sender
{
    //Convert whatever is written into a floatvalue with 2 decimal places
    amountTextField.text = F(@"%.2f", [[amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]);
    [self textFieldShouldReturn:amountTextField];
}

- (void)CVVTextFieldDoneTouched:(UIBarButtonItem *)sender
{
    [self textFieldShouldReturn:cardCodeTextField];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([amountTextField isFirstResponder])
    {
        amountTextField.text = F(@"%.2f", [[amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]);
    }
    return YES;
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    if ([amountTextField isFirstResponder])
    {
        amountTextField.text = F(@"%.2f", [[amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]);
    }
    [self.view endEditing:YES];
}

- (NSObject *)fetchLoggedInUser
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Login"];
    NSError *fetchError = nil;
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat:@"username LIKE[c] %@", [UserData getInstance].userName]];
    NSArray *loginArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    NSObject *loggedInUser = [loginArray objectAtIndex:0];
    
    return loggedInUser;
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
