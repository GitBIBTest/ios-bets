//
//  ChangePasswordVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 14/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "AppDelegate.h"
#import "Macros.h"

@interface ChangePasswordVC : UITableViewController

@property (nonatomic, retain) IBOutlet UITextField *oldPasswordTextField;
@property (nonatomic, retain) IBOutlet UITextField *passwordTextField;
@property (nonatomic, retain) IBOutlet UITextField *confirmPasswordTextField;

@property (nonatomic, retain) IBOutlet UIButton *confirmPasswordButton;

@end
