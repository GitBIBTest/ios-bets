//
//  FilterMaster.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterMaster : NSObject

+(FilterMaster*) sharedFilterMaster;

@property (nonatomic,assign) NSInteger sportIdentifier;
@property (nonatomic,retain) NSString *filter_category_uid;
@property (nonatomic,retain) NSString *filter_tournament_uid;

- (void)loadFilterForSport:(NSInteger)newSportIdentifier;

@end
