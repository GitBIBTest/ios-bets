//
//  ChangeLimitsService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 23/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "ChangeLimitsService.h"

@interface ChangeLimitsService ()
{
    NSMutableData *_downloadedData;
    NSString *typeOfService;
}

@end

@implementation ChangeLimitsService

-(void)downloadCurrentLimitsForUser:(NSString *)user andPassword:(NSString *)password
{
    typeOfService = @"select";
    NSString * url = @"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceChangeLimitsSelect.php?userid=";
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"%@%@%@%@", url, user,@"&password=", password)];
    NSLog(@"url user info = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)updateLimitsWithMonthLimit:(NSString *)monthLimit andBetLimit:(NSString *)betLimit ForUser:(NSString *)user andPassword:(NSString *)password
{
    typeOfService = @"update";
    NSString * url = @"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceChangeLimitsUpdate.php?userid=";
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"%@%@%@%@%@%@%@%@", url, user,@"&password=", password, @"&monthlimit=", monthLimit, @"&betlimit=", betLimit)];
    NSLog(@"url user info = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if ([typeOfService isEqualToString:@"select"])
    {
        // Parse the JSON that came in
        NSError *error;
        NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
        
        //has 2 rows top, one for limit type 1 (month), one for limit type 2 (bet)
        NSMutableDictionary *valuesToReturn = [[NSMutableDictionary alloc] init];
        
        if ([topLevelArray count] > 0)
        {
            //set limits if not null
            for (NSObject *k in topLevelArray)
            {
                if ([[k valueForKey:@"limit_type"] isEqual:[NSNull null]])
                {
                    break;
                }
                else if ([[k valueForKey:@"limit_type"] isEqualToString:@"1"])
                {
                    NSString *fetchedBetsLimitMonth = [k valueForKey:@"limit_amount"];
                    [valuesToReturn setValue:fetchedBetsLimitMonth forKey:@"bets_limit_month"];
                }
                else if ([[k valueForKey:@"limit_type"] isEqualToString:@"2"])
                {
                    NSString *fetchedBetsLimitBet = [k valueForKey:@"limit_amount"];
                    [valuesToReturn setValue:fetchedBetsLimitBet forKey:@"bets_limit_bet"];
                }
            }
        }
        
        if ([valuesToReturn valueForKey:@"bets_limit_month"] == nil)
        {
            [valuesToReturn setValue:DefaultBetsLimitMonth forKey:@"bets_limit_month"];
        }
        
        if ([valuesToReturn valueForKey:@"bets_limit_bet"] == nil)
        {
            [valuesToReturn setValue:DefaultBetsLimitBet forKey:@"bets_limit_bet"];
        }
        
        //TODO max permitted limit read from verification if present
        if ([valuesToReturn valueForKey:@"max_bets_limit_month"] == nil)
        {
            [valuesToReturn setValue:DefaultBetsLimitMonth forKey:@"max_bets_limit_month"];
        }
        
        if ([valuesToReturn valueForKey:@"max_bets_limit_bet"] == nil)
        {
            [valuesToReturn setValue:DefaultBetsLimitBet forKey:@"max_bets_limit_bet"];
        }
        
        NSArray *arrayToReturn = [NSArray arrayWithObject:valuesToReturn];
        
        [self.delegate limitsDownloaded:arrayToReturn];
    }
    else //if ([typeOfService isEqualToString:@"update"])
    {
        // Parse the JSON that came in
        NSError *error;
        NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
        
        NSMutableDictionary *valuesToReturn = [[NSMutableDictionary alloc] init];
        [valuesToReturn setValue:@"false" forKey:@"month"];
        [valuesToReturn setValue:@"false" forKey:@"bet"];
        NSLog(@"1: %@ , %@", [valuesToReturn valueForKey:@"month"], [valuesToReturn valueForKey:@"bet"]);
        
        if ([topLevelArray count] > 0)
        {
            [valuesToReturn setValue:[[topLevelArray objectAtIndex:0] valueForKey:@"month"] forKey:@"month"];
            [valuesToReturn setValue:[[topLevelArray objectAtIndex:1] valueForKey:@"bet"] forKey:@"bet"];
            NSLog(@"2: %@ , %@", [valuesToReturn valueForKey:@"month"], [valuesToReturn valueForKey:@"bet"]);
        }
        NSArray *arrayToReturn = [NSArray arrayWithObject:valuesToReturn];
        
        [self.delegate limitsChangeConfirmed:arrayToReturn];
    }
}

@end
