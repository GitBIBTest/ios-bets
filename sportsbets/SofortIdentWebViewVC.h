//
//  SofortIdentWebViewVC.h
//  Sports Betting by Bet IT Best
//
//  Created by Aleksandar Penev on 21/04/2016.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "AppDelegate.h"
#import "Macros.h"

@interface SofortIdentWebViewVC : UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *sofortIdentWebView;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
- (IBAction)closeWebView:(UIButton *)sender;

@property (strong, nonatomic) NSString *firstname;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *birthday;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *areaCode;
@property (strong, nonatomic) NSString *area;
@property (strong, nonatomic) NSString *houseNumber;

@end
