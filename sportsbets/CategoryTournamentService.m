
//
//  CategoryTournament.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 26.11.15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "CategoryTournamentService.h"

@interface CategoryTournamentService ()
{
    NSMutableData* _downloadedData;
}

@end

@implementation CategoryTournamentService

- (id)init
{
    if (!self)
    {
        self = [super init];
    }
    
    static dispatch_once_t once;
    
    //loads the data once
    dispatch_once(&once, ^
    {
        [self downloadSport];
    });
    return self;
}

- (void)downloadSport
{
    //Download the json file
    NSURL *jsonFileUrl = [NSURL URLWithString:@"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceSport.php"];
    //Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    //Create the NSURLConnection
    _sportConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)downloadCategory
{
    //Download the json file
    NSURL *jsonFileUrl = [NSURL URLWithString:@"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceCategory.php"];
    //Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    //Create the NSURLConnection
    _categoryConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)downloadTournament
{
    //Download the json file
    NSURL *jsonFileUrl = [NSURL URLWithString:@"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceTournament.php"];
    //Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    //Create the NSURLConnection
    _tournamentConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //Append the newly downloaded data
    [self.fileHandle writeData:data];
    [_downloadedData appendData:data];
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //Parse the JSON that came in
    NSError *error;
    NSArray *loadedData = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    //write sport data in local store
    if (connection == _sportConnection)
    {
        for (int i = 0; i < loadedData.count; i++)
        {
            NSDictionary *loadedData_dict = loadedData[i];
            NSEntityDescription *sport = [NSEntityDescription entityForName:@"Sport" inManagedObjectContext:self.managedObjectContext];
            NSManagedObject *newSport = [[NSManagedObject alloc]initWithEntity:sport insertIntoManagedObjectContext:self.managedObjectContext];
            //set attributes for entity Sport
            [newSport setValue:loadedData_dict[@"name"] forKey:@"sport"];
            [newSport setValue:loadedData_dict[@"uid"] forKey:@"uid"];
//            [self.managedObjectContext save:nil];
        }
        [self downloadCategory];
    }
    //write categories in local store
    else if (connection == _categoryConnection)
    {
        for (int i = 0; i < loadedData.count; i++)
        {
            NSDictionary *loadedData_dict = loadedData[i];
            NSEntityDescription *category = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:self.managedObjectContext];
            NSManagedObject *newCategory = [[NSManagedObject alloc]initWithEntity:category insertIntoManagedObjectContext:self.managedObjectContext];
            //set attributes for entity Category
            [newCategory setValue:loadedData_dict[@"name"] forKey:@"category_name"];
            [newCategory setValue:LocalizedString(loadedData_dict[@"name"], nil) forKey:@"translated_category_name"];
            [newCategory setValue:loadedData_dict[@"uid"] forKey:@"uid"];
            [newCategory setValue:loadedData_dict[@"sport_uid"] forKey:@"sport_uid"];
            [self.managedObjectContext save:nil];
            
            //sets relationship between sport and category
            NSPredicate *predicate_sport = [NSPredicate predicateWithFormat:@"(uid LIKE[c] %@)", loadedData_dict[@"sport_uid"]];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Sport"];
            [fetchRequest setPredicate:predicate_sport];
            NSArray *result = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            NSManagedObject* existingSport = [result objectAtIndex:0];
            
            NSMutableSet *sport = [existingSport mutableSetValueForKey:@"sport_has_categories"];
            [sport addObject:newCategory];
//            [self.managedObjectContext save:nil];
        }
        [self downloadTournament];
    }
    //write tournaments in local store
    else
    {
        for (int i = 0; i < loadedData.count; i++)
        {
            NSDictionary *loadedData_dict = loadedData[i];
            NSEntityDescription *tournament = [NSEntityDescription entityForName:@"Tournament" inManagedObjectContext:self.managedObjectContext];
            NSManagedObject *newTournament = [[NSManagedObject alloc]initWithEntity:tournament insertIntoManagedObjectContext:self.managedObjectContext];
            //set attributes for entity Sport
            NSInteger sorting = [loadedData_dict[@"sorting"] integerValue];
            /* give a high value to sorting when it's equal to 0, since 0 is the default value,
             so values that have not been manually set in the database don't take the priority */
            if (sorting == 0)
            {
                sorting = 100;
            }
            [newTournament setValue:loadedData_dict[@"name"] forKey:@"tournament_name"];
            [newTournament setValue:loadedData_dict[@"uid"] forKey:@"uid"];
            [newTournament setValue:loadedData_dict[@"category_uid"] forKey:@"category_uid"];
            [newTournament setValue:[NSNumber numberWithInteger:sorting] forKey:@"sorting"];
//            [self.managedObjectContext save:nil];
        }
        [self notificate];
    }
}

-(void)notificate
{
    [self.managedObjectContext save:nil];
    NSLog(@"Letting the delegate know that I'm done downloading the categories");
    [self.delegate categoriesTournamentsDownloaded];
}

//-(void)testShow
//{
//    NSPredicate *predicate_category = [NSPredicate predicateWithFormat:@"(category_uid LIKE[c] %@)", @"95"];
//    NSError *fetchError = nil;
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Tournament"];
//    [fetchRequest setPredicate:predicate_category];
//    NSArray * sport = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
//    
//    for (int i = 0 ; i < sport.count; i++)
//    {
//        NSString *tournament = [sport[i]valueForKey:@"tournament_name"];
//        // NSLog(@"tournaments = %@", tournament);
//    }
//}

@end