//
//  SportBetsCell.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 01/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "SportBetsCell.h"

@implementation SportBetsCell

@synthesize team1Label, team2Label, matchdateLabel, winner1Button, winner_noButton, winner2Button, hasDraw, label1, labelX, label2;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        
    }
    return self;
}

-(void)layoutSubviews
{
    if (!hasDraw)
    {
        [winner_noButton setHidden:YES];
        [labelX setHidden:YES];
    }
}

@end
