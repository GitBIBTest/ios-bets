//
//  BetProfitCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 09/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "Localization.h"

@interface BetProfitCell : UITableViewCell

@property (nonatomic,retain) UILabel *profitLabel;

-(void)setPotentialProfit:(NSString *)potentialProfit;

@end
