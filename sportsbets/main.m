//
//  main.m
//  sportsbets
//
//  Created by Dummy on 23.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    //int retVal = UIApplicationMain(argc, argv, @"AppDelegate", @"AppDelegate");
}
