//
//  BettingSlipMatchInfoCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 11/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"

@interface BettingSlipMatchInfoCell : UITableViewCell

@property (nonatomic,retain) UILabel *teamNameLabel;
@property (nonatomic,retain) UILabel *matchDateLabel;

@end
