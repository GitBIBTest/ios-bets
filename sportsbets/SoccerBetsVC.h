//
//  SoccerBetsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Dummy on 23.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "BetsService.h"
#import "Localization.h"
#import "BetsObject.h"
#import "BettingSlipVC.h"
#import "SportBetsCell.h"
#import "TextFieldNoCursor.h"
#import "Macros.h"
#import "AllSportsInARowCell.h"
#import "UserData.h"
#import "MBProgressHUD.h"
#import "BBBadgeBarButtonItem.h"
#import "FilterMaster.h"
#import "AccountOverviewService.h"
#import "SoccerBetsSectionTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+UrlEncoding.h"

@interface SoccerBetsVC : UITableViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, BetsServiceProtocol, AllSportsInARowCellProtocol, AccountOverviewServiceProtocol, UIGestureRecognizerDelegate>
{
    int data_retrievable;
    int sport_identifier;
    bool userLoggedIn;
    
    BetsService *betsService;
    UIPickerView *pickerViewBetType;
}

//my account button and login/balance button
@property (strong, nonatomic) IBOutlet UIButton *myAccountButton;
@property (strong, nonatomic) IBOutlet UIButton *balanceButton;

//left group of navigation bar Buttons
@property (strong, nonatomic) IBOutlet UIBarButtonItem *burgerButton;
@property (strong, nonatomic) IBOutlet UIButton *betSlipButton;

//right group of navigation bar Buttons
@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *depositButton;

@property (strong, nonatomic) TextFieldNoCursor *pickerViewBetTypeField;

- (void)betsDownloaded;

@end
