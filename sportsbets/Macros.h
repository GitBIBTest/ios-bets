//
//  Constants.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 01/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

///////////////////////////////////////////
// App
///////////////////////////////////////////

#define APP_VERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]

///////////////////////////////////////////
// Views
///////////////////////////////////////////
#define WIDTH(view) view.frame.size.width
#define HEIGHT(view) view.frame.size.height
#define X(view) view.frame.origin.x
#define Y(view) view.frame.origin.y
#define LEFT(view) view.frame.origin.x
#define TOP(view) view.frame.origin.y
#define BOTTOM(view) (view.frame.origin.y + view.frame.size.height)
#define RIGHT(view) (view.frame.origin.x + view.frame.size.width)

///////////////////////////////////////////
// Device & OS
///////////////////////////////////////////

#define Is4Inches() ([[UIScreen mainScreen] bounds].size.height == 568)
#define Is3_5Inches() ([[UIScreen mainScreen] bounds].size.height == 480.0f)
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

///////////////////////////////////////////
// Networking
///////////////////////////////////////////
#define IsConnected() !([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
#define ShowNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = NO

///////////////////////////////////////////
// Misc
///////////////////////////////////////////
#define URLIFY(urlString) [NSURL URLWithString:urlString]
#define F(string, args...) [NSString stringWithFormat:string, args]
#define ALERT(title, msg) [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]
#define BIBredcolor [UIColor colorWithRed:110/255.0 green:19/255.0 blue:22/255.0 alpha:1]
#define kOFFSET_FOR_KEYBOARD 350
#define IOS_DEFINED_APIKEY @"a@AGa23$!abc9"
#define BIB_ROUNDING_BEHAVIOUR [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES]

#define logIntVariable(x) NSLog( @"Value of %s = %d",#x, x)
#define logStringVariable(x) NSLog( @"Value of %s = %s",#x, x)
#define logFloatVariable(x) NSLog( @"Value of %s = %f",#x, x)

///////////////////////////////////////////
// Defaults
///////////////////////////////////////////
#define DefaultBetsLimitMonth @"1000000"
#define DefaultBetsLimitBet @"1000000"
#define UserPreferenceOddsDisplay [[NSUserDefaults standardUserDefaults] integerForKey:@"OddsDisplay"]