//
//  DepositVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "CDatePickerViewEx.h"
#import "DepositConfirmVC.h"
#import "TextFieldNoCursor.h"
#import "AppDelegate.h"
#import "UserData.h"

@interface DepositVC : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
{
    UIPickerView *methodPickerView;
    UIPickerView *cardTypePickerView;
}

@property (nonatomic,retain) IBOutlet UILabel *amountLabel;
@property (nonatomic,retain) IBOutlet UITextField *amountTextField;
@property (nonatomic,retain) IBOutlet TextFieldNoCursor *methodTextField;

@property (nonatomic,retain) IBOutlet UILabel *ibanLabel;
@property (nonatomic,retain) IBOutlet UITextField *ibanTextField;
@property (nonatomic,retain) IBOutlet UILabel *bicLabel;
@property (nonatomic,retain) IBOutlet UITextField *bicTextField;
@property (nonatomic,retain) IBOutlet UILabel *accountHolderLabel;
@property (nonatomic,retain) IBOutlet UITextField *accountHolderTextField;
@property (nonatomic,retain) IBOutlet UIButton *continue1Button;

@property (nonatomic,retain) IBOutlet UILabel *cardTypeLabel;
@property (nonatomic,retain) IBOutlet TextFieldNoCursor *cardTypeTextField;
@property (nonatomic,retain) IBOutlet UILabel *cardNumberLabel;
@property (nonatomic,retain) IBOutlet UITextField *cardNumberTextField;
@property (nonatomic,retain) IBOutlet UILabel *cardCodeLabel;
@property (nonatomic,retain) IBOutlet UITextField *cardCodeTextField;
@property (nonatomic,retain) IBOutlet UILabel *cardExpirationLabel;
@property (nonatomic,retain) IBOutlet CDatePickerViewEx *cardExpirationPicker;
@property (nonatomic,retain) IBOutlet UILabel *cardOwnerLabel;
@property (nonatomic,retain) IBOutlet UITextField *cardOwnerTextField;
@property (nonatomic,retain) IBOutlet UIButton *continue2Button;

@end
