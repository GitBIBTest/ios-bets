//
//  UserBetsDataArray.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 29/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserBetsDataArray.h"

@implementation UserBetsDataArray

- (id) init
{
    if (!self)
    {
        self = [super init];
    }
    self.userBetsDataArray = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)addUserBetsArray:(UserBetsData *)userBetsDataArray
{
    [self.userBetsDataArray addObject:userBetsDataArray];
}

@end
