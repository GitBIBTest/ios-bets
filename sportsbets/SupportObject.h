//
//  SupportObject.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 25/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SupportObject : NSObject

@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) NSString *categoryUid;
@property (strong, nonatomic) NSDictionary *reasons;

- (id)initWithUID:(NSString*)uid andName:(NSString*)name;

@end
