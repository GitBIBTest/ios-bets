//
//  MatchesHeader.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"

@interface MatchesHeader : UITableViewCell

@property (nonatomic,retain) UILabel *homeTeamLabel;
@property (nonatomic,retain) UILabel *awayTeamLabel;
@property (nonatomic,retain) UILabel *plusMinusLabel;
@property (nonatomic,retain) UIButton *deleteButton;

-(void) updateTeamLabelsWithHomeTeam:(NSString*)homeTeam awayTeam:(NSString*)awayTeam;
-(void) setWinningTeam:(NSInteger)winningTeam;
-(void) displayForCombi;

@end
