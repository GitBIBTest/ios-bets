//
//  AccountOverviewVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 12/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

/*--------------------------------------------------------------------------------
 TODO WHEN THE API IS DONE
 - Downloads data of the user using the API and displays them in labels
--------------------------------------------------------------------------------*/

#import "AccountOverviewVC.h"

@implementation AccountOverviewVC
{
    MBProgressHUD *progressHud;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //configuration of the navigation controller
    self.tabBarController.navigationItem.hidesBackButton = YES;

    self.burgerButton.target = self.revealViewController;
    self.burgerButton.action = @selector(revealToggle:);
    
    self.tabBarController.navigationItem.title = LocalizedString(@"my_account", nil);
    
    [self.closeButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                              [UIFont fontWithName:@"Montserrat-bold" size:30.0], NSFontAttributeName,
                                              [UIColor whiteColor],
                                              NSForegroundColorAttributeName,
                                              nil]
                                    forState:UIControlStateNormal];
    
    self.tabBarController.navigationItem.leftBarButtonItem = self.burgerButton;
    self.tabBarController.navigationItem.rightBarButtonItem = self.closeButton;
    
    //configuration of the tab bar
    self.tabBarItem.title = LocalizedString(@"overview", nil);
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:LocalizedString(@"my_account", nil)];
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:LocalizedString(@"my_bets", nil)];
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:LocalizedString(@"payments", nil)];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIFont fontWithName:@"Montserrat" size:11.0], NSFontAttributeName,
                                                       [UIColor lightGrayColor],
                                                       NSForegroundColorAttributeName,
                                                       nil]
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIFont fontWithName:@"Montserrat-bold" size:10.0], NSFontAttributeName,
                                                       [UIColor whiteColor],
                                                       NSForegroundColorAttributeName,
                                                       nil]
                                             forState:UIControlStateSelected];
    
    
    /*-----------------------------------------------------------------------
    // TODO add custom images to the tabbar, someone has to do them though //
    -----------------------------------------------------------------------*/
    
    //configuration of the tableview
    UIEdgeInsets adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight(self.tabBarController.tabBar.frame), 0);
    self.tableView.contentInset = adjustForTabbarInsets;
    self.tableView.scrollIndicatorInsets = adjustForTabbarInsets;
    
    self.tableView.separatorColor = [UIColor clearColor];
    
    self.tableView.allowsSelection = NO;
    
    NSArray *titleLabelsText = @[LocalizedString(@"account_balance", nil),
                                 LocalizedString(@"account_credit", nil),
                                 LocalizedString(@"bet_limits", nil),
                                 LocalizedString(@"proof_of_liquidity", nil),
                                 LocalizedString(@"blacklist_entries", nil),
                                 ];
    
    self.titleLabel1.text = titleLabelsText[0];
    self.titleLabel2.text = titleLabelsText[1];
    self.titleLabel3.text = titleLabelsText[2];
    self.titleLabel4.text = titleLabelsText[3];
    self.titleLabel5.text = titleLabelsText[4];
}

- (void)viewWillAppear:(BOOL)animated
{
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHud.labelText = @"Loading user info";
    
    UserData *userData = [UserData getInstance];
    
    AccountOverviewService *accountOverviewService = [[AccountOverviewService alloc] init];
    accountOverviewService.delegate = self;
    
    [accountOverviewService loadAccountOverviewForUser:userData.userId andForPassword:userData.passwordEncrypted];
}

- (void)accountOverviewDownloaded:(NSArray *)userInfo
{
    NSString *currency = @"€";
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setGroupingSeparator:[[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator]];
    
    //account balance
    NSString *accountBalanceString = F(@"%.2f", [[[userInfo objectAtIndex:0] valueForKey:@"account_balance"] floatValue]);
    self.displayLabel1.text = F(@"%@ %@", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[accountBalanceString floatValue]]], currency);
    
    //account credit
    NSString *accountCreditString = F(@"%.2f", [[[userInfo objectAtIndex:0] valueForKey:@"account_credit"] floatValue]);
    self.displayLabel2.text = F(@"%@ %@", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[accountCreditString floatValue]]], currency);
    
    //limit per month
    NSString *betsLimitMonthString = F(@"%.2f", [[[userInfo objectAtIndex:0] valueForKey:@"bets_limit_month"] floatValue]);
    self.displayLabel3.text = F(@"%@ %@", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[betsLimitMonthString floatValue]]], currency);
    
    //limit per bet
    NSString *betsLimitBetString = F(@"%.2f", [[[userInfo objectAtIndex:0] valueForKey:@"bets_limit_bet"] floatValue]);
    self.displayLabel4.text = F(@"%@ %@", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[betsLimitBetString floatValue]]], currency);
    
    //liquidity proof
    NSString *liquidity_proof = [[userInfo objectAtIndex:0] valueForKey:@"liquidity_proved"];
    if ([liquidity_proof isEqual:[NSNull null]])
    {
        self.displayLabel5.text = LocalizedString(@"liquidity_proof_null", nil);
    }
    else if ([liquidity_proof isEqualToString:@"liquidity-verification"])
    {
        self.displayLabel5.text = LocalizedString(@"liquidity_proof_in_verification", nil);
    }
    else
    {
        self.displayLabel5.text = liquidity_proof;
    }
    
    //blacklist entries
    self.displayLabel6.text = LocalizedString(@"empty", nil);
    
    [progressHud setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [progressHud setHidden:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 12;
    }
    else if ([@[@2, @4, @6, @7, @9, @11] containsObject:[NSNumber numberWithInteger:indexPath.row]])
    {
        return 30;
    }
    else
    {
        return 44;
    }
}

- (IBAction)closePressed:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
