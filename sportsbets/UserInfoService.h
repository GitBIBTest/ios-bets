//
//  UserInfoService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 18/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@protocol UserInfoServiceProtocol <NSObject>

-(void)userInfoDownloaded:(NSArray *)userInfo;

@end

@interface UserInfoService : NSObject

@property (nonatomic,weak) id <UserInfoServiceProtocol> delegate;

-(void)loadUserInfoForUser:(NSString *)user andForPassword:(NSString *)password;

@end
