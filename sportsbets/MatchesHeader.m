//
//  MatchesHeader.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "MatchesHeader.h"

@implementation MatchesHeader
{
    UIView *separatorLineView;
}

@synthesize homeTeamLabel, awayTeamLabel, plusMinusLabel, deleteButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        CGFloat cellHeight = HEIGHT(self);
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat leadingSpace = 15.0;
        CGFloat trailingSpace = 15.0;
        CGFloat deleteWidth = 50.0;
        CGFloat deleteOrigin = screenWidth - trailingSpace - deleteWidth;
        CGFloat plusMinusWidth = 25.0;
        CGFloat spacing = 10.0;
        CGFloat teamLabelOrigin = leadingSpace + plusMinusWidth + spacing;
        CGFloat teamLabelWidth = deleteOrigin - teamLabelOrigin - spacing;
        
        [self setBackgroundColor:BIBredcolor];
        
        homeTeamLabel = [[UILabel alloc] initWithFrame:CGRectMake(teamLabelOrigin, 5.0, teamLabelWidth, 15.0)];
        [homeTeamLabel setTextColor:[UIColor whiteColor]];
        [homeTeamLabel setFont:[UIFont fontWithName:@"Montserrat" size:13.0]];
        [homeTeamLabel setNumberOfLines:0];
        
        awayTeamLabel = [[UILabel alloc] initWithFrame:CGRectMake(teamLabelOrigin, 20.0, teamLabelWidth, 15.0)];
        [awayTeamLabel setTextColor:[UIColor whiteColor]];
        [awayTeamLabel setFont:[UIFont fontWithName:@"Montserrat" size:13.0]];
        [awayTeamLabel setNumberOfLines:0];
        
        plusMinusLabel = [[UILabel alloc] initWithFrame:CGRectMake(leadingSpace, 0.0, teamLabelWidth, cellHeight)];
        [plusMinusLabel setFont:[UIFont fontWithName:@"Montserrat" size:25.0]];
        [plusMinusLabel setText:@"+"];
        [plusMinusLabel setTextColor:[UIColor whiteColor]];
        
        deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(deleteOrigin, 0.0, deleteWidth, cellHeight)];
        [deleteButton.titleLabel setFont:[UIFont fontWithName:@"Montserrat" size:25.0]];
        [deleteButton setBackgroundColor:[UIColor clearColor]];
        [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [deleteButton setTitle:@"x" forState:UIControlStateNormal];
        [deleteButton.titleLabel setTextAlignment:NSTextAlignmentRight];
        
        //show custom separator line
        separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, cellHeight-1.0, screenWidth - 20.0, 1.0/[UIScreen mainScreen].scale)];
        separatorLineView.backgroundColor = [UIColor whiteColor];
        separatorLineView.tag = 999;
        
        [self.contentView addSubview:homeTeamLabel];
        [self.contentView addSubview:awayTeamLabel];
        [self.contentView addSubview:plusMinusLabel];
        [self.contentView addSubview:deleteButton];
        [self addSubview:separatorLineView];
    }
    return self;
}

- (void)updateTeamLabelsWithHomeTeam:(NSString*)homeTeam awayTeam:(NSString*)awayTeam
{
    [homeTeamLabel setText: homeTeam];
    [awayTeamLabel setText: awayTeam];
}

- (void)setWinningTeam:(NSInteger)winningTeam
{
    switch (winningTeam) {
        case 1:
        {
            NSMutableAttributedString *winningTeamText = [[NSMutableAttributedString alloc] initWithString:homeTeamLabel.text];
            [winningTeamText addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0, [homeTeamLabel.text length]}];
            [homeTeamLabel setAttributedText:winningTeamText];
        }
            break;
        case 2:
        {
            NSMutableAttributedString *winningTeamText = [[NSMutableAttributedString alloc] initWithString:awayTeamLabel.text];
            [winningTeamText addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0, [awayTeamLabel.text length]}];
            [awayTeamLabel setAttributedText:winningTeamText];
        }
            break;
    }
}

- (void)displayForCombi
{
    [plusMinusLabel setHidden:YES];
    [separatorLineView setHidden:YES];
}

@end
