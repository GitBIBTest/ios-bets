//
//  RegisterService.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 26/11/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "NSString+UrlEncoding.h"

@protocol RegisterServiceProtocol <NSObject>

-(void)registerOutputReceived:(NSArray *)userInfo;
-(void)finalizationOutputReceived:(NSArray *)returnData;

@end

@interface RegisterService : NSObject

@property (nonatomic,weak) id <RegisterServiceProtocol> delegate;

-(void)registerUserWithUserData:(NSArray *)registerUserData;
-(void)finalizeUserWithUsername:(NSString *)username andPassword:(NSString *)password regHash:(NSString *)regHash;

@end
