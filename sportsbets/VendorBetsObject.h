//
//  VendorBetstObject.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "VendorBetsData.h"

@interface VendorBetsObject : VendorBetsData

+ (VendorBetsObject*) sharedVendorBetsObject;

@end