//
//  LoginVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 24.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Localization.h"
#import "SWRevealViewController.h"
#import "Macros.h"
@class LoginService;

//to test the registration finalization
#import "RegisterService.h"

@interface LoginVC : UITableViewController <UITextFieldDelegate>
{
    LoginService *loginService;
}

@property (nonatomic,retain) IBOutlet UILabel *userLabel;
@property (nonatomic,retain) IBOutlet UITextField *userTextField;
@property (nonatomic,retain) IBOutlet UILabel *passwordLabel;
@property (nonatomic,retain) IBOutlet UITextField *passwordTextField;
@property (nonatomic,retain) IBOutlet UIButton *login;
@property (nonatomic,retain) IBOutlet UIButton *passwordForgottenButton;
@property (nonatomic,retain) IBOutlet UIButton *registerButton;

@end
