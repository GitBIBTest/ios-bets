//
//  BettingSlipVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 02/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "BettingSlipVC.h"

//deletebuttons have tag 100 + numberOfSection

@implementation BettingSlipVC
{
    NSInteger betslipBetType;
    NSMutableArray *vendorsPerBet;
    NSMutableArray *selectedVendorPerMatch;
    NSMutableArray *stakeForBet;
    NSMutableArray *rateForBet;
    NSInteger currentlyDownloadingMatch;
    MBProgressHUD *progressHud;
    NSDateFormatter *dateFormatter;
    NSInteger numberOfSections;
    NSMutableArray *boolArray;
    bool expandingAllSections;
    NSArray *vendorsPerCombi;
    NSMutableString *selectedVendorPerCombi;
    NSMutableString *rateForCombi;
    NSMutableString *stakeForCombi;
    
    NSInteger userWantsToPlaceBet;
    bool betJustPlaced;
}

@synthesize matches, vendor, rate;

- (void)viewDidLoad
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setAllowsSelection:YES];
    
    self.burgerButton.target = self.revealViewController;
    self.burgerButton.action = @selector(revealToggle:);
    
    self.navigationItem.hidesBackButton = YES;
    
    [self.closeButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                              [UIFont fontWithName:@"Montserrat-bold" size:30.0], NSFontAttributeName,
                                              [UIColor whiteColor],
                                              NSForegroundColorAttributeName,
                                              nil]
                                    forState:UIControlStateNormal];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    self.navigationItem.title = LocalizedString(@"betslip", nil);
    
    betslipBetType = 1;
    
    //Register classes of custom cells
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"BetTypeHeaderView"];
    [self.tableView registerClass:[BetTypeHeader class] forCellReuseIdentifier:@"BetTypeHeader"];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"MatchesHeaderView"];
    [self.tableView registerClass:[MatchesHeader class] forCellReuseIdentifier:@"MatchesHeader"];
    [self.tableView registerClass:[BettingSlipMatchInfoCell class] forCellReuseIdentifier:@"BettingSlipMatchInfoCell"];
    [self.tableView registerClass:[BettingSlipVendorCell class] forCellReuseIdentifier:@"BettingSlipVendorCell"];
    [self.tableView registerClass:[BetStakeCell class] forCellReuseIdentifier:@"BetStakeCell"];
    [self.tableView registerClass:[BetProfitCell class] forCellReuseIdentifier:@"BetProfitCell"];
    [self.tableView registerClass:[PlaceBetButtonCell class] forCellReuseIdentifier:@"PlaceBetButtonCell"];
    [self.tableView registerClass:[PlaceAllBetsCell class] forCellReuseIdentifier:@"PlaceAllBetsCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    betJustPlaced = NO;
    [self checkValidMatches];
    
    //this triggers the automatic procedure to open all sections by default
    [self automaticExpandAllSections];
    
    [self.tableView reloadData];
}

- (void)checkValidMatches
{
    //request data for matches in betting slip from local store
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"BettingSlipMatches"];
    NSError *fetchError = nil;
    
    //Execute Fetch Request
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    NSString * formatted_date = LocalizedString(@"date", nil);
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatted_date];
    
    matches = [[NSMutableArray alloc] init];
    
    for (NSManagedObject *obj in fetchResult) {
        NSDate *objDate = [dateFormatter dateFromString:[obj valueForKey:@"match_date"]];
        NSLog(@"date now %@ obj %@", [NSDate date], objDate);
        if ([[NSDate date] compare:objDate] != NSOrderedAscending)
        {
            [self.managedObjectContext deleteObject:obj];
            [self.managedObjectContext save:nil];
        }
        else
        {
            [matches addObject:obj];
        }
    }
    
    vendorsPerBet = [[NSMutableArray alloc] init];
    for (int i = 0; i < [matches count]; i++)
    {
        [vendorsPerBet addObject:[NSNull null]];
    }
    
    selectedVendorPerMatch = [[NSMutableArray alloc] init];
    for (int i = 0; i < [matches count]; i++)
    {
        selectedVendorPerMatch[i] = @"1";
    }
    
    rateForBet = [[NSMutableArray alloc] init];
    for (int i = 0; i < [matches count]; i++)
    {
        rateForBet[i] = [matches[i] valueForKey:@"selected_rate_value"];
    }
    
    stakeForBet = [[NSMutableArray alloc] init];
    for (int i = 0; i < [matches count]; i++)
    {
        //default stake 5 euro
        stakeForBet[i] = @"5";
    }
    
    vendorBetsService = [[VendorBetsService alloc]init];
    vendorBetsService.delegate = self;
    
    boolArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [matches count]; i++)
    {
        [boolArray addObject:[NSNumber numberWithBool:NO]];
    }
    
    selectedVendorPerCombi = [[NSMutableString alloc] initWithString:@"1"];
    stakeForCombi = [[NSMutableString alloc] initWithString:@"5"];
}

- (void)automaticExpandAllSections
{
    if ([matches count] > 0)
    {
        expandingAllSections = YES;
        [self downloadMatchInPosition:1];
    }
    else
    {
        [self.tableView reloadData];
    }
}

- (void)downloadMatchInPosition:(NSInteger)position
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:position];
    
    BOOL collapsed  = [[boolArray objectAtIndex:indexPath.section - 1] boolValue];
    BOOL aboutToShowData = !collapsed;
    
    //In case the sections are expanding, these values will get changed all together when all the data is downloaded
    //(see function vendorBetsDownloadedForMatch:(VendorBetsData *)vendorBetsDataForMatch)
    if (!expandingAllSections)
    {
        [boolArray replaceObjectAtIndex:indexPath.section - 1 withObject:[NSNumber numberWithBool:!collapsed]];
    }
    
    //download live data
    if (aboutToShowData)
    {
        currentlyDownloadingMatch = position;
        
        progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHud.labelText = F(@"Loading data for match %d", (int)position);
        
        UserData *userData = [UserData getInstance];
        
        NSString *selected_match = [[matches objectAtIndex:indexPath.section - 1] valueForKey:@"match_id"];
        NSString *selected_team = [[matches objectAtIndex:indexPath.section - 1] valueForKey:@"selected_rate"];
        
        if (userData.userId == nil)
        {
            [vendorBetsService loadVendorBetsForMatch:selected_match andSelectedTeam:selected_team];
        }
        else
        {
            [vendorBetsService loadVendorBetsForMatch:selected_match andSelectedTeam:selected_team forUser:userData.userId];
        }
    }
    //collapse section without downloading live data
    else
    {
        [self.tableView reloadData];
    }
}

-(void)vendorBetsDownloadedForMatch:(VendorBetsData *)vendorBetsDataForMatch
{
    if (betslipBetType == 1)
    {
        [vendorsPerBet replaceObjectAtIndex:currentlyDownloadingMatch-1 withObject:vendorBetsDataForMatch.vendorArray];
        
        [progressHud hide:YES];
        
        if (expandingAllSections && currentlyDownloadingMatch < [matches count])
        {
            [self downloadMatchInPosition:currentlyDownloadingMatch+1];
        }
        else
        {
            if (expandingAllSections)
            {
                for (int i = 0; i < [boolArray count]; i++)
                {
                    [boolArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
                }
                expandingAllSections = FALSE;
            }
        }
    }
    else if (betslipBetType == 2)
    {
        [progressHud hide:YES];
        
        vendorsPerCombi = vendorBetsDataForMatch.vendorArray;
    }
    else if (betslipBetType == 3)
    {
        //TODO
    }
    [self.tableView reloadData];
}

#pragma mark - Table View Delegate

//show as many sections as matches are in array
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([matches count] == 0)
    {
        numberOfSections = 1;
    }
    //number of matches plus header with buttons for selection of betslipBetType plus section for total profit and actually placing the bet
    else if (betslipBetType == 1)
    {
        numberOfSections = [matches count] + 2;
    }
    else if (betslipBetType == 2)
    {
        numberOfSections = 3;
    }
    else // betslipBetType == 3
    {
        //TODO
        numberOfSections = 1;
    }
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //section for selection of bet type without rows, only the cell "einzel, kombi, system"
    if (section == 0)
    {
        if ([matches count] == 0)
        {
            return 1;
        }
        return 0;
    }
    //number of Rows = number of bets per match plus first row with information, plus row for setting stake, plus cell with potential profit
    else
    {
        if (betslipBetType == 1)
        {
            if (section < numberOfSections-1)
            {
                if ([[boolArray objectAtIndex:section-1] boolValue])
                {
                    return [[vendorsPerBet objectAtIndex:section-1] count]+4;
                }
                else
                {
                    return 0;
                }
            }
            else //if (section >= numberOfSections-1)
            {
                return 0; //return 1 shows the "place all bets" row (only row in this section)
            }
        }
        else if (betslipBetType == 2)
        {
            if (section == 1)
            {
                return [vendorsPerCombi count]+[matches count];
            }
            else //if (section == 2)
            {
                if ([vendorsPerCombi count] == 0)
                {
                    return 1;
                }
                else
                {
                    return 3;
                }
            }
        }
        else // betslipBetType == 3
        {
            //TODO
            return 0;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([matches count] == 0)
    {
        return 120;
    }
    else
    {
        if (betslipBetType == 1)
        {
            if (indexPath.section > 0 && indexPath.section < numberOfSections-1 && [[boolArray objectAtIndex:indexPath.section-1] boolValue])
            {
                if (indexPath.row == 8)
                {
                    return 50;
                }
                else
                {
                    return 40;
                }
            }
            else if (indexPath.section >= numberOfSections-1)
            {
                return 100;
            }
        }
        else if (betslipBetType == 2)
        {
            if (indexPath.section == 2 && indexPath.row == 0 && [vendorsPerCombi count] == 0)
            {
                return 120;
            }
            else
            {
                return 40;
            }
        }
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (betslipBetType == 1)
    {
        if (section < numberOfSections-1 || numberOfSections == 1)
        {
            return 44.0;
        }
        else
        {
            return 0;
        }
    }
    else if (betslipBetType == 2)
    {
        if (section == 0)
        {
            return 44.0;
        }
        else
        {
            return 0;
        }
    }
    else //if (betslipBetType == 3)
    {
        if (section == 0)
        {
            return 44.0;
        }
        else
        {
            return 0;
        }
    }
}

#pragma mark - Table View Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (betslipBetType == 1)
    {
        if ([matches count] == 0)
        {
            UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@"EmptyBetslipCell"];
            
            UILabel *emptyBetslipLabel = (UILabel *)[myCell viewWithTag:101];
            emptyBetslipLabel.text = LocalizedString(@"empty_betslip", nil);
            return myCell;
        }
        else
        {
            if (indexPath.section < numberOfSections-1)
            {
                BOOL manyCells  = [[boolArray objectAtIndex:indexPath.section-1] boolValue];
                
                //if the section's supposed to be closed, no cell is visible
                if (manyCells)
                {
                    //match info cell
                    if (indexPath.row == 0)
                    {
                        NSString *cellIdentifier = @"BettingSlipMatchInfoCell";
                        BettingSlipMatchInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        
                        NSString *selectedRateForMatch = [[matches objectAtIndex:indexPath.section-1] valueForKey:@"selected_rate"];
                        
                        if ([selectedRateForMatch isEqualToString:@"1"])
                        {
                            cell.teamNameLabel.text = F(@"%@ %@", [[matches objectAtIndex:indexPath.section-1] valueForKey:@"hometeam"], LocalizedString(@"wins", nil));
                        }
                        else if ([selectedRateForMatch caseInsensitiveCompare:@"x"] == NSOrderedSame)
                        {
                            cell.teamNameLabel.text = F(@"%@", LocalizedString(@"draw", nil));
                        }
                        else //if ([selectedRateForMatch isEqualToString:@"2"])
                        {
                            cell.teamNameLabel.text = F(@"%@ %@", [[matches objectAtIndex:indexPath.section-1] valueForKey:@"awayteam"], LocalizedString(@"wins", nil));
                        }
                        
                        cell.matchDateLabel.text = [[matches objectAtIndex:indexPath.section-1] valueForKey:@"match_date"];
                        
                        return cell;
                    }
                    //vendors cells
                    else if (indexPath.row <= [[vendorsPerBet objectAtIndex:indexPath.section-1] count])
                    {
                        NSString *cellIdentifier = @"BettingSlipVendorCell";
                        BettingSlipVendorCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        
                        NSInteger selectedVendor = [[selectedVendorPerMatch objectAtIndex:indexPath.section-1] intValue];
                        NSString *currentVendorRate = F(@"%.2f",[[[[vendorsPerBet objectAtIndex:indexPath.section-1] objectAtIndex:indexPath.row-1] valueForKey:@"rate"] doubleValue]);
                        
                        if (indexPath.row == selectedVendor)
                        {
                            [cell selectCell];
                            [rateForBet replaceObjectAtIndex:indexPath.section-1 withObject:currentVendorRate];
                        }
                        else
                        {
                            [cell deselectCell];
                        }
                        
                        cell.vendorName.text = [[[vendorsPerBet objectAtIndex:indexPath.section-1] objectAtIndex:indexPath.row-1] valueForKey:@"vendor"];
                        [cell.vendorRateButton setTitle:currentVendorRate forState:UIControlStateNormal];
                        [cell.vendorRateButton addTarget:self action:@selector(rateButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                        [cell setLogo:[[[vendorsPerBet objectAtIndex:indexPath.section-1] objectAtIndex:indexPath.row-1] valueForKey:@"vendor_logo"]];
                        
                        return cell;
                    }
                    //cell for setting the stake
                    else if (indexPath.row == [[vendorsPerBet objectAtIndex:indexPath.section-1] count] + 1)
                    {
                        NSString *cellIdentifier = @"BetStakeCell";
                        BetStakeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        
                        cell.stakeTextField.delegate = self;
                        
                        double currentStake = [[stakeForBet objectAtIndex:indexPath.section-1] doubleValue];
                        
                        cell.stakeTextField.text = currentStake != 0 ? F(@"%.2f", currentStake) : @"";
                        [cell.stakeTextField addTarget:self
                                                action:@selector(textFieldDidChange:)
                                      forControlEvents:UIControlEventEditingChanged];
                        
                        [cell setTaxes];
                        
                        //set the inputAccessoryView
                        UIToolbar *toolBarStakeTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 44)];
                        toolBarStakeTextField.barStyle = UIBarStyleBlackOpaque;
                        UIBarButtonItem *stakeTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                                  target:self
                                                                                                                  action:@selector(stakeTextFieldDoneTouched:)];
                        [toolBarStakeTextField setItems:[NSArray arrayWithObjects:
                                                         [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                                       target:nil
                                                                                                       action:nil],
                                                         stakeTextFieldDoneButton, nil]];
                        [cell.stakeTextField setInputAccessoryView:toolBarStakeTextField];
                        
                        return cell;
                    }
                    //cell for showing the potential profit
                    else if (indexPath.row == [[vendorsPerBet objectAtIndex:indexPath.section-1] count] + 2)
                    {
                        NSString *cellIdentifier = @"BetProfitCell";
                        BetProfitCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        
                        double betRate = [[rateForBet objectAtIndex:indexPath.section-1] doubleValue];
                        double betStake = [[stakeForBet objectAtIndex:indexPath.section-1] doubleValue];
                        
                        double potentialProfit = betRate * betStake;
                        
                        [cell setPotentialProfit:F(@"%.2f",potentialProfit)];
                        
                        return cell;
                    }
                    else //if (indexPath.row == [[vendorsPerBet objectAtIndex:indexPath.section-1] count] + 3)
                    {
                        NSString *cellIdentifier = @"PlaceBetButtonCell";
                        
                        PlaceBetButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        cell.placeBetButton.tag = 100+indexPath.section;
                        [cell.placeBetButton removeTarget:nil
                                           action:NULL
                                 forControlEvents:UIControlEventAllEvents];
                        [cell.placeBetButton addTarget:self action:@selector(placeBetButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        
                        return cell;
                    }
                }
                else
                {
                    UITableViewCell *nullCell = [tableView dequeueReusableCellWithIdentifier:@""];
                    return nullCell;
                }
            }
            else //if (indexPath.section == numberOfSections-1)
            {
                NSString *cellIdentifier = @"PlaceAllBetsCell";
                PlaceAllBetsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                
                double totalProfit = 0;
                double totalStake = 0;
                
                for (int i = 0; i < [stakeForBet count]; i++)
                {
                    double betRate = [[rateForBet objectAtIndex:i] doubleValue];
                    double betStake = [[stakeForBet objectAtIndex:i] doubleValue];
                    totalProfit += betRate * betStake;
                    totalStake += betStake;
                }
                
                [cell setTotalPotentialProfit:F(@"%.2f",totalProfit) forStake:F(@"%.2f",totalStake)];
                
                [cell.placeAllBetsButton removeTarget:nil
                                   action:NULL
                         forControlEvents:UIControlEventAllEvents];
                [cell.placeAllBetsButton addTarget:self action:@selector(placeAllBetsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                if ([matches count] == 1)
                {
                    [cell setHidden:YES];
                }
                
                return cell;
            }
        }
    }
    else if (betslipBetType == 2)
    {
        if ([matches count] == 0)
        {
            UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@"EmptyBetslipCell"];
            
            UILabel *emptyBetslipLabel = (UILabel *)[myCell viewWithTag:101];
            emptyBetslipLabel.text = LocalizedString(@"empty_betslip", nil);
            return myCell;
        }
        else
        {
            if (indexPath.section == 1)
            {
                if (indexPath.row < [matches count])
                {
                    NSString *cellIdentifier = @"MatchesHeader";
                    MatchesHeader *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                    
                    NSString *homeTeam = [[matches objectAtIndex:indexPath.row] valueForKey:@"hometeam"];
                    NSString *awayTeam = [[matches objectAtIndex:indexPath.row] valueForKey:@"awayteam"];
                    
                    [cell updateTeamLabelsWithHomeTeam:homeTeam awayTeam:awayTeam];
                    
                    [cell setWinningTeam:[[[matches objectAtIndex:indexPath.row] valueForKey:@"selected_rate"] intValue]];
                    
                    
                    cell.deleteButton.tag = 100+indexPath.row+1;
                    [cell.deleteButton removeTarget:nil
                                       action:NULL
                             forControlEvents:UIControlEventAllEvents];
                    [cell.deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell displayForCombi];
                    
                    return cell;
                }
                //vendors cells
                else //if (indexPath.row < [matches count] + [vendorsPerCombi count])
                {
                    NSString *cellIdentifier = @"BettingSlipVendorCell";
                    BettingSlipVendorCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

                    NSInteger selectedVendor = [selectedVendorPerCombi intValue];
                    NSString *currentVendorRate = F(@"%.2f", [[[vendorsPerCombi objectAtIndex:indexPath.row-[matches count]] valueForKey:@"rate"] doubleValue]);
                    
                    if (indexPath.row-[matches count]+1 == selectedVendor)
                    {
                        [cell selectCell];
                        rateForCombi = [currentVendorRate mutableCopy];
                    }
                    else
                    {
                        [cell deselectCell];
                    }

                    cell.vendorName.text = [[vendorsPerCombi objectAtIndex:indexPath.row-[matches count]] valueForKey:@"vendor"];
                    [cell.vendorRateButton setTitle:currentVendorRate forState:UIControlStateNormal];
                    [cell.vendorRateButton addTarget:self action:@selector(rateButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                    [cell setLogo:[[vendorsPerCombi objectAtIndex:indexPath.row-[matches count]] valueForKey:@"vendor_logo"]];
                    
                    return cell;
                }
            }
            else //if (indexPath.section == 2)
            {
                //cell for setting the stake
                if (indexPath.row == 0)
                {
                    if ([vendorsPerCombi count] == 0)
                    {
                        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoCombiBetCell"];
                        
                        UILabel *noCombiLabel = (UILabel *)[cell viewWithTag:101];
                        
                        noCombiLabel.text = LocalizedString(@"no_combi_bet", nil);
                        noCombiLabel.numberOfLines = 0;
                        
                        return cell;
                    }
                    else
                    {
                        NSString *cellIdentifier = @"BetStakeCell";
                        BetStakeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                        
                        cell.stakeTextField.delegate = self;
                        
                        double currentStake = [stakeForCombi doubleValue];
                        
                        cell.stakeTextField.text = currentStake != 0 ? F(@"%.2f", currentStake) : @"";
                        [cell setTaxes];
                        
                        //place the "done" button
                        UIToolbar *toolBarStakeTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 44)];
                        toolBarStakeTextField.barStyle = UIBarStyleBlackOpaque;
                        UIBarButtonItem *stakeTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                                  target:self
                                                                                                                  action:@selector(stakeTextFieldDoneTouched:)];
                        [toolBarStakeTextField setItems:[NSArray arrayWithObjects:
                                                         [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                                       target:nil
                                                                                                       action:nil],
                                                         stakeTextFieldDoneButton, nil]];
                        [cell.stakeTextField setInputAccessoryView:toolBarStakeTextField];
                        
                        return cell;
                    }
                }
                //cell for showing the potential profit
                else if (indexPath.row == 1)
                {
                    NSString *cellIdentifier = @"BetProfitCell";
                    BetProfitCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                    
                    double betRate = [rateForCombi doubleValue];
                    double betStake = [stakeForCombi doubleValue];
                    
                    double potentialProfit = betRate * betStake;
                    
                    [cell setPotentialProfit:F(@"%.2f",potentialProfit)];
                    
                    return cell;
                }
                else //if (indexPath.row == 2)
                {
                    NSString *cellIdentifier = @"PlaceBetButtonCell";
                    
                    PlaceBetButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                    [cell.placeBetButton removeTarget:nil
                                       action:NULL
                             forControlEvents:UIControlEventAllEvents];
                    [cell.placeBetButton addTarget:self action:@selector(placeAllBetsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    
                    return cell;
                }
            }
        }
    }
    else // betslipBetType == 3 //TODO
    {
        if ([matches count] == 0)
        {
            UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@"EmptyBetslipCell"];
            
            UILabel *emptyBetslipLabel = (UILabel *)[myCell viewWithTag:101];
            emptyBetslipLabel.text = LocalizedString(@"empty_betslip", nil);
            return myCell;
        }
        else
        {
            UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@""];
            return myCell;
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        NSString *headerViewIdentifier = @"BetTypeHeaderView";
        UITableViewHeaderFooterView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerViewIdentifier];
        
        //show 3 buttons for selection of betslipBetType
        NSString *cellIdentifier = @"BetTypeHeader";
        BetTypeHeader *myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        myCell.delegate = self;
        
        [myCell setFrame:CGRectMake(0, 0, tableView.frame.size.width, myCell.frame.size.height)];
        
        if (betslipBetType == 1)
        {
            [myCell.betType1 setTitleColor:myCell.activeColor forState:UIControlStateNormal];
            [myCell.betType1 setUserInteractionEnabled:NO];
            [myCell.betType2 setTitleColor:myCell.inactiveColor forState:UIControlStateNormal];
            [myCell.betType2 setUserInteractionEnabled:YES];
            [myCell.betType3 setTitleColor:myCell.inactiveColor forState:UIControlStateNormal];
            [myCell.betType3 setUserInteractionEnabled:YES];
        }
        else if (betslipBetType == 2)
        {
            [myCell.betType1 setTitleColor:myCell.inactiveColor forState:UIControlStateNormal];
            [myCell.betType1 setUserInteractionEnabled:YES];
            [myCell.betType2 setTitleColor:myCell.activeColor forState:UIControlStateNormal];
            [myCell.betType2 setUserInteractionEnabled:NO];
            [myCell.betType3 setTitleColor:myCell.inactiveColor forState:UIControlStateNormal];
            [myCell.betType3 setUserInteractionEnabled:YES];
        }
        else //if (betslipBetType == 3)
        {
            [myCell.betType1 setTitleColor:myCell.inactiveColor forState:UIControlStateNormal];
            [myCell.betType1 setUserInteractionEnabled:YES];
            [myCell.betType2 setTitleColor:myCell.inactiveColor forState:UIControlStateNormal];
            [myCell.betType2 setUserInteractionEnabled:YES];
            [myCell.betType3 setTitleColor:myCell.activeColor forState:UIControlStateNormal];
            [myCell.betType3 setUserInteractionEnabled:NO];
        }
        
        [sectionView addSubview:myCell];
        
        return  sectionView;
    }
    else if (section < numberOfSections-1)
    {
        if (betslipBetType == 1)
        {
            NSString *headerViewIdentifier = @"MatchesHeaderView";
            UITableViewHeaderFooterView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerViewIdentifier];
            
            sectionView.tag=section;

            NSString *cellIdentifier = @"MatchesHeader";
            MatchesHeader *myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            [myCell setFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
            
            NSString *homeTeam = [[matches objectAtIndex:section-1] valueForKey:@"hometeam"];
            NSString *awayTeam = [[matches objectAtIndex:section-1] valueForKey:@"awayteam"];
            
            [myCell updateTeamLabelsWithHomeTeam:homeTeam awayTeam:awayTeam];
            
            [myCell setWinningTeam:[[[matches objectAtIndex:section-1] valueForKey:@"selected_rate"] intValue]];
            
            myCell.deleteButton.tag = 100+section;
            [myCell.deleteButton removeTarget:nil
                               action:NULL
                     forControlEvents:UIControlEventAllEvents];
            [myCell.deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([[boolArray objectAtIndex:section-1] boolValue])
            {
                myCell.plusMinusLabel.text = @"-";
            }
            
            if (section == numberOfSections - 2)
            {
                [[myCell viewWithTag:999] setHidden:YES];
            }

            //add gesture recognizer to headercell, so header becomes clickable
            UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionTapped:)];
            [sectionView addGestureRecognizer:headerTapped];
            
            [sectionView addSubview:myCell];
            
            return sectionView;
        }
        else if (betslipBetType == 2)
        {
            UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@""];
            return myCell;
        }
        else // betslipBetType == 3
        {
            //TODO
            UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@""];
            return myCell;
        }
    }
    else
    {
        return nil;
    }
}

- (IBAction)sectionTapped:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    [self downloadMatchInPosition:recognizer.view.tag];
}

- (void)rateButtonTouched:(id)sender
{
    BettingSlipVendorCell *cell = (BettingSlipVendorCell *)[(UIView *)sender superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (betslipBetType == 1)
    {
        if (indexPath.section != 0)
        {
            [selectedVendorPerMatch replaceObjectAtIndex:indexPath.section-1 withObject:F(@"%d",(int)indexPath.row)];
            [rateForBet replaceObjectAtIndex:indexPath.section-1 withObject:[[[vendorsPerBet objectAtIndex:indexPath.section-1] objectAtIndex:indexPath.row-1] valueForKey:@"rate"]];
            [self.tableView reloadData];
        }
    }
    else if (betslipBetType == 2)
    {
        selectedVendorPerCombi = [F(@"%d",(int)(indexPath.row-[matches count]+1)) mutableCopy];
        rateForCombi = [[vendorsPerCombi objectAtIndex:indexPath.row-[matches count]] valueForKey:@"rate"];
        [self.tableView reloadData];
    }
}

- (void)placeBetButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    
    UserData *userData = [UserData getInstance];
    if (userData.userId == nil)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:LocalizedString(@"user_must_login", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"cancel",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
        placeBetsService.delegate = self;
        
        NSInteger tappedMatch = [sender tag]-100;
        
        NSLog(@"tapped match %d", (int)tappedMatch);
        
        //dirty hack to a bug that gives +1 to tappedMatch for no apparent reason, when automatically switching from combi to single by deleting all the matches from the combi page
        if (tappedMatch == [matches count] + 1)
        {
            tappedMatch = tappedMatch - 1;
        }
        userWantsToPlaceBet = tappedMatch - 1;
        
        NSInteger selectedVendor = [[selectedVendorPerMatch objectAtIndex:userWantsToPlaceBet] intValue];
        NSString *betUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
        NSString *vendorUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
        NSString *vendorName = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
        NSString *selectedTeam = [[matches objectAtIndex:userWantsToPlaceBet] valueForKey:@"selected_rate"];
        NSString *stake = [stakeForBet objectAtIndex:userWantsToPlaceBet];
        
        NSArray *betData = [NSArray arrayWithObjects:betUID, vendorUID, vendorName, selectedTeam, stake, nil];
        
        [placeBetsService checkSingleBet:betData forUser:userData.userId andPassword:userData.password];
    }
}

- (void)placeAllBetsButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    
    UserData *userData = [UserData getInstance];
    if (userData.userId == nil)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:LocalizedString(@"user_must_login", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"cancel",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
        placeBetsService.delegate = self;
        
        NSInteger tappedMatch = [sender tag]-100;
        
        NSLog(@"tapped match %d", (int)tappedMatch);
        
        //dirty hack to a bug that gives +1 to tappedMatch for no apparent reason, when automatically switching from combi to single by deleting all the matches from the combi page
        if (tappedMatch == [matches count] + 1)
        {
            tappedMatch = tappedMatch - 1;
        }
        if (betslipBetType == 2)
        {
            NSInteger selectedVendor = [selectedVendorPerCombi intValue];
            NSString *vendorUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
            NSString *vendorName = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
            NSString *odd = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"rate"];
            NSString *stake = stakeForCombi;
            
            NSMutableArray *betsMutable = [[NSMutableArray alloc] init];
            for (NSManagedObject *k in matches) {
                NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                [obj setValue:[k valueForKey:@"match_id"] forKey:@"match"];
                [obj setValue:[k valueForKey:@"selected_rate"] forKey:@"bet_team"];
                [obj setValue:vendorUID forKey:@"vendor"];
                
                [betsMutable addObject:obj];
            }
            
            NSArray *betData = [NSArray arrayWithObjects:[betsMutable copy], vendorUID, vendorName, odd, stake, nil];
            
            [placeBetsService checkCombiBet:betData forUser:userData.userId andPassword:userData.password];
        }
    }
}

-(void)betCheckedWithResult:(NSArray *)betCheckResult forBetOfType:(NSInteger)betType
{
    [progressHud hide:YES];
    
    if ([[betCheckResult valueForKey:@"status"] caseInsensitiveCompare:@"error"] == NSOrderedSame)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[betCheckResult valueForKey:@"headline"]
                                              message:[betCheckResult valueForKey:@"text"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:[betCheckResult valueForKey:@"cancelbutton"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([[betCheckResult valueForKey:@"status"] caseInsensitiveCompare:@"success"] == NSOrderedSame)
    {
        NSString *stake = [betCheckResult valueForKey:@"stake"];
        NSDecimalNumber *taxes = [NSDecimalNumber decimalNumberWithString:[[betCheckResult valueForKey:@"taxes"] stringValue]];
        NSDecimalNumber *roundedTaxes = [taxes decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSDecimalNumber *fees = [NSDecimalNumber decimalNumberWithString:[[betCheckResult valueForKey:@"fees"] stringValue]];
        NSDecimalNumber *roundedFees = [fees decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSDecimalNumber *totalCost = [NSDecimalNumber decimalNumberWithString:[[betCheckResult valueForKey:@"totalCost"] stringValue]];
        NSDecimalNumber *roundedTotalCost = [totalCost decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSString *odds = [betCheckResult valueForKey:@"quote"];
        NSString *possibleWin = [betCheckResult valueForKey:@"win"];
        NSString *text = [betCheckResult valueForKey:@"text"];
        NSString *alertMessage = F(@"Stake: %@\nTaxes: %@\nFees: %@\nTotal cost: %@\nOdds: %@\nPossible win: %@\n\n%@", stake, roundedTaxes, roundedFees, roundedTotalCost, odds, possibleWin, text);
        
        if (betType == 1)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betCheckResult valueForKey:@"headline"]
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"cancel",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betCheckResult valueForKey:@"submitbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                           
                                           UserData *userData = [UserData getInstance];
                                           PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
                                           placeBetsService.delegate = self;
                                           
                                           NSInteger selectedVendor = [[selectedVendorPerMatch objectAtIndex:userWantsToPlaceBet] intValue];
                                           NSString *betUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
                                           NSString *vendorUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
                                           NSString *vendorName = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
                                           NSString *selectedTeam = [[matches objectAtIndex:userWantsToPlaceBet] valueForKey:@"selected_rate"];
                                           NSString *stake = [stakeForBet objectAtIndex:userWantsToPlaceBet];
                                           
                                           NSArray *betData = [NSArray arrayWithObjects:betUID, vendorUID, vendorName, selectedTeam, stake, nil];
                                           
                                           [placeBetsService placeSingleBet:betData forUser:userData.userId andPassword:userData.password];
                                       }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (betType == 2)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betCheckResult valueForKey:@"headline"]
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"cancel",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betCheckResult valueForKey:@"submitbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                           
                                           UserData *userData = [UserData getInstance];
                                           PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
                                           placeBetsService.delegate = self;
                                           
                                           NSInteger selectedVendor = [selectedVendorPerCombi intValue];
                                           NSString *vendorUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
                                           NSString *betUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
                                           NSString *vendorName = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
                                           NSString *odd = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"rate"];
                                           NSString *stake = stakeForCombi;
                                           
                                           NSMutableArray *betsMutable = [[NSMutableArray alloc] init];
                                           for (NSManagedObject *k in matches) {
                                               NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                                               [obj setValue:[k valueForKey:@"match_id"] forKey:@"match"];
                                               [obj setValue:[k valueForKey:@"selected_rate"] forKey:@"bet_team"];
                                               [obj setValue:vendorUID forKey:@"vendor"];
                                               [obj setValue:betUID forKey:@"bet_uid"];
                                               
                                               [betsMutable addObject:obj];
                                           }
                                           
                                           NSArray *betData = [NSArray arrayWithObjects:[betsMutable copy], betCheckResult, vendorUID, vendorName, odd, stake, nil];
                                           
                                           [placeBetsService placeCombiBet:betData forUser:userData.userId andPassword:userData.password];
                                       }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else //if (betType == 3)
        {
            //TODO
        }
    }
    else if ([[betCheckResult valueForKey:@"status"] caseInsensitiveCompare:@"wrong_quotes"] == NSOrderedSame)
    {
        NSString *stake = [betCheckResult valueForKey:@"stake"];
        NSDecimalNumber *taxes = [NSDecimalNumber decimalNumberWithString:[[betCheckResult valueForKey:@"taxes"] stringValue]];
        NSDecimalNumber *roundedTaxes = [taxes decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSDecimalNumber *fees = [NSDecimalNumber decimalNumberWithString:[[betCheckResult valueForKey:@"fees"] stringValue]];
        NSDecimalNumber *roundedFees = [fees decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSDecimalNumber *totalCost = [NSDecimalNumber decimalNumberWithString:[[betCheckResult valueForKey:@"totalCost"] stringValue]];
        NSDecimalNumber *roundedTotalCost = [totalCost decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSString *odds = [betCheckResult valueForKey:@"quote"];
        NSString *possibleWin = [betCheckResult valueForKey:@"win"];
        NSString *text = [betCheckResult valueForKey:@"text"];
        NSString *alertMessage = F(@"%@\n\nStake: %@\nTaxes: %@\nFees: %@\nTotal cost: %@\nOdds: %@\nPossible win: %@", text, stake, roundedTaxes, roundedFees, roundedTotalCost, odds, possibleWin);
        
        if (betType == 1)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betCheckResult valueForKey:@"headline"]
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"cancel",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betCheckResult valueForKey:@"submitbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                           
                                           UserData *userData = [UserData getInstance];
                                           PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
                                           placeBetsService.delegate = self;
                                           
                                           NSInteger selectedVendor = [[selectedVendorPerMatch objectAtIndex:userWantsToPlaceBet] intValue];
                                           NSString *betUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
                                           NSString *vendorUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
                                           NSString *vendorName = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
                                           NSString *selectedTeam = [[matches objectAtIndex:userWantsToPlaceBet] valueForKey:@"selected_rate"];
                                           NSString *stake = [stakeForBet objectAtIndex:userWantsToPlaceBet];
                                           
                                           NSArray *betData = [NSArray arrayWithObjects:betUID, vendorUID, vendorName, selectedTeam, stake, nil];
                                           
                                           [placeBetsService placeSingleBet:betData forUser:userData.userId andPassword:userData.password];
                                       }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (betType == 2)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betCheckResult valueForKey:@"headline"]
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"cancel",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betCheckResult valueForKey:@"submitbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                           
                                           UserData *userData = [UserData getInstance];
                                           PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
                                           placeBetsService.delegate = self;
                                           
                                           NSInteger selectedVendor = [selectedVendorPerCombi intValue];
                                           NSString *vendorUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
                                           NSString *betUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
                                           NSString *vendorName = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
                                           NSString *odd = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"rate"];
                                           NSString *stake = stakeForCombi;
                                           
                                           NSMutableArray *betsMutable = [[NSMutableArray alloc] init];
                                           for (NSManagedObject *k in matches) {
                                               NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                                               [obj setValue:[k valueForKey:@"match_id"] forKey:@"match"];
                                               [obj setValue:[k valueForKey:@"selected_rate"] forKey:@"bet_team"];
                                               [obj setValue:vendorUID forKey:@"vendor"];
                                               [obj setValue:betUID forKey:@"bet_uid"];
                                               
                                               [betsMutable addObject:obj];
                                           }
                                           
                                           NSArray *betData = [NSArray arrayWithObjects:[betsMutable copy], betCheckResult, vendorUID, vendorName, odd, stake, nil];
                                           
                                           [placeBetsService placeCombiBet:betData forUser:userData.userId andPassword:userData.password];
                                       }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else //if (betType == 3)
        {
            //TODO
        }
    }
    else //generic error with different status
    {
        @try
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betCheckResult valueForKey:@"headline"]
                                                  message:[betCheckResult valueForKey:@"text"]
                                                  preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betCheckResult valueForKey:@"cancelbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        @catch (NSException *exception)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"Unknown error", nil)
                                                  message:LocalizedString(@"Please try again later", nil)
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

-(void)betPlacedWithResult:(NSArray *)betPlaceResult forBetOfType:(NSInteger)betType
{
    [progressHud hide:YES];
    
    if (betPlaceResult == nil)
    {
        @try
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betPlaceResult valueForKey:@"headline"]
                                                  message:[betPlaceResult valueForKey:@"text"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betPlaceResult valueForKey:@"cancelbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        @catch (NSException *exception)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"Unknown error", nil)
                                                  message:LocalizedString(@"Please try again later", nil)
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    if ([[betPlaceResult valueForKey:@"status"] caseInsensitiveCompare:@"error"] == NSOrderedSame)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[betPlaceResult valueForKey:@"headline"]
                                              message:[betPlaceResult valueForKey:@"text"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:[betPlaceResult valueForKey:@"cancelbutton"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([[betPlaceResult valueForKey:@"status"] caseInsensitiveCompare:@"success"] == NSOrderedSame)
    {
        NSString *currency = @"€";
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setGroupingSeparator:[[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator]];
        [numberFormatter setMaximumFractionDigits:2];
        [numberFormatter setMinimumFractionDigits:2];
        
        NSDecimalNumber *balanceNumber = [NSDecimalNumber decimalNumberWithString:[[betPlaceResult valueForKey:@"balance"] stringValue]];
        NSDecimalNumber *roundedBalance = [balanceNumber decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        
        NSString *newBalance = F(@"%@ %@", [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[roundedBalance doubleValue]]], currency);
        NSString *text = [betPlaceResult valueForKey:@"text"];
        NSString *alertMessage = F(@"%@\n\nNew Account Balance: %@", text, newBalance);
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[betPlaceResult valueForKey:@"headline"]
                                              message:alertMessage
                                              preferredStyle:UIAlertControllerStyleAlert];
        if (betType == 1)
        {
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betPlaceResult valueForKey:@"cancelbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           betJustPlaced = YES;
                                           [self deleteButtonPressed:nil];
                                       }];
            
            [alertController addAction:okAction];
        }
        else if (betType == 2)
        {
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betPlaceResult valueForKey:@"cancelbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self deleteAllMatches];
                                       }];
            
            [alertController addAction:okAction];
        }
        else //if (betType == 3)
        {
            //TODO
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([[betPlaceResult valueForKey:@"status"] caseInsensitiveCompare:@"wrong_quotes"] == NSOrderedSame)
    {
        NSString *stake = [betPlaceResult valueForKey:@"stake"];
        NSDecimalNumber *taxes = [NSDecimalNumber decimalNumberWithString:[[betPlaceResult valueForKey:@"taxes"] stringValue]];
        NSDecimalNumber *roundedTaxes = [taxes decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSDecimalNumber *fees = [NSDecimalNumber decimalNumberWithString:[[betPlaceResult valueForKey:@"fees"] stringValue]];
        NSDecimalNumber *roundedFees = [fees decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSDecimalNumber *totalCost = [NSDecimalNumber decimalNumberWithString:[[betPlaceResult valueForKey:@"totalCost"] stringValue]];
        NSDecimalNumber *roundedTotalCost = [totalCost decimalNumberByRoundingAccordingToBehavior:BIB_ROUNDING_BEHAVIOUR];
        NSString *odds = [betPlaceResult valueForKey:@"quote"];
        NSString *possibleWin = [betPlaceResult valueForKey:@"win"];
        NSString *text = [betPlaceResult valueForKey:@"text"];
        NSString *alertMessage = F(@"%@\n\nStake: %@\nTaxes: %@\nFees: %@\nTotal cost: %@\nOdds: %@\nPossible win: %@", text, stake, roundedTaxes, roundedFees, roundedTotalCost, odds, possibleWin);
        
        if (betType == 1)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betPlaceResult valueForKey:@"headline"]
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"cancel",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betPlaceResult valueForKey:@"submitbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                           
                                           UserData *userData = [UserData getInstance];
                                           PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
                                           placeBetsService.delegate = self;
                                           
                                           NSInteger selectedVendor = [[selectedVendorPerMatch objectAtIndex:userWantsToPlaceBet] intValue];
                                           NSString *betUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
                                           NSString *vendorUID = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
                                           NSString *vendorName = [[[vendorsPerBet objectAtIndex:userWantsToPlaceBet] objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
                                           NSString *selectedTeam = [[matches objectAtIndex:userWantsToPlaceBet] valueForKey:@"selected_rate"];
                                           NSString *stake = [stakeForBet objectAtIndex:userWantsToPlaceBet];
                                           
                                           NSArray *betData = [NSArray arrayWithObjects:betUID, vendorUID, vendorName, selectedTeam, stake, nil];
                                           
                                           [placeBetsService placeSingleBet:betData forUser:userData.userId andPassword:userData.password];
                                       }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (betType == 2)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betPlaceResult valueForKey:@"headline"]
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"cancel",nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betPlaceResult valueForKey:@"submitbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                           
                                           UserData *userData = [UserData getInstance];
                                           PlaceBetsService *placeBetsService = [[PlaceBetsService alloc] init];
                                           placeBetsService.delegate = self;
                                           
                                           NSInteger selectedVendor = [selectedVendorPerCombi intValue];
                                           NSString *vendorUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_uid"];
                                           NSString *betUID = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor_bet_uid"];
                                           NSString *vendorName = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"vendor"];
                                           NSString *odd = [[vendorsPerCombi objectAtIndex:selectedVendor-1] valueForKey:@"rate"];
                                           NSString *stake = stakeForCombi;
                                           
                                           NSMutableArray *betsMutable = [[NSMutableArray alloc] init];
                                           for (NSManagedObject *k in matches) {
                                               NSMutableDictionary *obj = [[NSMutableDictionary alloc] init];
                                               [obj setValue:[k valueForKey:@"match_id"] forKey:@"match"];
                                               [obj setValue:[k valueForKey:@"selected_rate"] forKey:@"bet_team"];
                                               [obj setValue:vendorUID forKey:@"vendor"];
                                               [obj setValue:betUID forKey:@"bet_uid"];
                                               
                                               [betsMutable addObject:obj];
                                           }
                                           
                                           NSArray *betData = [NSArray arrayWithObjects:[betsMutable copy], betPlaceResult, vendorUID, vendorName, odd, stake, nil];
                                           
                                           [placeBetsService placeCombiBet:betData forUser:userData.userId andPassword:userData.password];
                                       }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else //if (betType == 3)
        {
            //TODO
        }
    }
    else //generic error with different status
    {
        @try
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[betPlaceResult valueForKey:@"headline"]
                                                  message:[betPlaceResult valueForKey:@"text"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:[betPlaceResult valueForKey:@"cancelbutton"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        @catch (NSException *exception)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"Unknown error", nil)
                                                  message:LocalizedString(@"Please try again later", nil)
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)deleteButtonPressed:(id)sender
{
    NSInteger tappedMatch;
    
    if (!betJustPlaced)
    {
        tappedMatch = [sender tag]-100;
        
        //dirty hack to a bug that gives +1 to tappedMatch for no apparent reason, when automatically switching from combi to single by deleting all the matches from the combi page
        if (tappedMatch == [matches count] + 1)
        {
            tappedMatch = tappedMatch - 1;
        }
    }
    else
    {
        tappedMatch = userWantsToPlaceBet+1;
        betJustPlaced = NO;
    }
    NSString *matchID = [[matches objectAtIndex:tappedMatch-1] valueForKey:@"match_id"];
    NSString *matchRate = [[matches objectAtIndex:tappedMatch-1] valueForKey:@"selected_rate"];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BettingSlipMatches"];
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"match_id == %@ AND selected_rate == %@", matchID, matchRate];
    [fetchRequest setPredicate:fetchPredicate];
    NSError *fetchError = nil;
    
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    NSManagedObject *matchToRemove = [fetchResult lastObject]; //the query has to give just one item, so lastObject is the same as objectAtIndex:0
    [self.managedObjectContext deleteObject:matchToRemove];
    
    [self.managedObjectContext save:nil];
    
    [matches removeObjectAtIndex:tappedMatch-1];
    if (betslipBetType == 1)
    {
        [self checkValidMatches];
        [self automaticExpandAllSections];
    }
    else if (betslipBetType == 2)
    {
        if ([matches count] < 2)
        {
            [self betslipBetTypeChanged:1];
        }
        else
        {
            [self callCombiService];
        }
    }
}

- (void)deleteAllMatches
{
    NSFetchRequest *allMatches = [[NSFetchRequest alloc] init];
    [allMatches setEntity:[NSEntityDescription entityForName:@"BettingSlipMatches" inManagedObjectContext:self.managedObjectContext]];
    [allMatches setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *categoriesResult = [self.managedObjectContext executeFetchRequest:allMatches error:&error];
    
    for (NSManagedObject *toDelete in categoriesResult) {
        [self.managedObjectContext deleteObject:toDelete];
    }
    
    [self.managedObjectContext save:nil];
    [self betslipBetTypeChanged:1];
}

- (void)betslipBetTypeChanged:(NSInteger)betslipBetTypeTag
{
    //first of all close every eventual keyboard in the screen
    [self.view endEditing:YES];
    
    [self checkValidMatches];
    
    //can't go to combi or bets if there is less than 2 matches
    if ([matches count] > 1 || betslipBetTypeTag == 1)
    {
        betslipBetType = betslipBetTypeTag;
        
        if (betslipBetType == 1)
        {
            if ([matches count] > 0)
            {
                //close all sections and reload the service
                for (int i = 0; i < [boolArray count]; i++)
                {
                    [boolArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                }
                [self automaticExpandAllSections];
            }
        }
        else if (betslipBetType == 2)
        {
            //call combi service
            [self callCombiService];
        }
        else if (betslipBetType == 3)
        {
            //////// test call system service
            vendorBetsService = [[VendorBetsService alloc] init];
            vendorBetsService.delegate = self;
            NSMutableArray *mutableCombiMatches = [[NSMutableArray alloc] init];
            NSMutableArray *mutableCombiTeams = [[NSMutableArray alloc] init];
            for (NSObject *obj in matches) {
                [mutableCombiMatches addObject:[obj valueForKey:@"match_id"]];
                NSString *selected_team = [obj valueForKey:@"selected_rate"];
                
                [mutableCombiTeams addObject:selected_team];
            }
            NSArray *combiMatches = [NSArray arrayWithArray:mutableCombiMatches];
            NSArray *combiTeams = [NSArray arrayWithArray:mutableCombiTeams];
            NSMutableArray *ignoreList = [NSMutableArray arrayWithArray:@[@(YES), @(YES), @(YES)]];
            
            UserData *userData = [UserData getInstance];
//            if (userData.userId == nil)
//            {
            [vendorBetsService loadVendorBetsForSystem:combiMatches andSelectedTeams:combiTeams ignoreKBets:ignoreList];
//            }
//            else
//            {
//                [vendorBetsService loadVendorBetsForSystem:combiMatches andSelectedTeams:combiTeams ignoreKBets:ignoreList forUser:userData.userId];
//            }
            //////// end of test lines
        }

        [self.tableView reloadData];
    }
}

- (void)callCombiService
{
    //prevent calling the service for less than 2 bets
    if ([matches count] < 2)
    {
        [self betslipBetTypeChanged:1];
    }
    else
    {
        progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        vendorBetsService = [[VendorBetsService alloc] init];
        vendorBetsService.delegate = self;
        NSMutableArray *mutableCombiMatches = [[NSMutableArray alloc] init];
        NSMutableArray *mutableCombiTeams = [[NSMutableArray alloc] init];
        for (NSObject *obj in matches) {
            [mutableCombiMatches addObject:[obj valueForKey:@"match_id"]];
            NSString *selected_team = [obj valueForKey:@"selected_rate"];
            
            [mutableCombiTeams addObject:selected_team];
        }
        NSArray *combiMatches = [NSArray arrayWithArray:mutableCombiMatches];
        NSArray *combiTeams = [NSArray arrayWithArray:mutableCombiTeams];
        
        UserData *userData = [UserData getInstance];
        if (userData.userId == nil)
        {
            [vendorBetsService loadVendorBetsForCombi:combiMatches andSelectedTeams:combiTeams];
        }
        else
        {
            [vendorBetsService loadVendorBetsForCombi:combiMatches andSelectedTeams:combiTeams forUser:userData.userId];
        }
    }
}

- (IBAction)closePressed:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)stakeTextFieldDoneTouched:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kOFFSET_FOR_KEYBOARD, 0);
    
    UITableViewCell *cell = (UITableViewCell *) textField.superview;
    [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.text = @"";
}

- (void)textFieldDidChange:(UITextField *)textField
{
    BetStakeCell *cell = (BetStakeCell *)[(UIView *)textField superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    //indexPath takes high values if the cell was dequeued
    if ([self.tableView.indexPathsForVisibleRows indexOfObject:indexPath] != NSNotFound)
    {
        //update the potential profit and the taxes in real time
        BetProfitCell *profitCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section]];
        
        double betStake = [textField.text doubleValue];
        double betRate;
        
        if (betslipBetType == 1)
        {
            betRate = [[rateForBet objectAtIndex:indexPath.section-1] doubleValue];
        }
        else if (betslipBetType == 2)
        {
            betRate = [rateForCombi doubleValue];
        }
        
        double potentialProfit = betRate * betStake;
        
        [profitCell setPotentialProfit:F(@"%.2f",potentialProfit)];
        [cell setTaxes];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    BetStakeCell *cell = (BetStakeCell *)[(UIView *)textField superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    NSString *currentStake = F(@"%.2f", [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue]);
    
    //indexPath takes high values if the cell was dequeued
    if ([self.tableView.indexPathsForVisibleRows indexOfObject:indexPath] != NSNotFound)
    {
        if (betslipBetType == 1)
        {
            [stakeForBet replaceObjectAtIndex:indexPath.section-1 withObject:currentStake];
            
            textField.text = [currentStake doubleValue] != 0 ? currentStake : @"";
            [cell setTaxes];
            
            //update the potential profit, which is a row after this textfield
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationAutomatic];
            //update the total potential profit, which is in last section
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:numberOfSections-1] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else if (betslipBetType == 2)
        {
            stakeForCombi = [currentStake mutableCopy];
            
            textField.text = [currentStake doubleValue] != 0 ? currentStake : @"";
            [cell setTaxes];
            
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    
    self.tableView.contentInset = UIEdgeInsetsZero;
    
    return YES;
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end