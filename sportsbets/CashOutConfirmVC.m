//
//  DepositConfirmVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 16/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "CashOutConfirmVC.h"

@implementation CashOutConfirmVC

@synthesize passedData;

/*
 passedData objects:
 0 - amount
 1 - iban
 2 - bic
 3 - account holder
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.overviewLabel.text = LocalizedString(@"overview", nil);
    self.amountLabel.text = LocalizedString(@"amount", nil);
    self.amountDisplay.text = F(@"%@ €", [passedData objectAtIndex:0]);
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    self.ibanDisplay.text = [passedData objectAtIndex:1];
    self.bicDisplay.text = [passedData objectAtIndex:2];
    self.accountHolderLabel.text = LocalizedString(@"account_holder", nil);
    self.accountHolderDisplay.text = [passedData objectAtIndex:3];
    [self.transferConfirmButton setTitle:LocalizedString(@"confirm", nil) forState:UIControlStateNormal];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            return 44;
        case 1:
            return 44;
        case 2:
            return 44;
        case 3:
            return 44;
        case 4:
            return 30;
        case 5:
            return 44;
        case 6:
            return 60;
        default:
            break;
    }
    return 44;
}

@end
