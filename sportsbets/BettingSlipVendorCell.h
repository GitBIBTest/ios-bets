//
//  BettingSlipVendorCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 09/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@interface BettingSlipVendorCell : UITableViewCell

@property (nonatomic,retain) UILabel *vendorName;
@property (nonatomic,retain) UIButton *vendorRateButton;

-(void)selectCell;
-(void)deselectCell;
-(void)setLogo:(NSString *)logoImageName;

@end
