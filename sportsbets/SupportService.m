//
//  SupportService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/04/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "SupportService.h"

@implementation SupportService
{
    NSMutableData *_downloadedData;
    NSInteger operationID;
}

- (id)init
{
    if (!self)
    {
        self = [super init];
    }
    return self;
}

- (void)downloadSupportOptions
{
    operationID = 1;
    
    NSString *language;
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en-US"])
    {
        language = @"2";
    }
    else
    {
        language = @"1";
    }
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/support_service/download_options";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&lang=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], language);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)sendSupportRequest
{
    operationID = 2;
    
    UserData *userData = [UserData getInstance];
    
    NSString *loggedIn;
    
    if (userData.userId == nil)
    {
        loggedIn = @"0";
    }
    else
    {
        loggedIn = @"1";
    }
    
    NSString *username;
    NSString *email;
    NSString *name;
    NSString *surname;
    NSString *message;
    NSString *categoryUID;
    NSString *reasonUID;
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/support_service/download_options";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&loggedin=%@&username=%@&email=%@&name=%@&surname=%@&message=%@&category=%@&reason=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], loggedIn, [username urlEncodeUsingEncoding:NSUTF8StringEncoding], [email urlEncodeUsingEncoding:NSUTF8StringEncoding], [name urlEncodeUsingEncoding:NSUTF8StringEncoding], [surname urlEncodeUsingEncoding:NSUTF8StringEncoding], [message urlEncodeUsingEncoding:NSUTF8StringEncoding], [categoryUID urlEncodeUsingEncoding:NSUTF8StringEncoding], [reasonUID urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonResult = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"response from the server = %@", jsonResult);
    
    if (operationID == 1)
    {
        NSDictionary *resultCategories = [jsonResult valueForKey:@"result"];
        NSArray *categoriesUIDs = [resultCategories allKeys];
        
        NSMutableArray *allCategoriesAndReasons = [[NSMutableArray alloc] init];
        
        for (NSString *key in categoriesUIDs)
        {
            SupportObject *supportObject = [[SupportObject alloc] initWithUID:key andName:[[resultCategories valueForKey:key] valueForKey:@"name"]];
            NSArray *reasonsForCategory = [[resultCategories valueForKey:key] valueForKey:@"reasons"];
            NSMutableDictionary *reasonsDict = [[NSMutableDictionary alloc] init];
            for (NSDictionary *reason in reasonsForCategory)
            {
                [reasonsDict setValue:[reason valueForKey:@"name"] forKey:[reason valueForKey:@"uid"]];
            }
            supportObject.reasons = [reasonsDict copy];
            [allCategoriesAndReasons addObject:supportObject];
        }
        [self commitData:[allCategoriesAndReasons copy]];
    }
    else //if (operationID == 2)
    {
        [self commitData:jsonResult];
    }
}

- (void)commitData:(NSArray *)dataToCommit
{
    if (operationID == 1)
    {
        [self.delegate supportOptionsDownloaded:dataToCommit];
    }
    else //if (operationID == 2)
    {
        [self.delegate supportRequestSent:dataToCommit];
    }
}

@end
