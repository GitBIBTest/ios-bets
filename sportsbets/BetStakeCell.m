//
//  BetStakeCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 09/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "BetStakeCell.h"

@implementation BetStakeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat euroLabelWidth = 15.0;
        CGFloat euroLabelOriginX = screenWidth - 10.0 - euroLabelWidth;
        CGFloat stakeTextFieldWidth = 90.0;
        CGFloat stakeTextFieldOriginX = euroLabelOriginX - 5.0 - stakeTextFieldWidth;
        CGFloat stakeLabelWidth = screenWidth/2.0-60.0;
        CGFloat stakeLabelOriginX = stakeTextFieldOriginX - 5.0 - stakeLabelWidth;
        
        UILabel *stakeLabel = [[UILabel alloc] initWithFrame:CGRectMake(stakeLabelOriginX, 15.0, stakeLabelWidth, 20.0)];
        [stakeLabel setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        [stakeLabel setText:LocalizedString(@"stake", nil)];
        [stakeLabel setTextAlignment:NSTextAlignmentRight];
        
        self.stakeTextField = [[UITextField alloc] initWithFrame:CGRectMake(stakeTextFieldOriginX, 15.0, stakeTextFieldWidth, 20.0)];
        [self.stakeTextField setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        [self.stakeTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:LocalizedString(@"stake", nil) attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}]];
        [self.stakeTextField setTextAlignment:NSTextAlignmentCenter];
        [self.stakeTextField clearsOnBeginEditing];
        [self.stakeTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        [self.stakeTextField setTextColor:[UIColor whiteColor]];
        [self.stakeTextField setBackground:[UIImage imageNamed:@"OddButton"]];
        
        UILabel *euroLabel = [[UILabel alloc] initWithFrame:CGRectMake(euroLabelOriginX, 15.0, euroLabelWidth, 20.0)];
        [euroLabel setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        [euroLabel setText:@"€"];
        
        self.taxesLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 15.0, 150.0, 20.0)];
        [self.taxesLabel setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        
        //show custom separator line
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, self.frame.size.height-1.0, screenWidth - 20.0, 1.0/[UIScreen mainScreen].scale)];
        separatorLineView.backgroundColor = [UIColor lightGrayColor];
        
        [self addSubview:stakeLabel];
        [self addSubview:self.stakeTextField];
        [self addSubview:euroLabel];
        [self addSubview:self.taxesLabel];
        [self addSubview:separatorLineView];
    }
    return self;
}

- (void)setTaxes
{
    CGFloat taxesRateF = 0.071;
    NSDecimalNumber *taxesRate = [NSDecimalNumber decimalNumberWithString:F(@"%f",taxesRateF)];
    NSDecimalNumber *stake = [NSDecimalNumber decimalNumberWithString:self.stakeTextField.text];
    
    NSDecimalNumber *taxes = [stake decimalNumberByMultiplyingBy:taxesRate withBehavior:BIB_ROUNDING_BEHAVIOUR];
    
    NSString *currency = @"€";
    [self.taxesLabel setText:F(@"%@ %@ %@", LocalizedString(@"taxes", nil), taxes, currency)];
}

@end
