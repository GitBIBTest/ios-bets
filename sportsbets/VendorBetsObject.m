//
//  VendorBetsObject.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "VendorBetsObject.h"

@implementation VendorBetsObject

+ (instancetype)sharedVendorBetsObject
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^
    {
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

@end