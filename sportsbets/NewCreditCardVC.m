//
//  NewCreditCardVC.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 07/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "NewCreditCardVC.h"

@implementation NewCreditCardVC
{
    NSArray *acceptedCardTypes;
    NSInteger thisYear;
    NSInteger thisMonth;
}

@synthesize cardTypeTextField, cardNumberTextField, cardCodeTextField, cardExpirationPicker, cardOwnerTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //define the accepted credit cards
    acceptedCardTypes = @[F(@"%s%@%s","-",LocalizedString(@"card_type", nil),"-"), @"VISA", @"MasterCard", @"Maestro"];
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    //place the done button on the "security code" textfield
    UIToolbar *toolBarCVVTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarCVVTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *CVVTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                            target:self
                                                                                            action:@selector(CVVTextFieldDoneTouched:)];
    [toolBarCVVTextField setItems:[NSArray arrayWithObjects:
                                   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                 target:nil
                                                                                 action:nil],
                                   CVVTextFieldDoneButton, nil]];
    cardCodeTextField.inputAccessoryView = toolBarCVVTextField;
    
    //configure the pickerviews
    //card type
    cardTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    cardTypePickerView.delegate = self;
    cardTypePickerView.dataSource = self;
    cardTypePickerView.showsSelectionIndicator = YES;
    cardTypePickerView.tag = 1;
    
    //place the done button on the "card type" pickerview
    UIToolbar *toolBarCardTypePickerView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarCardTypePickerView.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *cardTypePickerViewDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                  target:self
                                                                                                  action:@selector(cardTypePickerViewDoneTouched:)];
    [toolBarCardTypePickerView setItems:[NSArray arrayWithObjects:
                                         [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                       target:nil
                                                                                       action:nil],
                                         cardTypePickerViewDoneButton, nil]];
    self.cardTypeTextField.inputAccessoryView = toolBarCardTypePickerView;
    
    //configure labels and other views
    self.cardTypeLabel.text = LocalizedString(@"card_type", nil);
    cardTypeTextField.placeholder = LocalizedString(@"card_type", nil);
    [cardTypeTextField setInputView:cardTypePickerView];
    self.cardNumberLabel.text = LocalizedString(@"card_number", nil);
    cardNumberTextField.placeholder = LocalizedString(@"card_number", nil);
    self.cardCodeLabel.text = LocalizedString(@"card_security_code", nil);
    cardCodeTextField.placeholder = LocalizedString(@"card_security_code", nil);
    self.cardExpirationLabel.text = LocalizedString(@"expiration_date", nil);
    //get current year and set it as minimum card's expiration date
    NSDateComponents* components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:[NSDate date]];
    thisYear = [components year];
    thisMonth = [components month];
    [cardExpirationPicker setupMinYear:thisYear maxYear:thisYear + 10];
    self.cardOwnerLabel.text = LocalizedString(@"card_owner", nil);
    cardOwnerTextField.placeholder = LocalizedString(@"card_owner", nil);
    [self.confirmButton setTitle:LocalizedString(@"save", nil) forState:UIControlStateNormal];
}

//------------------------------------------------------------------------------------
//Picker View Category/Tournament
//------------------------------------------------------------------------------------

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [acceptedCardTypes count];
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [acceptedCardTypes objectAtIndex:row];
}

- (void)cardTypePickerViewDoneTouched:(UIBarButtonItem *)sender
{
    if ([cardTypePickerView selectedRowInComponent:0] == 0)
    {
        self.cardTypeTextField.text = @"";
    }
    else
    {
        self.cardTypeTextField.text = [acceptedCardTypes objectAtIndex:[cardTypePickerView selectedRowInComponent:0]];
    }
    [self.view endEditing:YES];
}

//------------------------------------------------------------------------------------
//TableView Functions
//------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 30;
        case 1:
            return 44;
        case 2:
            return 30;
        case 3:
            return 44;
        case 4:
            return 30;
        case 5:
            return 44;
        case 6:
            return 30;
        case 7:
            return 60;
        case 8:
            return 30;
        case 9:
            return 44;
        case 10:
            return 60;
        default:
            break;
    }
    return 44;
}

//------------------------------------------------------------------------------------
//Actions for buttons and misc functions
//------------------------------------------------------------------------------------

-(IBAction)confirmButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    BOOL showAlert = NO;
    
    //sanitize the strings by removing (or trimming) spaces
    cardNumberTextField.text = [cardNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    cardCodeTextField.text = [cardCodeTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    cardOwnerTextField.text = [cardOwnerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    
    //card type must be set to something
    if ([cardTypePickerView selectedRowInComponent:0] == 0)
    {
        showAlert = YES;
        self.cardTypeLabel.textColor = [UIColor redColor];
    }
    else
    {
        self.cardTypeLabel.textColor = [UIColor blackColor];
    }
    //card number must be 16 digits long and just decimal
    if ([cardNumberTextField.text length] != 16 || [cardNumberTextField.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.cardNumberLabel.textColor = [UIColor redColor];
        cardNumberTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.cardNumberLabel.textColor = [UIColor blackColor];
    }
    //security code must 3 digits long and just decimal
    if ([cardCodeTextField.text length] != 3 || [cardCodeTextField.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.cardCodeLabel.textColor = [UIColor redColor];
        cardCodeTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.cardCodeLabel.textColor = [UIColor blackColor];
    }
    //the card needs not to be expired
    if ([[cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:1] forComponent:1] integerValue] == thisYear &&
        [[cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:0] forComponent:0] integerValue] < thisMonth)
    {
        showAlert = YES;
        self.cardExpirationLabel.textColor = [UIColor redColor];
    }
    else
    {
        self.cardExpirationLabel.textColor = [UIColor blackColor];
    }
    //name needs at least one space and no punctuation, symbols or numbers
    if ([cardOwnerTextField.text length] < 3 ||
        [[cardOwnerTextField.text componentsSeparatedByString:@" "] count] - 1 < 1 ||
        [cardOwnerTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound ||
        [cardOwnerTextField.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound ||
        [cardOwnerTextField.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.cardOwnerLabel.textColor = [UIColor redColor];
        cardOwnerTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.cardOwnerLabel.textColor = [UIColor blackColor];
    }
    
    if (showAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Errors in the input", nil)
                                              message:@"Please correct the highlighted fields and retry"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        if ([self saveCreditCard])
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"Error", nil)
                                                  message:@"Problem saving the data. Please try again later."
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:LocalizedString(@"Ok", nil)
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"OK action");
                                           }];
            
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (BOOL)saveCreditCard
{
    NSString *expiration_month = [cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:0] forComponent:0];
    NSString *expiration_year = [cardExpirationPicker titleForRow:[cardExpirationPicker selectedRowInComponent:1] forComponent:1];
    NSManagedObject *newCreditCard = [NSEntityDescription insertNewObjectForEntityForName:@"CreditCard" inManagedObjectContext:self.managedObjectContext];
    [newCreditCard setValue:cardTypeTextField.text forKey:@"type"];
    [newCreditCard setValue:cardNumberTextField.text forKey:@"number"];
    [newCreditCard setValue:cardCodeTextField.text forKey:@"cvc"];
    [newCreditCard setValue:F(@"%@ %@", expiration_month, expiration_year) forKey:@"expiration_date"];
    [newCreditCard setValue:cardOwnerTextField.text forKey:@"owner"];
    [newCreditCard setValue:[self fetchLoggedInUser] forKeyPath:@"toLogin"];
    
    if ([self.managedObjectContext save:nil])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)CVVTextFieldDoneTouched:(UIBarButtonItem *)sender
{
    [self textFieldShouldReturn:cardCodeTextField];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

- (NSObject *)fetchLoggedInUser
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Login"];
    NSError *fetchError = nil;
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat:@"username LIKE[c] %@", [UserData getInstance].userName]];
    NSArray *loginArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    NSObject *loggedInUser = [loginArray objectAtIndex:0];
    
    return loggedInUser;
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end