//
//  FilterMaster.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "FilterMaster.h"

@implementation FilterMaster

+ (instancetype)sharedFilterMaster
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

-(void)loadFilterForSport:(NSInteger)newSportIdentifier
{
    self.sportIdentifier = newSportIdentifier;
    self.filter_category_uid = nil;
    self.filter_tournament_uid = nil;
}

@end
