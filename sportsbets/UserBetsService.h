//
//  UserBetsService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "UserBetsModel.h"
#import "UserBetsData.h"
#import "UserBetsObject.h"
#import "Localization.h"
#import "NSString+UrlEncoding.h"

@protocol UserBetsServiceProtocol <NSObject>

- (void)userBetsDownloaded;

@end

@interface UserBetsService : NSObject

@property (nonatomic,weak) id <UserBetsServiceProtocol> delegate;

-(void)loadCurrentBetsForUser:(NSString *)user andPassword:(NSString *)password limitTo:(NSInteger)userBetsToLoad;
-(void)loadMyBetsForUser:(NSString *)user andPassword:(NSString *)password limitTo:(NSInteger)userBetsToLoad;

@end
