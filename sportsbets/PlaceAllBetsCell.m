//
//  PlaceAllBetsCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 17/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "PlaceAllBetsCell.h"

@implementation PlaceAllBetsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat labelsWidth = screenWidth - 40.0;
        CGFloat buttonWidth = 180;
        CGFloat buttonOriginX = screenWidth/2.0 - buttonWidth/2.0;
        
        self.stakeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 10.0, labelsWidth, 20.0)];
        [self.stakeLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:15.0]];
        [self.stakeLabel setTextAlignment:NSTextAlignmentCenter];
        
        self.profitLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 35.0, labelsWidth, 20.0)];
        [self.profitLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:15.0]];
        [self.profitLabel setTextAlignment:NSTextAlignmentCenter];
        
        self.placeAllBetsButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonOriginX, 60.0, buttonWidth, 32.0)];
        [self.placeAllBetsButton setTitle:LocalizedString(@"place_all_bets", nil) forState:UIControlStateNormal];
        [self.placeAllBetsButton.titleLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:16.0]];
        [self.placeAllBetsButton setBackgroundColor:BIBredcolor];
        [self.placeAllBetsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        //show custom separator line
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 1.0, screenWidth - 20.0, 1.0/[UIScreen mainScreen].scale)];
        separatorLineView.backgroundColor = BIBredcolor;
        separatorLineView.tag = 999;
        
        [self addSubview:self.profitLabel];
        [self addSubview:self.stakeLabel];
        [self addSubview:self.placeAllBetsButton];
        [self addSubview:separatorLineView];
    }
    return self;
}

-(void)setTotalPotentialProfit:(NSString *)potentialProfit forStake:(NSString *)totalStake
{
    NSString *totalStakeText = LocalizedString(@"total_stake", nil);
    NSString *potentialProfitText = LocalizedString(@"total_potential_profit", nil);
    NSString *euro = @"€";
    
    self.stakeLabel.text = F(@"%@ %@ %@", totalStakeText, totalStake, euro);
    self.profitLabel.text = F(@"%@ %@ %@", potentialProfitText, potentialProfit, euro);
}

@end
