//
//  Utils.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 21/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Macros.h"

@interface Utils : NSObject

-(int)lineCountForText:(NSString *)text andFont:(UIFont *)font lineWidth:(CGFloat)lineWidth;
-(NSDictionary *)urlPathToDictionary:(NSString *)path;

@end
