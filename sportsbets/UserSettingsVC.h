//
//  UserSettingsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"
#import "TextFieldNoCursor.h"

@interface UserSettingsVC : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    UIPickerView *minutesPickerView;
}

@property (nonatomic,retain) IBOutlet UILabel *oddsDisplayLabel;
@property (nonatomic,retain) IBOutlet UILabel *notificationsLabel;
@property (nonatomic,retain) IBOutlet UILabel *notificationsTyp1Label;
@property (nonatomic,retain) IBOutlet UILabel *notificationsTyp2Label;
@property (nonatomic,retain) IBOutlet UILabel *autoLogoutAfterLabel;
@property (nonatomic,retain) IBOutlet UILabel *minutesLabel;

@property (nonatomic,retain) IBOutlet UISegmentedControl *oddsDisplayControl;
@property (nonatomic,retain) IBOutlet UISegmentedControl *notificationsType1Control;
@property (nonatomic,retain) IBOutlet UISegmentedControl *notificationsType2Control;
@property (nonatomic,retain) IBOutlet TextFieldNoCursor
*minutesTextField;

@end
