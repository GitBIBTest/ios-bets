//
//  BetProfitCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 09/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "BetProfitCell.h"

@implementation BetProfitCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat profitLabelWidth = screenWidth - 40.0;
        
        self.profitLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 15.0, profitLabelWidth, 20.0)];
        [self.profitLabel setFont:[UIFont fontWithName:@"Montserrat" size:15.0]];
        [self.profitLabel setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:self.profitLabel];
    }
    return self;
}

-(void)setPotentialProfit:(NSString *)potentialProfit
{
    NSString *potentialProfitText = LocalizedString(@"potential_profit", nil);
    NSString *potentialProfitNumber = potentialProfit;
    NSString *euro = @"€";
    NSString *labelText = F(@"%@ %@ %@", potentialProfitText, potentialProfitNumber, euro);
    
    [self.profitLabel setText:labelText];
}

@end
