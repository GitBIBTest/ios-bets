//
//  RegistrationConfirmationViewController.m
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 7/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "RegistrationConfirmationVC.h"

@implementation RegistrationConfirmationVC
{
    NSDictionary *userDict;
    NSMutableString *givenPassword;
    NSMutableString *givenUsername;
    MBProgressHUD *progressHud;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Registration Confirm";
    self.navigationController.navigationBar.topItem.title = @"";
//    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.usernameTextField.placeholder = LocalizedString(@"username", nil);
    self.passwordTextField.placeholder = LocalizedString(@"password", nil);
    self.confirmPasswordTextField.placeholder = LocalizedString(@"password_confirm", nil);
    [self.confirmButton setTitle:@"Registrierung Abschließen" forState:UIControlStateNormal];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"registration_confirm_dictionary"] != nil)
    {
        userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"registration_confirm_dictionary"];
        NSLog(@"%@", userDict);
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title;
    switch (section) {
        case 0:
            title = LocalizedString(@"registration_confirm_title", nil);
            break;
        case 1:
            title = LocalizedString(@"registration_confirm_message", nil);
            break;
        default:
            title = @"";
            break;
    }
    return title;
}

- (IBAction)confirmButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    
    bool showUserAlert = NO;
    bool showPasswordAlert = NO;
    bool showPasswordConfirmationAlert = NO;
    
    self.usernameTextField.text = [self.usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    NSCharacterSet *illegalUserCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
    illegalUserCharacterSet = [illegalUserCharacterSet invertedSet];
    
    if (self.usernameTextField.text == nil || [self.usernameTextField.text length] == 0 ||
        [self.usernameTextField.text rangeOfCharacterFromSet:illegalUserCharacterSet].location != NSNotFound)
    {
        showUserAlert = YES;
        self.usernameTextField.textColor = [UIColor redColor];
    }
    else if (![self isValidPassword:self.passwordTextField.text])
    {
        showPasswordAlert = YES;
        self.passwordTextField.textColor = [UIColor redColor];
    }
    else if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        showPasswordConfirmationAlert = YES;
        self.passwordTextField.textColor = [UIColor redColor];
        self.confirmPasswordTextField.textColor = [UIColor redColor];
    }
    
    if (showUserAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:LocalizedString(@"username_invalid", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (showPasswordAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:LocalizedString(@"password_validation_failed", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (showPasswordConfirmationAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:LocalizedString(@"password_confirm_not_equal", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];

        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        givenUsername = [self.usernameTextField.text mutableCopy];
        givenPassword = [self.passwordTextField.text mutableCopy];
        
        RegisterService *registerService = [[RegisterService alloc] init];
        registerService.delegate = self;
        [registerService finalizeUserWithUsername:givenUsername andPassword:givenPassword regHash:[userDict valueForKey:@"hash"]];
    }
}

- (void)finalizationOutputReceived:(NSArray *)returnData
{
    [progressHud hide:YES];
    
    if ([[returnData valueForKey:@"status"] isEqualToString:@"success"])
    {
        NSLog(@"%@", returnData);
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"registration_confirm_dictionary"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"registration completed");
        
        NSEntityDescription *login = [NSEntityDescription entityForName:@"Login" inManagedObjectContext:self.managedObjectContext];
        NSManagedObject *newLogin = [[NSManagedObject alloc]initWithEntity:login insertIntoManagedObjectContext:self.managedObjectContext];
        //set attributes for entity Sport
        [newLogin setValue:givenUsername forKey:@"username"];
        [newLogin setValue:givenPassword forKey:@"password"];
        [newLogin setValue:[returnData valueForKey:@"password_encrypted"] forKey:@"password_encrypted"];
        [newLogin setValue:[returnData valueForKey:@"user_id"] forKey:@"id"];
        [self.managedObjectContext save:nil];
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Registration complete", nil)
                                              message:F(@"Your registration is complete and you are now logged in as %@", givenUsername)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else //if ([[returnData valueForKey:@"status"] isEqualToString:@"error"]
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:[returnData valueForKey:@"message"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
             
- (BOOL)isValidPassword:(NSString*)password
{
    NSRegularExpression* regex = [[NSRegularExpression alloc] initWithPattern:@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[#?!@$%^&*-]).{8,}$"
 options:0 error:nil];
    return [regex numberOfMatchesInString:password options:0 range:NSMakeRange(0, [password length])] > 0;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

//to retrieve managed object context
-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
