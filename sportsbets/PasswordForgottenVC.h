//
//  PasswordForgottenVC.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 28/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "AppDelegate.h"

@interface PasswordForgottenVC : UITableViewController <UITextFieldDelegate>

@property (nonatomic,retain) IBOutlet UILabel *emailLabel;
@property (nonatomic,retain) IBOutlet UITextField *emailTextField;
@property (nonatomic,retain) IBOutlet UIButton *confirmButton;

@end
