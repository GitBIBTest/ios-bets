//
//  BasketballBetsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 11/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "BetsService.h"
#import "Localization.h"
#import "BetsObject.h"
#import "BettingSlipVC.h"
#import "SportBetsCell.h"
#import "TextFieldNoCursor.h"
#import "Macros.h"
#import "AllSportsInARowCell.h"
#import "UserData.h"
#import "MBProgressHUD.h"
#import "BBBadgeBarButtonItem.h"
#import "FilterMaster.h"

@interface BasketballBetsVC : UITableViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, BetsServiceProtocol, AllSportsInARowCellProtocol>
{
    int data_retrievable;
    int sport_identifier;
    bool userLoggedIn;
    
    BetsService *betsService;
    UIPickerView *pickerViewBetType;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *burgerButton;
@property (strong, nonatomic) UIBarButtonItem *secondBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *fourthBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterButton;

@property (strong, nonatomic) TextFieldNoCursor *pickerViewBetTypeField;

- (void)betsDownloaded;

@end
