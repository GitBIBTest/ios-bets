//
//  SoccerBetsSectionTableViewCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 12/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoccerBetsSectionTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *countryFlag;
@property (strong, nonatomic) IBOutlet UILabel *leagueName;


@end
