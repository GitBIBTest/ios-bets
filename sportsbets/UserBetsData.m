//
//  UserBetsData.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserBetsData.h"

@implementation UserBetsData

- (id) init
{
    if (!self)
    {
        self = [super init];
    }
    self.userBetsArray = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)addUserBet:(UserBetsModel *)userBetsData
{
    [self.userBetsArray addObject:userBetsData];
}

@end
