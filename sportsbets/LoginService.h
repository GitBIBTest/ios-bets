//
//  LoginService.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 24.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "LoginVC.h"
#import "Macros.h"
#import "NSString+UrlEncoding.h"

@interface LoginService : NSObject

@property LoginVC *loginVC;

-(void)checkLoginForUsername:(NSString *)username andForPassword:(NSString *)password;

@end
