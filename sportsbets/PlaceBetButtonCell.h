//
//  PlaceBetButtonCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 17/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"

@interface PlaceBetButtonCell : UITableViewCell

@property (nonatomic,retain) UIButton *placeBetButton;

@end
