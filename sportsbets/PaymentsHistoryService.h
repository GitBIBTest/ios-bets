//
//  PaymentsHistoryService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 24/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@protocol PaymentsHistoryServiceProtocol <NSObject>

-(void)historyDownloaded:(NSArray *)historyArray;

@end

@interface PaymentsHistoryService : NSObject

@property (nonatomic,retain) id <PaymentsHistoryServiceProtocol> delegate;

-(void)downloadPaymentHistoryOfType:(NSString *)type ForUser:(NSString *)user andPassword:(NSString *)password;

@end
