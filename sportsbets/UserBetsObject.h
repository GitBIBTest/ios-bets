//
//  UserBetsObject.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserBetsDataArray.h"

@interface UserBetsObject : UserBetsDataArray

+ (UserBetsObject*) sharedUserBetsObject;

@end
