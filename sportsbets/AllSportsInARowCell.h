//
//  AllSportsInARowCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 02/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@protocol AllSportsInARowCellProtocol <NSObject>

- (void)sportLogoHasBeenTouched:(NSString *)sportIdentifier;

@end

@interface AllSportsInARowCell : UITableViewCell

@property (weak,nonatomic) id <AllSportsInARowCellProtocol> delegate;

- (void)highlightImageWithTag:(NSInteger)sportIdentifier;

@end
