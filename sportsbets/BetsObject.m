//
//  BetsObject.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "BetsObject.h"

@implementation BetsObject

+ (instancetype)sharedBetsObject
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^
    {
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

@end