//
//  SofortIdentVC.h
//  Sports Betting by Bet IT Best
//
//  Created by Aleksandar Penev on 21/04/2016.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "AppDelegate.h"
#import "Macros.h"

@interface SofortIdentVC : UITableViewController
@property (strong, nonatomic) IBOutlet UITextField *firstnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *surnameTextField;
@property (strong, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (strong, nonatomic) IBOutlet UIButton *startVerificationButton;
@property (strong, nonatomic) IBOutlet UIDatePicker *birthdayDatePicker;
@property (strong, nonatomic) IBOutlet UITextField *streetTextField;
@property (strong, nonatomic) IBOutlet UITextField *areaCodeTextField;
@property (strong, nonatomic) IBOutlet UITextField *areaTextField;
@property (strong, nonatomic) IBOutlet UITextField *houseNumberTextField;
@property (strong, nonatomic) IBOutlet UIButton *dateButton;

@property (assign) BOOL pickerIsShowing;
@property (strong, nonatomic) NSDate *selectedDate;
@property (strong, nonatomic) UITextField *activeTextField;

- (IBAction)datePickerValueChanged:(id)sender;
- (IBAction)startVerification:(UIButton *)sender;
- (IBAction)selectDate:(UIButton *)sender;

@end
