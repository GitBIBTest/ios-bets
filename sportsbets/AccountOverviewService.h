//
//  AccountOverviewService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 18/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Macros.h"

@protocol AccountOverviewServiceProtocol <NSObject>

-(void)accountBalanceDownloaded:(NSString *)balance;
-(void)accountOverviewDownloaded:(NSArray *)userInfo;

@end

@interface AccountOverviewService : NSObject

@property (nonatomic,weak) id <AccountOverviewServiceProtocol> delegate;

-(void)loadAccountBalanceForUser:(NSString *)user andForPassword:(NSString *)password;
-(void)loadAccountOverviewForUser:(NSString *)user andForPassword:(NSString *)password;

@end

