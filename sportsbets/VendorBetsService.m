//
//  VendorBetsService.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "VendorBetsService.h"

@implementation VendorBetsService
{
    VendorBetsData *vendorBetsData;
    NSMutableData *_downloadedData;
    NSInteger downloadedBetType; //1 single, 2 combi, 3 system
}

- (id)init
{
    if (!self)
    {
        self = [super init];
    }
    return self;
}

-(void)loadVendorBetsForMatch:(NSString *)match_id andSelectedTeam:(NSString *)selected_team
{
    downloadedBetType = 1;
    NSString *url = @"http://dev.betitbest.com/bets/en/api/betslip/single";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&matchid=%@-%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], match_id, selected_team);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadVendorBetsForMatch:(NSString *)match_id andSelectedTeam:(NSString *)selected_team forUser:(NSString *)user
{
    downloadedBetType = 1;
    NSString *url = @"http://dev.betitbest.com/bets/en/api/betslip/single";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&matchid=%@-%@&userid=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], match_id, selected_team, [user urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadVendorBetsForCombi:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams
{
    downloadedBetType = 2;
    
    NSMutableString *matchesQuery = [[NSMutableString alloc] init];
    for (int i = 0; i < [match_ids count]-1; i++)
    {
        [matchesQuery appendString:F(@"%@-%@___", match_ids[i], selected_teams[i])];
    }
    [matchesQuery appendString:F(@"%@-%@", [match_ids lastObject], [selected_teams lastObject])];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/betslip/combi";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&matches=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], matchesQuery);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadVendorBetsForCombi:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams forUser:(NSString *)user
{
    downloadedBetType = 2;
    
    NSMutableString *matchesQuery = [[NSMutableString alloc] init];
    for (int i = 0; i < [match_ids count]-1; i++)
    {
        [matchesQuery appendString:F(@"%@-%@___", match_ids[i], selected_teams[i])];
    }
    [matchesQuery appendString:F(@"%@-%@", [match_ids lastObject], [selected_teams lastObject])];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/betslip/combi";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&matches=%@&userid=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], matchesQuery, [user urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadVendorBetsForSystem:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams ignoreKBets:(NSArray *)ignoreList
{
    downloadedBetType = 3;
    
    NSMutableString *matchesQuery = [[NSMutableString alloc] init];
    for (int i = 0; i < [match_ids count]-1; i++)
    {
        [matchesQuery appendString:F(@"%@-%@___", match_ids[i], selected_teams[i])];
    }
    [matchesQuery appendString:F(@"%@-%@", [match_ids lastObject], [selected_teams lastObject])];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/betslip/combi";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&matches=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], matchesQuery);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}
-(void)loadVendorBetsForSystem:(NSArray *)match_ids andSelectedTeams:(NSArray *)selected_teams ignoreKBets:(NSArray *)ignoreList forUser:(NSString *)user
{
    downloadedBetType = 3;
    
    NSMutableString *matchesQuery = [[NSMutableString alloc] init];
    for (int i = 0; i < [match_ids count]-1; i++)
    {
        [matchesQuery appendString:F(@"%@-%@___", match_ids[i], selected_teams[i])];
    }
    [matchesQuery appendString:F(@"%@-%@", [match_ids lastObject], [selected_teams lastObject])];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/betslip/combi";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&matches=%@&userid=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], matchesQuery, [user urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSArray *loadedVendorBets = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"login vendors bets = %@", loadedVendorBets);
    
    if (downloadedBetType == 1) //single bets
    {
        if ([[loadedVendorBets valueForKey:@"status"] isEqualToString:@"success"])
        {
            NSArray *dataArray = [loadedVendorBets valueForKey:@"data"];
            vendorBetsData = [[VendorBetsData alloc] init];

            for (int i = 0; i < dataArray.count; i++)
            {
                NSDictionary *vendorDict = dataArray[i];
                VendorBetsModel *vendorBetsModel = [[VendorBetsModel alloc]init];
                vendorBetsModel.vendor = vendorDict[@"vendor"];
                vendorBetsModel.vendor_uid = vendorDict[@"vendor_uid"];
                vendorBetsModel.rate = vendorDict[F(@"odd%@", vendorDict[@"selected_team"])];
                vendorBetsModel.fee = vendorDict[@"vendor_fee"];
                vendorBetsModel.maxbet = vendorDict[@"maxbet"];
                vendorBetsModel.minbet = vendorDict[@"minbet"];
                vendorBetsModel.vendor_bet_uid = vendorDict[@"bet_uid"];
                vendorBetsModel.vendor_logo = vendorDict[@"vendorlogo"];
                
                [vendorBetsData addVendorBets:vendorBetsModel];
            }
            
            [self commitData:vendorBetsData];
        }
    }
    else if (downloadedBetType == 2) //combi bets
    {
        if ([[loadedVendorBets valueForKey:@"status"] isEqualToString:@"success"])
        {
            NSDictionary *dataArray = [loadedVendorBets valueForKey:@"result"];
            NSDictionary *oddsArray = [loadedVendorBets valueForKey:@"data"];
            
            vendorBetsData = [[VendorBetsData alloc] init];
            
            if ([oddsArray count] != 0)
            {
               NSArray* sortedKeys = [oddsArray keysSortedByValueUsingComparator:^(id first, id second) {
                    if ([first floatValue] > [second floatValue])
                        return (NSComparisonResult)NSOrderedAscending;
                    
                    if ([first floatValue] < [second floatValue])
                        return (NSComparisonResult)NSOrderedDescending;
                    return (NSComparisonResult)NSOrderedSame;
                }];
                
                NSMutableArray *sortedOdds = [[NSMutableArray alloc] init];
                
                NSInteger numberOfVendors = MIN(5, [sortedKeys count]);
                
                for (int i = 0; i < numberOfVendors; i++)
                {
                    [sortedOdds addObject:[oddsArray valueForKey:sortedKeys[i]]];
                    NSArray *vendorDict = [[dataArray valueForKey:[[dataArray allKeys] objectAtIndex:0]] valueForKey:sortedKeys[i]];
                    
                    VendorBetsModel *vendorBetsModel = [[VendorBetsModel alloc]init];
                    vendorBetsModel.vendor = [vendorDict valueForKey:@"vendor"];
                    vendorBetsModel.vendor_uid = sortedKeys[i];
                    vendorBetsModel.rate = sortedOdds[i];
                    vendorBetsModel.fee = [vendorDict valueForKey:@"vendor_fee"];
                    vendorBetsModel.maxbet = [vendorDict valueForKey:@"maxbet"];
                    vendorBetsModel.minbet = [vendorDict valueForKey:@"minbet"];
                    vendorBetsModel.vendor_bet_uid = [vendorDict valueForKey:@"bet_uid"];
                    vendorBetsModel.vendor_logo = [vendorDict valueForKey:@"vendorlogo"];
                    
                    NSLog(@"%@ %@ %@ %@ %@ %@ %@", vendorBetsModel.vendor, vendorBetsModel.vendor_uid, vendorBetsModel.rate, vendorBetsModel.fee, vendorBetsModel.maxbet, vendorBetsModel.minbet, vendorBetsModel.vendor_bet_uid);
                    
                    [vendorBetsData addVendorBets:vendorBetsModel];
                }
            }
        }
        [self commitData:vendorBetsData];
    }
    else if (downloadedBetType == 3) //system bets
    {
        
    }
}

-(void)commitData:(VendorBetsData *)vendorBetsDataInstance
{
    [self.delegate vendorBetsDownloadedForMatch:vendorBetsDataInstance];
}

@end