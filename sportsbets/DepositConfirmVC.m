//
//  DepositConfirmVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 16/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "DepositConfirmVC.h"

@implementation DepositConfirmVC
{
    BOOL hideSection0;
    BOOL hideSection1;
}

@synthesize passedData, passedPaymentType;

/*
 passedData objects:
    case bank transfer (passedPaymentType = type1):
        0 - amount
        1 - iban
        2 - bic
        3 - account holder
    case credit card (passedPaymentType = type2):
        0 - amount
        1 - card type
        2 - card number
        3 - cvv
        4 - expiration month
        5 - expiration year
        6 - card holder
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.overviewLabel.text = LocalizedString(@"overview", nil);
    self.amountLabel.text = LocalizedString(@"amount", nil);
    self.amountDisplay.text = F(@"%@ €", [passedData objectAtIndex:0]);
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    if ([passedPaymentType isEqualToString:@"type1"])
    {
        hideSection0 = NO;
        hideSection1 = YES;
        
        self.ibanDisplay.text = [passedData objectAtIndex:1];
        self.bicDisplay.text = [passedData objectAtIndex:2];
        self.accountHolderLabel.text = LocalizedString(@"account_holder", nil);
        self.accountHolderDisplay.text = [passedData objectAtIndex:3];
        [self.transferConfirmButton setTitle:LocalizedString(@"confirm_payment", nil) forState:UIControlStateNormal];
    }
    else if ([passedPaymentType isEqualToString:@"type2"])
    {
        hideSection0 = YES;
        hideSection1 = NO;
        
        self.cardTypeLabel.text = LocalizedString(@"card_type", nil);
        self.cardTypeDisplay.text = [passedData objectAtIndex:1];
        self.cardNumberLabel.text = LocalizedString(@"card_number", nil);
        self.cardNumberDisplay.text = [passedData objectAtIndex:2];
        self.cardCodeLabel.text = LocalizedString(@"card_security_code", nil);
        self.cardCodeDisplay.text = [passedData objectAtIndex:3];
        self.expirationLabel.text = LocalizedString(@"expiration_date", nil);
        self.expirationDisplay.text = F(@"%@ / %@", [passedData objectAtIndex:4], [passedData objectAtIndex:5]);
        self.cardHolderLabel.text = LocalizedString(@"card_owner", nil);
        self.cardHolderDisplay.text = [passedData objectAtIndex:6];
        [self.cardConfirmButton setTitle:LocalizedString(@"confirm_payment", nil) forState:UIControlStateNormal];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Bank transfer
    if (section == 1)
    {
        if(hideSection0)
        {
            return 0;
        }
        else
        {
            return 5;
        }
    }
    //Credit cards
    if (section == 2)
    {
        if (hideSection1)
        {
            return 0;
        }
        else
        {
            return 8;
        }
    }
    //section 0 always visible
    else
    {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        switch (indexPath.row) {
            case 0:
                return 44;
            case 1:
                return 44;
            case 2:
                return 30;
            case 3:
                return 44;
            case 4:
                return 60;
            default:
                break;
        }
    }
    else if (indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
                return 44;
            case 1:
                return 30;
            case 2:
                return 44;
            case 3:
                return 44;
            case 4:
                return 44;
            case 5:
                return 30;
            case 6:
                return 44;
            case 7:
                return 60;
            default:
                break;
        }
    }
    return 44;
}

@end
