//
//  ChangeLimitsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Macros.h"
#import "ChangeLimitsService.h"
#import "UserData.h"
#import "MBProgressHUD.h"

@interface ChangeLimitsVC : UITableViewController <UITextFieldDelegate, ChangeLimitsServiceProtocol>
{
    NSString *currentMonthLimit;
    NSString *currentBetLimit;
    NSString *maxPermittedMonthLimit;
    NSString *maxPermittedBetLimit;
}

@property (nonatomic,retain) IBOutlet UILabel *monthLimitLabel;
@property (nonatomic,retain) IBOutlet UITextField *monthLimitTextField;
@property (nonatomic,retain) IBOutlet UILabel *betLimitLabel;
@property (nonatomic,retain) IBOutlet UITextField *betLimitTextField;
@property (nonatomic,retain) IBOutlet UILabel *adviceLabel;
@property (nonatomic,retain) IBOutlet UIButton *confirmButton;

@end
