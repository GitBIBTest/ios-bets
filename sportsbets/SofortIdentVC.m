//
//  SofortIdentVC.m
//  Sports Betting by Bet IT Best
//
//  Created by Aleksandar Penev on 21/04/2016.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "SofortIdentVC.h"
#import "SofortIdentWebViewVC.h"

#define kPickerIndex 1
#define kDatePickerSection 2
#define kPickerCellHeight 216

@implementation SofortIdentVC

@synthesize areaTextField;
@synthesize firstnameTextField;
@synthesize surnameTextField;
@synthesize birthdayLabel;
@synthesize birthdayDatePicker;
@synthesize startVerificationButton;
@synthesize streetTextField;
@synthesize areaCodeTextField;
@synthesize houseNumberTextField;
@synthesize dateButton;
@synthesize pickerIsShowing;
@synthesize selectedDate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = LocalizedString(@"sofort_ident", nil);
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    birthdayDatePicker.hidden = YES;
    birthdayLabel.text = LocalizedString(@"birthdate", nil);
    [startVerificationButton setTitle:LocalizedString(@"start_verification", nil) forState:UIControlStateNormal];
    
    firstnameTextField.placeholder = LocalizedString(@"name", nil);
    surnameTextField.placeholder = LocalizedString(@"surname", nil);
    streetTextField.placeholder = LocalizedString(@"address_street", nil);
    houseNumberTextField.placeholder = LocalizedString(@"address_housenr", nil);
    areaCodeTextField.placeholder = LocalizedString(@"address_plz", nil);
    areaTextField.placeholder = LocalizedString(@"address_city", nil);
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}


- (IBAction)datePickerValueChanged:(id)sender {
    UIDatePicker *tmpDatePicker = (UIDatePicker *)sender;
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter1 setDateFormat:@"dd.MM.yyyy"]; //// here set format of date which is in your output date (means above str with format)
    birthdayLabel.text =  [dateFormatter1 stringFromDate:tmpDatePicker.date];
    
    selectedDate = tmpDatePicker.date;
}

- (IBAction)startVerification:(UIButton *)sender {
    //showSofortIdentWebView
    
    BOOL canRegister = YES;
    NSMutableString *message = [NSMutableString string];
    [message appendString:[NSString stringWithFormat:@"%@\n", LocalizedString(@"fill_in_fields", nil)]];
    if([firstnameTextField.text length] == 0) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"name", nil)]];
    }
    if([surnameTextField.text length] == 0) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"surname", nil)]];
    }
    if([birthdayLabel.text length] == 0 || [birthdayLabel.text isEqualToString:LocalizedString(@"birthdate", nil)]) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"birthdate", nil)]];
    }
    if([streetTextField.text length] == 0) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"address_street", nil)]];
    }
    if([houseNumberTextField.text length] == 0) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"address_housenr", nil)]];
    }
    if([areaCodeTextField.text length] == 0) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"address_plz", nil)]];
    }
    if([areaTextField.text length] == 0) {
        canRegister = NO;
        [message appendString:[NSString stringWithFormat:@"-%@\n", LocalizedString(@"address_city", nil)]];
    }
    
    if(canRegister){
        [self performSegueWithIdentifier:@"showSofortIdentWebView" sender:self];
    } else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Error", nil)
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok",nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (IBAction)selectDate:(UIButton *)sender {
    if (pickerIsShowing){
        
        [self hidePickerCell];
        
    }else {
        
        [self.activeTextField resignFirstResponder];
        
        [self showPickerCell];
    }
}

#pragma mark - Table view methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    if (indexPath.row == kPickerIndex && indexPath.section == kDatePickerSection){
        
        height = self.pickerIsShowing ? kPickerCellHeight : 0.0f;
        
    }
    
    return height;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 NSString *sectionName;
 switch (section)
 {
 case 0:
 sectionName = LocalizedString(@"name", nil);
 break;
 case 1:
 sectionName = LocalizedString(@"surname", nil);
 break;
 case 2:
 sectionName = LocalizedString(@"birthdate", nil);
 break;
 case 3:
 sectionName = LocalizedString(@"address_street", nil);
 break;
 case 4:
 sectionName = LocalizedString(@"address_housenr", nil);
 break;
 case 5:
 sectionName = LocalizedString(@"address_plz", nil);
 break;
 case 6:
 sectionName = LocalizedString(@"address_city", nil);
 break;
 default:
 sectionName = @"";
 break;
 }
 return sectionName;
 }
 
 /*- (CGFloat)tableView:(UITableView*)tableView
 heightForHeaderInSection:(NSInteger)section {
 return 25.0;
 }*/

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    return YES;
}

- (void)signUpForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)keyboardWillShow {
    
    if (pickerIsShowing){
        [self hidePickerCell];
    }
}

- (void)showPickerCell {
    
    pickerIsShowing = YES;
    
    if(!selectedDate){
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter1 setDateFormat:@"dd.MM.yyyy"]; //// here set format of date which is in your output date (means above str with format)
        birthdayLabel.text =  [dateFormatter1 stringFromDate:[NSDate date]];
        selectedDate = [NSDate date];
    }
    
    [self.tableView beginUpdates];
    
    [self.tableView endUpdates];
    
    birthdayDatePicker.hidden = NO;
    birthdayDatePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        birthdayDatePicker.alpha = 1.0f;
        
    }];
}

- (void)hidePickerCell {
    
    pickerIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         birthdayDatePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         birthdayDatePicker.hidden = YES;
                     }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.activeTextField = textField;
    
    if (self.pickerIsShowing){
        
        [self hidePickerCell];
        
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"showSofortIdentWebView"]) {
        SofortIdentWebViewVC *vc = [segue destinationViewController];
        vc.firstname = firstnameTextField.text;
        vc.surname = surnameTextField.text;
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
        vc.birthday = [dateFormatter1 stringFromDate:selectedDate];
        vc.street = streetTextField.text;
        vc.houseNumber = houseNumberTextField.text;
        vc.area = areaTextField.text;
        vc.areaCode = areaCodeTextField.text;
    }
}
@end