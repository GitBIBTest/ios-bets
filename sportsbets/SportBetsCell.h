//
//  SportBetsCell.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 01/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@interface SportBetsCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *team1Label;
@property (nonatomic, retain) IBOutlet UILabel *team2Label;
@property (nonatomic, retain) IBOutlet UILabel *matchdateLabel;
@property (nonatomic, retain) IBOutlet UIButton *winner1Button;
@property (nonatomic, retain) IBOutlet UIButton *winner_noButton;
@property (nonatomic, retain) IBOutlet UIButton *winner2Button;
@property (nonatomic, retain) IBOutlet UILabel *label1;
@property (nonatomic, retain) IBOutlet UILabel *labelX;
@property (nonatomic, retain) IBOutlet UILabel *label2;


@property (nonatomic, assign) BOOL hasDraw;

@end