//
//  BetTypeHeader.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"

@protocol betTypeHeaderProtocol <NSObject>

-(void)betslipBetTypeChanged:(NSInteger)betTypeTag;

@end

@interface BetTypeHeader : UITableViewCell

@property (nonatomic,weak) id <betTypeHeaderProtocol> delegate;

@property (nonatomic,retain) UIView *containerView;
@property (nonatomic,retain) UIButton *betType1;
@property (nonatomic,retain) UIButton *betType2;
@property (nonatomic,retain) UIButton *betType3;
@property (nonatomic,retain) UIColor *activeColor;
@property (nonatomic,retain) UIColor *inactiveColor;

@end
