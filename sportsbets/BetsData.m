//
//  BetsData.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 26/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "BetsData.h"

@implementation BetsData

- (id) init
{
    if (!self)
    {
        self = [super init];
    }
    self.betsArray = [[NSMutableArray alloc] init];
    self.tournamentArray = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)addBets:(BetsModel *)betsData orderByTournament:(BOOL)orderByTournament
{
    [self.betsArray addObject:betsData];
    if (orderByTournament)
    {
        [self addTournament:betsData.tournament_uid andTournamentName:betsData.tournament_name forCategoryName:betsData.category_name];
        [self addBets:betsData toTournament:betsData.tournament_uid];
    }
    else
    {
        [self addTournament:betsData.tournament_uid andTournamentName:betsData.tournament_name withBet:betsData forCategoryName:betsData.category_name];
    }

}

-(void)addBets:(BetsModel *)betsData toTournament:(NSString *)tournamentid
{
    TournamentModel *tournamentModel = [[TournamentModel alloc]init];
    for (TournamentModel *tournament in self.tournamentArray)
    {
        if ([tournament.tournament_uid isEqualToString:tournamentid])
        {
            tournamentModel = tournament;
            break;
        }
    }
    [tournamentModel.betsArray addObject:betsData];
}

-(void)addTournament:(NSString *)tournamentid andTournamentName:(NSString *)tournamentname forCategoryName:(NSString *)categoryname
{
    _Bool tournamentFound = NO;
    for (TournamentModel *storedTournament in self.tournamentArray)
    {
        if ([tournamentid isEqualToString:storedTournament.tournament_uid])
        {
            tournamentFound = YES;
            break;
        }
    }
    if (!tournamentFound)
    {
        TournamentModel *tournamentModel = [[TournamentModel alloc]initWithName:tournamentname andWithID:tournamentid forCategoryName:categoryname];
        [self.tournamentArray addObject:tournamentModel];
    }
}

-(void)addTournament:(NSString *)tournamentid andTournamentName:(NSString *)tournamentname withBet:(BetsModel *)betsData forCategoryName:(NSString *)categoryname
{
    TournamentModel *tournamentModel;
    if ([[[self.tournamentArray lastObject] valueForKey:@"tournament_uid"] isEqualToString:tournamentid])
    {
        tournamentModel = [self.tournamentArray lastObject];
    }
    else
    {
        tournamentModel = [[TournamentModel alloc]initWithName:tournamentname andWithID:tournamentid forCategoryName:categoryname];
        [self.tournamentArray addObject:tournamentModel];
    }
    [tournamentModel.betsArray addObject:betsData];
}

@end