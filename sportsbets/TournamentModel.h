//
//  TournamentModel.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>

//contains betsArray, to request all bets for one tournament, necessary to show data in tableView
@interface TournamentModel : NSObject

@property (strong,nonatomic) NSString *category_name;
@property (strong,nonatomic) NSString *tournament_name;
@property (strong,nonatomic) NSString *tournament_uid;
@property (strong,nonatomic) NSMutableArray *betsArray;
@property (nonatomic,assign) NSInteger sorting;

- (id) initWithName:(NSString*)tournamentName andWithID:(NSString*)tournamentID forCategoryName:(NSString*)categoryName;

@end