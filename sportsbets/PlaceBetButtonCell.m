//
//  PlaceBetButtonCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 17/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "PlaceBetButtonCell.h"

@implementation PlaceBetButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat buttonWidth = 180;
        CGFloat buttonOriginX = screenWidth/2.0 - buttonWidth/2.0;
        
        self.placeBetButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonOriginX, 6.0, buttonWidth, 32.0)];
        [self.placeBetButton setTitle:LocalizedString(@"place_bet", nil) forState:UIControlStateNormal];
        [self.placeBetButton.titleLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:16.0]];
        [self.placeBetButton setBackgroundColor:BIBredcolor];
        [self.placeBetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self addSubview:self.placeBetButton];
    }
    return self;
}

@end
