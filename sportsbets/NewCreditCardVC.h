//
//  NewCreditCardVC.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 07/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Macros.h"
#import "UserData.h"
#import "CDatePickerViewEx.h"
#import "TextFieldNoCursor.h"

@interface NewCreditCardVC : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
{
    UIPickerView *cardTypePickerView;
}
@property (nonatomic,retain) IBOutlet UILabel *cardTypeLabel;
@property (nonatomic,retain) IBOutlet TextFieldNoCursor *cardTypeTextField;
@property (nonatomic,retain) IBOutlet UILabel *cardNumberLabel;
@property (nonatomic,retain) IBOutlet UITextField *cardNumberTextField;
@property (nonatomic,retain) IBOutlet UILabel *cardCodeLabel;
@property (nonatomic,retain) IBOutlet UITextField *cardCodeTextField;
@property (nonatomic,retain) IBOutlet UILabel *cardExpirationLabel;
@property (nonatomic,retain) IBOutlet CDatePickerViewEx *cardExpirationPicker;
@property (nonatomic,retain) IBOutlet UILabel *cardOwnerLabel;
@property (nonatomic,retain) IBOutlet UITextField *cardOwnerTextField;
@property (nonatomic,retain) IBOutlet UIButton *confirmButton;

@end
