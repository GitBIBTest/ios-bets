//
//  BetVendorTableViewCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 22/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BetVendorTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *vendorImage;
@property (strong, nonatomic) IBOutlet UILabel *vendorName;

@end
