//
//  CashOutHistoryVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "CashOutHistoryVC.h"

@implementation CashOutHistoryVC
{
    NSMutableArray *previousWithdrawals;
    MBProgressHUD *progressHud;
    NSDateFormatter *dateFormatter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    previousWithdrawals = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    
    UserData *userData = [UserData getInstance];
    
    PaymentsHistoryService *paymentsHistoryService = [[PaymentsHistoryService alloc] init];
    paymentsHistoryService.delegate = self;
    [paymentsHistoryService downloadPaymentHistoryOfType:@"out" ForUser:userData.userId andPassword:userData.passwordEncrypted];
    
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHud.labelText = @"Loading...";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [progressHud hide:YES];
}

- (void)historyDownloaded:(NSArray *)historyArray
{
    for (NSObject *k in historyArray)
    {
        double unixTimeStamp = [[k valueForKey:@"processed"] doubleValue];
        NSTimeInterval _interval=unixTimeStamp;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateFormat:LocalizedString(@"date_no_hour", nil)];
        
        NSString *dateString = [dateFormatter stringFromDate:date];
        NSString *paymentType;
        switch ([[k valueForKey:@"paymentservice_uid"] intValue]) {
            case 1:
                paymentType = @"PayPal";
                break;
            case 2:
                paymentType = @"SOFORT Überweisung";
                break;
            case 3:
                paymentType = LocalizedString(@"credit_card_payment", nil);
                break;
            case 4:
                paymentType = @"SEPA-Auszahlung";
                break;
            default:
                paymentType = LocalizedString(@"error", nil);
                break;
        }
        
        NSMutableDictionary *kWithdrawal = [[NSMutableDictionary alloc] init];
        [kWithdrawal setValue:[k valueForKey:@"total"] forKey:@"amount"];
        [kWithdrawal setValue:dateString forKey:@"date"];
        [kWithdrawal setValue:paymentType forKey:@"payment_type"];
        
        [previousWithdrawals addObject:kWithdrawal];
    }
    
    [progressHud hide:YES];
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MAX(1, [previousWithdrawals count]);
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([previousWithdrawals count] == 0)
    {
        return 90;
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    UITableViewCell *cell;
    
    if ([previousWithdrawals count] == 0)
    {
        cellIdentifier = @"NoPreviousWithdrawalsCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        UILabel *nopreviousWithdrawalsLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 20.0, WIDTH(self.view)-30.0, 50.0)];
        [nopreviousWithdrawalsLabel setFont:[UIFont fontWithName:@"Montserrat" size:17.0]];
        [nopreviousWithdrawalsLabel setTextAlignment:NSTextAlignmentCenter];
        nopreviousWithdrawalsLabel.text = LocalizedString(@"no_previous_withdrawals", nil);
        nopreviousWithdrawalsLabel.numberOfLines = 2;
        
        [cell addSubview:nopreviousWithdrawalsLabel];
    }
    else
    {
        cellIdentifier = @"PreviousWithdrawalCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        NSObject *currentWithdrawal = [previousWithdrawals objectAtIndex:indexPath.row];
        
        UILabel *withdrawalAmountLabel = (UILabel *)[cell viewWithTag:10];
        UILabel *withdrawalDateLabel = (UILabel *)[cell viewWithTag:20];
        UILabel *withdrawalTypeLabel = (UILabel *)[cell viewWithTag:30];
        
        withdrawalDateLabel.text = [currentWithdrawal valueForKey:@"date"];
        withdrawalAmountLabel.text = F(@"%@ €",[currentWithdrawal valueForKey:@"amount"]);
        withdrawalTypeLabel.text = LocalizedString([currentWithdrawal valueForKey:@"payment_type"], nil);
    }
    return cell;
}

@end