//
//  VendorBetsModel.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VendorBetsModel : NSObject

@property (strong,nonatomic) NSString *vendor;
@property (strong,nonatomic) NSString *vendor_uid;
@property (strong,nonatomic) NSString *rate;
@property (strong,nonatomic) NSString *fee;
@property (strong,nonatomic) NSString *vendor_bet_uid;
@property (strong,nonatomic) NSString *minbet;
@property (strong,nonatomic) NSString *maxbet;
@property (strong,nonatomic) NSString *vendor_logo;

@end