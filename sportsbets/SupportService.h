//
//  SupportService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/04/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Localization.h"
#import "Macros.h"
#import "NSString+UrlEncoding.h"
#import "SupportObject.h"
#import "UserData.h"

@protocol SupportServiceProtocol <NSObject>

- (void)supportOptionsDownloaded:(NSArray *)supportObjectsArray;
- (void)supportRequestSent:(NSArray *)serverReponse;

@end

@interface SupportService : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, weak) id <SupportServiceProtocol> delegate;

-(void)downloadSupportOptions;
-(void)sendSupportRequest; //parameters to add depending on the model used

@end