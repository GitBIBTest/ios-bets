//
//  MyBetsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UserData.h"
#import "Macros.h"
#import "UserBetsService.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "Reachability.h"

@interface MyBetsVC : UITableViewController <UITableViewDataSource, UITableViewDelegate, UserBetsServiceProtocol>

@end
