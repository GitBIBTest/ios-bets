//
//  BlacklistTableViewController.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 22/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vendor.h"
#import "BetVendorTableViewCell.h"

@interface BlacklistTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *vendors;
@property (strong, nonatomic) UIBarButtonItem *saveButton;

@end
