//
//  LoginVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Dummy on 24.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

/*--------------------------------------------------------------------------------
 TODO WHEN THE API IS DONE
 - Change the dummy login into a real one by sending the actual data to the server
    and ask for confirmation of successful login
 --------------------------------------------------------------------------------*/

#import "LoginVC.h"
#import "LoginService.h"

@implementation LoginVC
{
    UINavigationController *navigationController;
}

@synthesize userTextField, passwordTextField, login;
-(void)viewDidLoad
{
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //initialize properties
    self.userLabel.text = LocalizedString(@"username", nil);
    userTextField.placeholder = LocalizedString(@"username", nil);
    self.passwordLabel.text = LocalizedString(@"password", nil);
    passwordTextField.placeholder = LocalizedString(@"password", nil);
    [login setTitle:LocalizedString(@"login", nil) forState:UIControlStateNormal];
    [self.passwordForgottenButton setTitle:LocalizedString(@"password_forgotten", nil) forState:UIControlStateNormal];
    [self.registerButton setTitle:LocalizedString(@"register", nil) forState:UIControlStateNormal];
}

-(IBAction)loginClicked
{
    [userTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];

    NSString *username = [NSString stringWithString:userTextField.text];
    NSString *password = [NSString stringWithString:passwordTextField.text];
    
    //////////////this is for testing, remove when finished testing
    BOOL justGoOn = NO;
    if ([username isEqualToString:@""])
    {
        justGoOn = YES;
        username = @"francesco1234";
        password = @"Test123%";
    }
    //if one of both textfields is empty, show an alert message
    if (!justGoOn && (userTextField.text == nil || [userTextField.text isEqualToString:@""] || passwordTextField.text == nil || [passwordTextField.text isEqualToString:@""]))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"incomplete input", nil)
                                              message:@"Please fill in both fields"
                                              preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    //if fields are filled correctly
    else
    {
        //send Login to database, call LoginService
        loginService = [[LoginService alloc] init];
        loginService.loginVC = self;
        [loginService checkLoginForUsername:username andForPassword:password];
        [navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)passwordForgottenClicked:(id)sender
{
    NSLog(@"password forgotten button clicked");
}

-(IBAction)registerPressed:(id)sender
{
    //Show RegisterVC
    [self performSegueWithIdentifier:@"registerVC" sender:sender];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return NO;
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

@end
