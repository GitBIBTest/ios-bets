//
//  AllSportsInARowCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 02/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "AllSportsInARowCell.h"

@implementation AllSportsInARowCell
{
    NSInteger highlightedImage;
}

- (void)layoutSubviews
{
    UIImageView *sport1ImageView = (UIImageView *)[self.contentView viewWithTag:1];
    sport1ImageView.image = [sport1ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport2ImageView = (UIImageView *)[self.contentView viewWithTag:2];
    sport2ImageView.image = [sport2ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport3ImageView = (UIImageView *)[self.contentView viewWithTag:3];
    sport3ImageView.image = [sport3ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport4ImageView = (UIImageView *)[self.contentView viewWithTag:4];
    sport4ImageView.image = [sport4ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport5ImageView = (UIImageView *)[self.contentView viewWithTag:5];
    sport5ImageView.image = [sport5ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport6ImageView = (UIImageView *)[self.contentView viewWithTag:6];
    sport6ImageView.image = [sport6ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport7ImageView = (UIImageView *)[self.contentView viewWithTag:7];
    sport7ImageView.image = [sport7ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *sport8ImageView = (UIImageView *)[self.contentView viewWithTag:8];
    sport8ImageView.image = [sport8ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UITapGestureRecognizer *tappedImage1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
    [sport1ImageView addGestureRecognizer:tappedImage1];
    UITapGestureRecognizer *tappedImage2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
    [sport2ImageView addGestureRecognizer:tappedImage2];
//    UITapGestureRecognizer *tappedImage3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
//    [sport3ImageView addGestureRecognizer:tappedImage3];
//    UITapGestureRecognizer *tappedImage4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
//    [sport4ImageView addGestureRecognizer:tappedImage4];
//    UITapGestureRecognizer *tappedImage5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
//    [sport5ImageView addGestureRecognizer:tappedImage5];
//    UITapGestureRecognizer *tappedImage6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
//    [sport6ImageView addGestureRecognizer:tappedImage6];
//    UITapGestureRecognizer *tappedImage7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
//    [sport7ImageView addGestureRecognizer:tappedImage7];
//    UITapGestureRecognizer *tappedImage8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTouched:)];
//    [sport8ImageView addGestureRecognizer:tappedImage8];
    
    [sport1ImageView setUserInteractionEnabled:YES];
    [sport2ImageView setUserInteractionEnabled:YES];
//    [sport3ImageView setUserInteractionEnabled:YES];
//    [sport4ImageView setUserInteractionEnabled:YES];
//    [sport5ImageView setUserInteractionEnabled:YES];
//    [sport6ImageView setUserInteractionEnabled:YES];
//    [sport7ImageView setUserInteractionEnabled:YES];
//    [sport8ImageView setUserInteractionEnabled:YES];
}

- (void)highlightImageWithTag:(NSInteger)sportIdentifier
{
    UIImageView *highlightedSportImageView = [self viewWithTag:sportIdentifier];
    highlightedSportImageView.tintColor = BIBredcolor;
    highlightedImage = sportIdentifier;
}

- (void)imageTouched:(id)sender
{
    UIImageView *touchedImageView = (UIImageView *)[sender view];
    [touchedImageView setUserInteractionEnabled:NO];
    NSInteger senderTag = [touchedImageView tag];
 
    if (senderTag != highlightedImage)
    {
        [self.delegate sportLogoHasBeenTouched:F(@"%d",(int)senderTag)];
    }
}

@end
