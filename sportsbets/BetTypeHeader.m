//
//  BetTypeHeader.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "BetTypeHeader.h"

@implementation BetTypeHeader

@synthesize containerView, betType1, betType2, betType3;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        CGFloat cellHeight = HEIGHT(self);
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat buttonsWidth = screenWidth/3.0;
        
        self.activeColor = [UIColor whiteColor];
        self.inactiveColor = [UIColor lightGrayColor];
        
        betType1 = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, buttonsWidth, cellHeight)];
        betType2 = [[UIButton alloc] initWithFrame:CGRectMake(buttonsWidth, 0.0, buttonsWidth, cellHeight)];
        betType3 = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth-buttonsWidth, 0.0, buttonsWidth, cellHeight)];
        
        [betType1 setTitle:LocalizedString(@"bet_mode_single",nil) forState:UIControlStateNormal];
        [betType1.titleLabel setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        [betType1 setBackgroundColor:[UIColor grayColor]];
        [betType1 setTag:1];
        [betType1 addTarget:self action:@selector(betTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [betType2.titleLabel setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        [betType2 setTitle:LocalizedString(@"bet_mode_combi", nil) forState:UIControlStateNormal];
        [betType2 setBackgroundColor:[UIColor grayColor]];
        [betType2 setTag:2];
        [betType2 addTarget:self action:@selector(betTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [betType3 setTitle:LocalizedString(@"bet_mode_system", nil) forState:UIControlStateNormal];
        [betType3.titleLabel setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
        [betType3 setBackgroundColor:[UIColor grayColor]];
        [betType3 setTag:3];
        [betType3 addTarget:self action:@selector(betTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:betType1];
        [self.contentView addSubview:betType2];
        [self.contentView addSubview:betType3];
    }
    return self;
}

-(void)betTypeClicked:(id)sender
{
    [self.delegate betslipBetTypeChanged:[sender tag]];
}

@end
