//
//  Localization.h
//  Sports Bets by Bet IT Best
//
//  Created by Dummy on 14.07.15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

//class for localization --> translation of strings or urls of the app
//method LocalizedString(_key,_comment) can be used in the app for translations
//a translation for the _key string must be in the JSON-files of every language
//if there is no translation the method will return the _key itself without translation
// --> so no error will cause if there is no translation possible


#import <Foundation/Foundation.h>

#define LocalizedString(_key,_comment) [[Localization sharedInstance]localizedStringForKey:(_key) comment:(_comment)]

@interface Localization : NSObject
{
    NSDictionary *_dictionary;
}

+(id)sharedInstance;
-(NSString *)localizedStringForKey:(NSString *)key comment:(NSString *)comment;

@end
