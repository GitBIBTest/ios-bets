//
//  TextFieldNoCursor.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 28/11/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldNoCursor : UITextField

@end
