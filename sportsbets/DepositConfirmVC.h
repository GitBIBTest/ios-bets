//
//  DepositConfirmVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 16/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "Localization.h"
#import "Macros.h"

@interface DepositConfirmVC : UITableViewController

@property (nonatomic,retain) IBOutlet UILabel *overviewLabel;
@property (nonatomic,retain) IBOutlet UILabel *amountLabel;
@property (nonatomic,retain) IBOutlet UILabel *amountDisplay;

@property (nonatomic,retain) IBOutlet UILabel *ibanDisplay;
@property (nonatomic,retain) IBOutlet UILabel *bicDisplay;
@property (nonatomic,retain) IBOutlet UILabel *accountHolderLabel;
@property (nonatomic,retain) IBOutlet UILabel *accountHolderDisplay;
@property (nonatomic,retain) IBOutlet UIButton *transferConfirmButton;

@property (nonatomic,retain) IBOutlet UILabel *cardTypeLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardTypeDisplay;
@property (nonatomic,retain) IBOutlet UILabel *cardNumberLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardNumberDisplay;
@property (nonatomic,retain) IBOutlet UILabel *cardCodeLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardCodeDisplay;
@property (nonatomic,retain) IBOutlet UILabel *expirationLabel;
@property (nonatomic,retain) IBOutlet UILabel *expirationDisplay;
@property (nonatomic,retain) IBOutlet UILabel *cardHolderLabel;
@property (nonatomic,retain) IBOutlet UILabel *cardHolderDisplay;
@property (nonatomic,retain) IBOutlet UIButton *cardConfirmButton;

@property (nonatomic) NSArray *passedData;
@property (nonatomic) NSString *passedPaymentType;

@end
