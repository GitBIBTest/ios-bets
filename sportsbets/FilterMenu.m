//
//  FilterMenu.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "FilterMenu.h"

@implementation FilterMenu
{
    BOOL isFirstRun;
    NSArray *categories;
    NSArray *tournaments;
    MBProgressHUD *progressHud;
    NSInteger expandedSection;
    NSInteger currentlySelectedRow;
    NSString *category_uid;
    NSString *tournament_uid;
    NSInteger lastLoadedSport;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.height = [self heightForTableView];
    self.tableView.frame = tableViewFrame;
    
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"CategoryHeaderView"];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //first run configuration
    isFirstRun = NO;
    
    if (![userDefaults valueForKey:@"version"] || [userDefaults floatForKey:@"version"] != [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue])
    {
        isFirstRun = YES;
    }
    
    bool downloadFiltersAnyway = YES;
    
    /* Download live filters */
    if (isFirstRun || downloadFiltersAnyway)
    {
        //first of all empty the database
        [self emptyDatabase];
        CategoryTournamentService *categoryTournamentService = [[CategoryTournamentService alloc]init];
        categoryTournamentService.delegate = self;
        progressHud = [MBProgressHUD showHUDAddedTo:[self.view viewWithTag:999] animated:YES];
        progressHud.labelText = @"Downloading filters...";
    }
    else
    {
        [self loadCategories];
    }
    
    expandedSection = -1;
    currentlySelectedRow = -1;
    lastLoadedSport = [[FilterMaster sharedFilterMaster] sportIdentifier];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([[FilterMaster sharedFilterMaster] sportIdentifier] != lastLoadedSport)
    {
        category_uid = [[FilterMaster sharedFilterMaster] filter_category_uid];
        tournament_uid = [[FilterMaster sharedFilterMaster] filter_tournament_uid];
        
        expandedSection = (category_uid != nil) ? [category_uid intValue] : -1;
        currentlySelectedRow = (tournament_uid != nil) ? [tournament_uid intValue] : -1;
        
        [self loadCategories];
    }
}

-(void)emptyDatabase
{
    NSFetchRequest *allCategories = [[NSFetchRequest alloc] init];
    [allCategories setEntity:[NSEntityDescription entityForName:@"Category" inManagedObjectContext:self.managedObjectContext]];
    [allCategories setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *categoriesResult = [self.managedObjectContext executeFetchRequest:allCategories error:&error];
    
    for (NSManagedObject *toDelete in categoriesResult) {
        [self.managedObjectContext deleteObject:toDelete];
    }
    
    NSFetchRequest *allTournaments = [[NSFetchRequest alloc] init];
    [allTournaments setEntity:[NSEntityDescription entityForName:@"Tournament" inManagedObjectContext:self.managedObjectContext]];
    [allTournaments setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSArray *tournamentsResult = [self.managedObjectContext executeFetchRequest:allTournaments error:&error];
    
    for (NSManagedObject *toDelete in tournamentsResult) {
        [self.managedObjectContext deleteObject:toDelete];
    }
    [self.managedObjectContext save:nil];
}

-(void)loadCategories
{
    //fill category array for category pickerView
    NSPredicate *predicate_category = [NSPredicate predicateWithFormat:@"(sport_uid LIKE[c] %@)", F(@"%d",(int)[FilterMaster sharedFilterMaster].sportIdentifier)];
    NSError *fetchError = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Category"];
    [fetchRequest setPredicate:predicate_category];
    
    //sort categories by translated name
    NSSortDescriptor *sort_by_translatedCategory = [NSSortDescriptor sortDescriptorWithKey:@"translated_category_name" ascending:YES];
    NSArray *sorting = [[NSArray alloc]initWithObjects:sort_by_translatedCategory, nil];
    [fetchRequest setSortDescriptors:sorting];
    
    categories = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    lastLoadedSport = [[FilterMaster sharedFilterMaster] sportIdentifier];
    
    [self.tableView reloadData];
}

-(void)categoriesTournamentsDownloaded
{
    [progressHud hide:YES];
    [[NSUserDefaults standardUserDefaults] setFloat:[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion" ] floatValue] forKey:@"version"];
    [self loadCategories];
}

- (CGFloat)heightForTableView
{
    return CGRectGetHeight([[UIScreen mainScreen] bounds]) -
    (CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]) +
     CGRectGetHeight(self.navigationController.navigationBar.frame) +
     CGRectGetHeight(self.tabBarController.tabBar.frame));
}

- (CGFloat)widthForTableView
{
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberToReturn = [categories count];
    
    return numberToReturn;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //returns number of rows, number of rows is like number of leagues per section/country
    
    NSInteger numberToReturn = 0;
    
    if (section == expandedSection)
    {
        numberToReturn = [tournaments count];
    }
    else
    {
        numberToReturn = 0;
    }
    
    return numberToReturn;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"TournamentCell";
    UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel *tournamentNameLabel = [myCell viewWithTag:20];
    tournamentNameLabel.text = [[tournaments objectAtIndex:indexPath.row] valueForKey:@"tournament_name"];

    //we should add a checkmark here to make the selection more clear to the user
    if (indexPath.row == currentlySelectedRow)
    {
        [tournamentNameLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:11.0]];
        [tournamentNameLabel setTextColor:[UIColor whiteColor]];
    }
    
    return myCell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *headerViewIdentifier = @"CategoryHeaderView";
    UITableViewHeaderFooterView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerViewIdentifier];
    
    sectionView.tag = section;
    
    NSString *cellIdentifier = @"CategoryHeaderCell";
    UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [myCell setFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UILabel *categoryNameLabel = [myCell viewWithTag:20];
    categoryNameLabel.text = [[categories objectAtIndex:section] valueForKey:@"translated_category_name"];
    
    if (section == expandedSection)
    {
        [categoryNameLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:13.0]];
        [categoryNameLabel setTextColor:[UIColor whiteColor]];
    }
    
    //add gesture recognizer to headercell, so header becomes clickable
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionTapped:)];
    [sectionView addGestureRecognizer:headerTapped];

    NSString *categoryName;
    if ([categoryNameLabel.text length] > 8 && [[categoryNameLabel.text substringFromIndex:[categoryNameLabel.text length]-8] isEqualToString:@" Amateur"])
    {
        categoryName = [categoryNameLabel.text substringToIndex:[categoryNameLabel.text length]-8];
        NSLog(@"%@", categoryName);
    }
    else
    {
        categoryName = categoryNameLabel.text;
    }

    UIImageView *countryFlag = [myCell viewWithTag:10];
    [countryFlag sd_setImageWithURL:[NSURL URLWithString:F(@"https://www.betitbest.com/fileadmin/user_upload/logos/app_logos/%@.png", [categoryName urlEncodeUsingEncoding:NSUTF8StringEncoding])]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                       //do stuff after completion
                                   }];

    
    [sectionView addSubview:myCell];
    
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (IBAction)sectionTapped:(UITapGestureRecognizer *)recognizer
{
    //tap on section -> change boolarray --> boolvalue for tapped section --> display rows for section
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:recognizer.view.tag];
    
    expandedSection = (indexPath.section != expandedSection) ? indexPath.section : -1;
    
    if (expandedSection != -1)
    {
        category_uid = [[categories objectAtIndex:indexPath.section] valueForKey:@"uid"];
    
        NSPredicate *predicate_category = [NSPredicate predicateWithFormat:@"(category_uid LIKE[c] %@)", category_uid];
        NSError *fetchError = nil;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Tournament"];
        [fetchRequest setPredicate:predicate_category];
        NSSortDescriptor *sort_by_sorting = [NSSortDescriptor sortDescriptorWithKey:@"sorting" ascending:YES];
        NSArray *sorting = [[NSArray alloc]initWithObjects:sort_by_sorting, nil];
        [fetchRequest setSortDescriptors:sorting];
        tournaments = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    }
    else
    {
        category_uid = nil;
    }
    [[FilterMaster sharedFilterMaster] setFilter_category_uid:category_uid];
    tournament_uid = nil;
    currentlySelectedRow = -1;
    [[FilterMaster sharedFilterMaster] setFilter_tournament_uid:tournament_uid];
    
    NSLog(@"category uid = %@", category_uid);
    
    [self.tableView reloadData];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadTableView" object:self];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentlySelectedRow = (indexPath.row != currentlySelectedRow) ? indexPath.row : -1;
    
    if (currentlySelectedRow != -1)
    {
        tournament_uid = [[tournaments objectAtIndex:indexPath.row] valueForKey:@"uid"];
    }
    else
    {
        tournament_uid = nil;
    }
    [[FilterMaster sharedFilterMaster] setFilter_tournament_uid:tournament_uid];
    
    NSLog(@"tournament uid = %@", tournament_uid);
    
    [self.tableView reloadData];
        
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadTableView" object:self];
}

//to retrieve managed object context
-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end