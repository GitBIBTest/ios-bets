//
//  MeinKontoHeaderCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 20/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeinKontoHeaderCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *usernameLabel;
@property (nonatomic, retain) IBOutlet UILabel *balanceLabel;


@end
