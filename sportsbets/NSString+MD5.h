//
//  NSString+MD5.h
//  Ogulo
//
//  Created by Samuel Becker on 25.07.13.
//  Copyright (c) 2013 Samuel Becker. All rights reserved.
//

#include <UIKit/UIKit.h>

@interface NSString(MD5)

- (NSString *)MD5;

@end