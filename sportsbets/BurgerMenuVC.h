//
//  BurgerMenuVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 23/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Localization.h"
#import "SoccerBetsVC.h"

@interface BurgerMenuVC : UITableViewController

@end
