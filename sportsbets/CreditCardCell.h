//
//  CreditCardCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 06/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditCardCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *creditCardTypeLabel;
@property (nonatomic,retain) IBOutlet UILabel *creditCardNumberLabel;

@end
