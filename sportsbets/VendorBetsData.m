//
//  VendorBetsData.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 03/12/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "VendorBetsData.h"

@implementation VendorBetsData

- (id) init
{
    if (!self)
    {
        self = [super init];
    }
    self.vendorArray = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)addVendorBets:(VendorBetsModel *)vendorBetsData
{
    [self.vendorArray addObject:vendorBetsData];
}

@end