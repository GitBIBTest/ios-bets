//
//  SofortIdentWebView.VC.m
//  Sports Betting by Bet IT Best
//
//  Created by Aleksandar Penev on 21/04/2016.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "SofortIdentWebViewVC.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSString+MD5.h"

@implementation SofortIdentWebViewVC

@synthesize sofortIdentWebView;
@synthesize closeButton;
@synthesize firstname;
@synthesize surname;
@synthesize area;
@synthesize areaCode;
@synthesize houseNumber;
@synthesize street;
@synthesize birthday;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *url = [NSString stringWithFormat:@"https://www.sofort.com/payment/agecheck?user_id=92535&project_id=218518&firstname=%@&lastname=%@&street=%@+%@&city=%@&zipcode=%@&birthday=%@&address_country_id=&bank_code=&account_country_id=&user_variable_0=&user_variable_1=&user_variable_2=&user_variable_3=&user_variable_4=&user_variable_5=&hash=%@", [firstname urlEncodeUsingEncoding:NSUTF8StringEncoding], [surname urlEncodeUsingEncoding:NSUTF8StringEncoding], [street urlEncodeUsingEncoding:NSUTF8StringEncoding], [houseNumber urlEncodeUsingEncoding:NSUTF8StringEncoding], [area urlEncodeUsingEncoding:NSUTF8StringEncoding], [areaCode urlEncodeUsingEncoding:NSUTF8StringEncoding], [birthday urlEncodeUsingEncoding:NSUTF8StringEncoding], [self buildHash]];
    
    //[string urlEncodeUsingEncoding:NSUTF8StringEncoding]
    
    [sofortIdentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    sofortIdentWebView.delegate = self;
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewWillDisappear:(BOOL)animated {
    [sofortIdentWebView stopLoading];
    sofortIdentWebView.delegate = nil;
    //mWebView = nil;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Finished");
}

- (IBAction)closeWebView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSString *)buildHash{
    NSString * value;
    
    NSString *stringToHash = [NSString stringWithFormat:@"92535|218518|%@|%@|%@ %@|%@|%@|%@||||||||||scJt78dGseWEamM1OnA7", firstname, surname, street, houseNumber, area, areaCode, birthday];
    
    //stringToHash = @"12345|54321|Max|Mustermann|Unter den Linden 77|Berlin|10117|1978-09-24|DE||DE|123456||||||4-8-15-16-23-42";
    
    /*const char* str = [stringToHash UTF8String];
     unsigned char result[CC_SHA256_DIGEST_LENGTH];
     CC_SHA256(str, strlen(str), result);
     
     NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
     for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
     {
     [ret appendFormat:@"%02x",result[i]];
     }*/
    
    //value = [NSString stringWithFormat:@"%@", ret];
    value = [stringToHash MD5];
    /*user_id|project_id|firstname|lastname|street|city|zipcode|birthday|
     address_country_id|bank_code|account_country_id|user_variable_0|
     user_variable_1|user_variable_2|user_variable_3|user_variable_4|
     user_variable_5|project_password*/
    
    // 303af25fcce2f3ff1cde84b7a05e867450fe8918139cc70892ea60714c058131
    return value;
}
@end
