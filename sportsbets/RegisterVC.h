//
//  RegisterVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 26/11/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Localization.h"
#import "SWRevealViewController.h"
#import "RegisterService.h"
#import "Macros.h"

@interface RegisterVC : UITableViewController <UITextFieldDelegate, RegisterServiceProtocol>

@property (nonatomic,retain) IBOutlet UILabel *registerLabel;
@property (nonatomic,retain) IBOutlet UILabel *nameLabel;
@property (nonatomic,retain) IBOutlet UITextField *nameTextField;
@property (nonatomic,retain) IBOutlet UILabel *surnameLabel;
@property (nonatomic,retain) IBOutlet UITextField *surnameTextField;
@property (nonatomic,retain) IBOutlet UILabel *birthdateLabel;
@property (nonatomic,retain) IBOutlet UITextField *birthdateDayTextField;
@property (nonatomic,retain) IBOutlet UITextField *birthdateMonthTextField;
@property (nonatomic,retain) IBOutlet UITextField *birthdateYearTextField;
@property (nonatomic,retain) IBOutlet UILabel *address_streetLabel;
@property (nonatomic,retain) IBOutlet UITextField *address_streetTextField;
@property (nonatomic,retain) IBOutlet UITextField *address_housenrTextField;
@property (nonatomic,retain) IBOutlet UILabel *address_plzLabel;
@property (nonatomic,retain) IBOutlet UITextField *address_plzTextField;
@property (nonatomic,retain) IBOutlet UILabel *address_cityLabel;
@property (nonatomic,retain) IBOutlet UITextField *address_cityTextField;
@property (nonatomic,retain) IBOutlet UILabel *emailLabel;
@property (nonatomic,retain) IBOutlet UITextField *emailTextField;
@property (nonatomic,retain) IBOutlet UILabel *phoneNumberLabel;
@property (nonatomic,retain) IBOutlet UITextField *phoneNumberTextField;
@property (nonatomic,retain) IBOutlet UIButton *enroll;

@end