//
//  UserBetsModel.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserBetsModel : NSObject

@property (strong,nonatomic) NSString *bet_sport;
@property (strong,nonatomic) NSString *bet_sport_uid;
@property (strong,nonatomic) NSString *vendor;
@property (strong,nonatomic) NSString *vendor_uid;
@property (strong,nonatomic) NSString *bet_type;
@property (strong,nonatomic) NSString *bet_status;
@property (strong,nonatomic) NSString *uid;
@property (strong,nonatomic) NSString *playerbet_uid;
@property (strong,nonatomic) NSString *bet_datetime_placed;
@property (strong,nonatomic) NSString *match_datetime;
@property (strong,nonatomic) NSString *bet_team1;
@property (strong,nonatomic) NSString *bet_team2;
@property (strong,nonatomic) NSString *bet_winner_team;
@property (strong,nonatomic) NSString *rate;
@property (strong,nonatomic) NSString *stake;
@property (strong,nonatomic) NSString *possible_win;
@property (strong,nonatomic) NSString *bet_result;

@end
