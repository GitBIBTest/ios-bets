//
//  AccountOverviewVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 12/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Localization.h"
#import "Macros.h"
#import "MBProgressHUD.h"
#import "UserData.h"
#import "AccountOverviewService.h"

@interface AccountOverviewVC : UITableViewController <AccountOverviewServiceProtocol>

@property (retain, nonatomic) IBOutlet UIBarButtonItem *burgerButton;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *closeButton;

@property (retain, nonatomic) IBOutlet UILabel *titleLabel1;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel2;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel3;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel4;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel5;
@property (retain, nonatomic) IBOutlet UILabel *displayLabel1;
@property (retain, nonatomic) IBOutlet UILabel *displayLabel2;
@property (retain, nonatomic) IBOutlet UILabel *displayLabel3;
@property (retain, nonatomic) IBOutlet UILabel *displayLabel4;
@property (retain, nonatomic) IBOutlet UILabel *displayLabel5;
@property (retain, nonatomic) IBOutlet UILabel *displayLabel6;

@end
