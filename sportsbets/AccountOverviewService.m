//
//  AccountOverviewService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 18/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "AccountOverviewService.h"

@interface AccountOverviewService ()
{
    NSMutableData *_downloadedData;
    NSInteger calledFunction;
}

@end

@implementation AccountOverviewService

-(void)loadAccountBalanceForUser:(NSString *)user andForPassword:(NSString *)password
{
    calledFunction = 1;
    
    NSString * url = @"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceAccountBalance.php?userid=";
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"%@%@%@%@", url, user,@"&password=", password)];
    NSLog(@"url user balance = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadAccountOverviewForUser:(NSString *)user andForPassword:(NSString *)password
{
    calledFunction = 2;
    
    NSString * url = @"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceAccountOverview.php?userid=";
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"%@%@%@%@", url, user,@"&password=", password)];
    NSLog(@"url user info = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    if ([topLevelArray count] > 0)
    {
        if (calledFunction == 1)
        {
            [self.delegate accountBalanceDownloaded:[[topLevelArray objectAtIndex:0] valueForKey:@"account_balance"]];
        }
        else if (calledFunction == 2)
        {
            //has 2 rows top, one for limit type 1 (month), one for limit type 2 (bet)
            NSMutableDictionary *valuesToReturn = [[NSMutableDictionary alloc] init];
            
            //set balance
            NSString *fetchedAccountBalance = [[topLevelArray objectAtIndex:0] valueForKey:@"account_balance"];
            [valuesToReturn setValue:fetchedAccountBalance forKey:@"account_balance"];
        
            //set limits if not null
            for (NSObject *k in topLevelArray)
            {
                if ([[k valueForKey:@"limit_type"] isEqual:[NSNull null]] || [k valueForKey:@"limit_type"] == nil)
                {
                    break;
                }
                else if ([[k valueForKey:@"limit_type"] isEqualToString:@"1"])
                {
                    NSString *fetchedBetsLimitMonth = [k valueForKey:@"limit_amount"];
                    [valuesToReturn setValue:fetchedBetsLimitMonth forKey:@"bets_limit_month"];
                }
                else if ([[k valueForKey:@"limit_type"] isEqualToString:@"2"])
                {
                    NSString *fetchedBetsLimitBet = [k valueForKey:@"limit_amount"];
                    [valuesToReturn setValue:fetchedBetsLimitBet forKey:@"bets_limit_bet"];
                }
            }
            
            if ([valuesToReturn valueForKey:@"bets_limit_month"] == nil || [[valuesToReturn valueForKey:@"bets_limit_month"] isEqual:[NSNull null]])
            {
                [valuesToReturn setValue:DefaultBetsLimitMonth forKey:@"bets_limit_month"];
            }

            if ([valuesToReturn valueForKey:@"bets_limit_bet"] == nil || [[valuesToReturn valueForKey:@"bets_limit_bet"] isEqual:[NSNull null]])
            {
                [valuesToReturn setValue:DefaultBetsLimitBet forKey:@"bets_limit_bet"];
            }
            
            double sum_stake = 0;
            if ([[topLevelArray objectAtIndex:0] valueForKey:@"sum_stake"] != nil && ![[[topLevelArray objectAtIndex:0] valueForKey:@"sum_stake"] isEqual:[NSNull null]])
            {
                sum_stake = [[[topLevelArray objectAtIndex:0] valueForKey:@"sum_stake"] doubleValue];
            }
            
            CGFloat account_credit = MIN([[valuesToReturn valueForKey:@"account_balance"] doubleValue],
                                         [[valuesToReturn valueForKey:@"bets_limit_month"] doubleValue] - sum_stake
                                         );
            NSString *fetchedAccountCredit = F(@"%.02f", account_credit);
            [valuesToReturn setValue:fetchedAccountCredit forKey:@"account_credit"];
            
            [valuesToReturn setValue:[[topLevelArray objectAtIndex:0] valueForKey:@"liquidity_proved"] forKey:@"liquidity_proved"];
            
            NSArray *arrayToReturn = [NSArray arrayWithObject:valuesToReturn];
            
            [self.delegate accountOverviewDownloaded:arrayToReturn];
        }
    }
    
    //TODO Logout with an error if the [topLevelArray count] == 0 (means password was wrong)
}

@end
