//
//  BettingSlipVendorCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 09/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "BettingSlipVendorCell.h"

@implementation BettingSlipVendorCell
{
    UIImageView *logoImageView;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat vendorNameWidth = screenWidth/2.0;
        CGFloat vendorRateWidth = 60.0;
        
        logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20.0, 2.0, 50.0, 40.0)];
        [logoImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.vendorName = [[UILabel alloc] initWithFrame:CGRectMake(75.0, 15.0, vendorNameWidth, 17.0)];
        self.vendorRateButton = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth - 10.0 - vendorRateWidth, 11.0, vendorRateWidth, 24.0)];
        [self.vendorRateButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        
        //show custom separator line
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, self.frame.size.height-1.0, screenWidth - 20.0, 1.0/[UIScreen mainScreen].scale)];
        separatorLineView.backgroundColor = [UIColor lightGrayColor];
        
        [self addSubview:logoImageView];
        [self addSubview:self.vendorName];
        [self addSubview:self.vendorRateButton];
        [self addSubview:separatorLineView];
    }
    return self;
}

-(void)selectCell
{
    [self.vendorRateButton setBackgroundColor:BIBredcolor];
    [self.vendorRateButton.titleLabel setTextColor:[UIColor whiteColor]];
    [self.vendorName setFont:[UIFont fontWithName:@"Montserrat-bold" size:14.0]];
}

-(void)deselectCell
{
    [self.vendorRateButton setBackgroundColor:[UIColor lightGrayColor]];
    [self.vendorRateButton.titleLabel setTextColor:[UIColor whiteColor]];
    [self.vendorName setFont:[UIFont fontWithName:@"Montserrat" size:14.0]];
}

-(void)setLogo:(NSString *)logoImageName
{
    [logoImageView setImage:[UIImage imageNamed:logoImageName]];
}

@end
