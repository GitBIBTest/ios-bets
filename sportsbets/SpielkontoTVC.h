//
//  SpielkontoViewController.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 20/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MeinKontoHeaderCell.h"
#import "UserData.h"
#import "AccountOverviewService.h"
#import "Localization.h"

@interface SpielkontoTVC : UITableViewController <AccountOverviewServiceProtocol>

@property (strong, nonatomic) IBOutlet UILabel *nameLabelText;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;

@property (strong, nonatomic) IBOutlet UILabel *paymentsSectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *depositLabel;
@property (strong, nonatomic) IBOutlet UILabel *cashOutLabel;
@property (strong, nonatomic) IBOutlet UILabel *depositLimitsLabel;
@property (strong, nonatomic) IBOutlet UILabel *myAccountSectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *userDataLabel;
@property (strong, nonatomic) IBOutlet UILabel *changePasswordLabel;
@property (strong, nonatomic) IBOutlet UILabel *ageVerificationLabel;
@property (strong, nonatomic) IBOutlet UILabel *betLimitsLabel;
@property (strong, nonatomic) IBOutlet UILabel *blacklistLabel;
@property (strong, nonatomic) IBOutlet UILabel *myBetsSectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentBetsLabel;
@property (strong, nonatomic) IBOutlet UILabel *myBetsLabel;
@property (strong, nonatomic) IBOutlet UIButton *logOutButton;

@end
