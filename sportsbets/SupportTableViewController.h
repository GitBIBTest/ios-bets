//
//  SupportViewController.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 25/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SupportService.h"
#import "Macros.h"
#import "Localization.h"
#import "SupportObject.h"
#import "MBProgressHUD.h"

@interface SupportTableViewController : UITableViewController <SupportServiceProtocol, UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *userNameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *surnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

@property (strong, nonatomic) IBOutlet UITextField *categoryTextField;
@property (strong, nonatomic) IBOutlet UITextField *reasonTextField;
@property (strong, nonatomic) IBOutlet UITextView *messageTextView;

@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *surnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *reasonLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) UIPickerView *categorypicker;
@property (strong, nonatomic) UIPickerView *reasonPicker;

@property (strong, nonatomic) NSArray *downloadedSupportObjects;

@property (strong, nonatomic) NSMutableString *selectedCategoryUID;
@property (strong, nonatomic) NSMutableString *selectedReasonUID;

@end
