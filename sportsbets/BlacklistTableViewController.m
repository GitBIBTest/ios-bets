//
//  BlacklistTableViewController.m
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 22/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "BlacklistTableViewController.h"

@interface BlacklistTableViewController ()

@end

@implementation BlacklistTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //create a new back button named cancel to make it clear to the User that he is not saving the changes when he returns to the previous view
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem = newBackButton;

    //create a right bar button item to save changes and push them in the database
    self.saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save Changes" style:UIBarButtonItemStyleDone target:self action:@selector(Save:)];
    self.saveButton.enabled = false;
    self.navigationItem.rightBarButtonItem = self.saveButton;
}

//this method is called when the user presses the cancel Button
- (void) back:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Your changes will not be saved"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {

    self.vendors = [[NSMutableArray alloc] init];

    //for testing purposes I created dummy data which will be replaced when the service or an importer is done
    Vendor *vendor1 = [[Vendor alloc] init];
    Vendor *vendor2 = [[Vendor alloc] init];
    Vendor *vendor3 = [[Vendor alloc] init];
    Vendor *vendor4 = [[Vendor alloc] init];

    vendor1.name = @"tipico";
    vendor2.name = @"bet365";
    vendor3.name = @"sportingbet";
    vendor4.name = @"bwin";

    vendor1.blacklisted = YES;
    vendor2.blacklisted = YES;
    vendor3.blacklisted = NO;
    vendor4.blacklisted = NO;

    vendor1.imageURL = @"4ea181c25d572b3a017493207a72b327";
    vendor2.imageURL = @"523565d1596eda298d5bf8ab78069e43";
    vendor3.imageURL = @"6ead16f97a6a6e1b925b40595b27daf3";
    vendor4.imageURL = @"68020226382d35057c8312b661b280a4";

    [self.vendors addObject:vendor1];
    [self.vendors addObject:vendor2];
    [self.vendors addObject:vendor3];
    [self.vendors addObject:vendor4];
}

//this method is called when the user presses the save Button, once the web service is done, the adjustmnes should be implemented here
-(IBAction)Save:(id)sender
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"toDo: integrate web service in order to push changes"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return self.vendors.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BetVendorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vendorCell" forIndexPath:indexPath];
    Vendor *vendorInRow = self.vendors[indexPath.row];
    [self decorate:cell with:vendorInRow];    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Vendor *vendorInRow = self.vendors[indexPath.row];
    BetVendorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vendorCell" forIndexPath:indexPath];
    self.saveButton.enabled = true;
    if (vendorInRow.blacklisted) {
        vendorInRow.blacklisted = NO;
    } else {
        vendorInRow.blacklisted = YES;
    }
    [self decorate:cell with:vendorInRow];

    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headercell"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (void)decorate:(BetVendorTableViewCell *)cell with:(Vendor*)vendor {
    cell.vendorName.text = vendor.name;
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", vendor.imageURL]];
    if (vendor.blacklisted) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

@end
