//
//  MyAccountVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 14/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UserInfoService.h"
#import "MBProgressHUD.h"
#import "UserData.h"

@interface PersonalDataVC : UITableViewController <UserInfoServiceProtocol, UITextFieldDelegate>

@property (nonatomic,retain) IBOutlet UITextField *username;
@property (nonatomic,retain) IBOutlet UITextField *nameTextField;
@property (nonatomic,retain) IBOutlet UITextField *surnameTextField;
@property (nonatomic,retain) IBOutlet UITextField *birthdayTextField;
@property (nonatomic,retain) IBOutlet UITextField *countryTextField;
@property (nonatomic,retain) IBOutlet UITextField *cityTextField;
@property (nonatomic,retain) IBOutlet UITextField *plzTextField;
@property (nonatomic,retain) IBOutlet UITextField *streetTextField;
@property (nonatomic,retain) IBOutlet UITextField *houseNrTextField;
@property (nonatomic,retain) IBOutlet UITextField *emailTextField;
@property (nonatomic,retain) IBOutlet UITextField *phoneTextField;

@property (strong, nonatomic) UIBarButtonItem *saveButton;

@end
