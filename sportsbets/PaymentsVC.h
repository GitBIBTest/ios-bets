//
//  PaymentsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"

@interface PaymentsVC : UITableViewController

@property (nonatomic,retain) IBOutlet UILabel *depositNowLabel;
@property (nonatomic,retain) IBOutlet UILabel *depositHistoryLabel;
@property (nonatomic,retain) IBOutlet UILabel *cashOutLabel;
@property (nonatomic,retain) IBOutlet UILabel *cashOutHistoryLabel;
@property (nonatomic,retain) IBOutlet UILabel *changeLimitsLabel;
@property (nonatomic,retain) IBOutlet UILabel *myCardsLabel;

@end
