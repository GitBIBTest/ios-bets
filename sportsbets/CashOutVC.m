//
//  CashOutVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "CashOutVC.h"

@implementation CashOutVC
{
    MBProgressHUD *progressHud;
}

@synthesize amountTextField, ibanTextField, bicTextField, accountHolderTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    //place the done button on the "amount" textfield
    UIToolbar *toolBarAmountTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarAmountTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *amountTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                               target:self
                                                                                               action:@selector(amountTextFieldDoneTouched:)];
    [toolBarAmountTextField setItems:[NSArray arrayWithObjects:
                                      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                    target:nil
                                                                                    action:nil],
                                      amountTextFieldDoneButton, nil]];
    amountTextField.inputAccessoryView = toolBarAmountTextField;
    
    //configure labels and other views
    self.amountLabel.text = LocalizedString(@"amount", nil);
    amountTextField.placeholder = LocalizedString(@"amount", nil);
    self.accountHolderLabel.text = LocalizedString(@"account_holder", nil);
    accountHolderTextField.placeholder = F(@"%@ %@", LocalizedString(@"name", nil), LocalizedString(@"surname", nil));
    [self.continueButton setTitle:LocalizedString(@"continue", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AccountOverviewService *accountOverviewService = [[AccountOverviewService alloc] init];
    accountOverviewService.delegate = self;
    
    UserData *userData = [UserData getInstance];
    
    [accountOverviewService loadAccountOverviewForUser:userData.userId andForPassword:userData.passwordEncrypted];
    
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHud.labelText = @"Loading...";
}

- (void)accountOverviewDownloaded:(NSArray *)userInfo
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setGroupingSeparator:[[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator]];
    
    userDisposableCash = F(@"%.2f", [[[userInfo objectAtIndex:0] valueForKey:@"account_balance"] floatValue]);
    
    self.maxLabel.text = F(@"Max. %@ €", [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[userDisposableCash floatValue]]]);
    [progressHud hide:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [progressHud hide:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 44;
        case 1:
            return 30;
        case 2:
            return 44;
        case 3:
            return 30;
        case 4:
            return 44;
        case 5:
            return 30;
        case 6:
            return 44;
        case 7:
            return 60;
        default:
            break;
    }
    return 44;
}

//------------------------------------------------------------------------------------
//Actions for buttons and misc functions
//------------------------------------------------------------------------------------

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    [self.view endEditing:YES];
    BOOL showAlert = NO;
    
    //sanitize the strings by removing (or trimming) spaces
    ibanTextField.text = [ibanTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    bicTextField.text = [bicTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    accountHolderTextField.text = [accountHolderTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    
    //an amount has to be inserted and not be higher than the user's disposable money
    if ([amountTextField.text floatValue] == 0 || [amountTextField.text floatValue] > [userDisposableCash floatValue])
    {
        showAlert = YES;
        self.amountLabel.textColor = [UIColor redColor];
        amountTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.amountLabel.textColor = [UIColor blackColor];
    }
    //german IBANs have always 22 digits
    if ([ibanTextField.text length] != 22)
    {
        showAlert = YES;
        self.ibanLabel.textColor = [UIColor redColor];
        ibanTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.ibanLabel.textColor = [UIColor blackColor];
    }
    //if bic has 8 digits, it's the bank's main office, so add 3 Xs
    if ([bicTextField.text length] == 8)
    {
        [self.bicTextField.text stringByAppendingString:@"XXX"];
        self.bicLabel.textColor = [UIColor blackColor];
    }
    else if ([bicTextField.text length] != 11)
    {
        showAlert = YES;
        self.bicLabel.textColor = [UIColor redColor];
        bicTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.bicLabel.textColor = [UIColor blackColor];
    }
    //name needs at least one space and no punctuation, symbols or numbers
    if ([accountHolderTextField.text length] < 3 ||
        [[accountHolderTextField.text componentsSeparatedByString:@" "] count] - 1 < 1 ||
        [accountHolderTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound ||
        [accountHolderTextField.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound ||
        [accountHolderTextField.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.accountHolderLabel.textColor = [UIColor redColor];
        accountHolderTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.accountHolderLabel.textColor = [UIColor blackColor];
    }
    
    if (showAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Errors in the input", nil)
                                              message:@"Please correct the highlighted fields and retry"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)amountTextFieldDoneTouched:(UIBarButtonItem *)sender
{
    //Convert whatever is written into a floatvalue with 2 decimal places
    amountTextField.text = F(@"%.2f", [[amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]);
    [self textFieldShouldReturn:amountTextField];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CashOutConfirmVC *cashOutConfirmVC = [segue destinationViewController];
    cashOutConfirmVC.passedData = [NSArray arrayWithObjects: amountTextField.text, ibanTextField.text, bicTextField.text, accountHolderTextField.text, nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([amountTextField isFirstResponder])
    {
        amountTextField.text = F(@"%.2f", [[amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]);
    }
    return YES;
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    if ([amountTextField isFirstResponder])
    {
        amountTextField.text = F(@"%.2f", [[amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]);
    }
    [self.view endEditing:YES];
}

@end