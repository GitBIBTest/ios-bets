//
//  BetstObject.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "BetsData.h"

@interface BetsObject : BetsData

+ (BetsObject*) sharedBetsObject;

@end