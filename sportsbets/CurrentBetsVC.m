//
//  CurrentBetsVC.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "CurrentBetsVC.h"

@implementation CurrentBetsVC
{
    MBProgressHUD *progressHud;
    bool data_retrievable;
    
    UserBetsService *userBetsService;
    
    NSMutableArray *currentBetsArray;
    NSInteger numberOfBetSections;
    NSInteger betsToLoad;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationItem.title = LocalizedString(@"current_bets", nil);
    
    //Register classes of custom cells
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"CurrentBetHeaderView"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    data_retrievable = NO;
    
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHud.labelText = @"Loading...";
    
    betsToLoad = 10;
    
    userBetsService = [[UserBetsService alloc] init];
    userBetsService.delegate = self;
    
    [self loadUserBets];
}

- (void)loadUserBets
{
    UserData *userData = [UserData getInstance];

    [userBetsService loadCurrentBetsForUser:userData.userId andPassword:userData.password limitTo:betsToLoad];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [progressHud hide:YES];
}

- (void)userBetsDownloaded
{
    currentBetsArray = [UserBetsObject sharedUserBetsObject].userBetsDataArray;
    
    if ([currentBetsArray count] > 0)
    {
        data_retrievable = YES;
    }
    
    [self.tableView reloadData];
    [progressHud hide:YES];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!data_retrievable)
    {
        return 1;
    }
    numberOfBetSections = [currentBetsArray count];
    
    //show load more button just if there are potentially more bets
    if (numberOfBetSections < betsToLoad)
    {
        return numberOfBetSections;
    }
    else
    {
        //+1 for the load more button
        return numberOfBetSections + 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < numberOfBetSections)
    {
        if (!data_retrievable)
        {
            return 1;
        }
        else
        {
            UserBetsData *userBetsData = [currentBetsArray objectAtIndex:section];
            
            NSInteger count = [userBetsData.userBetsArray count];
            
            return count;
        }
    }
    else //if (section == numberOfBetSections)
    {
        return 1;
    }
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 75;
    }
    else
    {
        return 60;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section < numberOfBetSections)
    {
        return 20;
    }
    else //if (section == numberOfBetSections)
    {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *headerViewIdentifier = @"CurrentBetHeaderView";
    UITableViewHeaderFooterView *currentBetsHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerViewIdentifier];
    
    NSString *cellIdentifier = @"CurrentBetHeaderCell";
    UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [myCell setFrame:CGRectMake(0, 0, tableView.frame.size.width, myCell.frame.size.height)];
    
    UserBetsData *currentUserBetsData = [currentBetsArray objectAtIndex:section];
    UserBetsModel *currentUserBet = [currentUserBetsData.userBetsArray firstObject];
    
    UILabel *betTypeLabel = [myCell viewWithTag:10];
    switch ([currentUserBet.bet_type intValue]) {
        case 1:
            betTypeLabel.text = LocalizedString(@"bet_mode_single", nil);
            break;
        case 2:
            betTypeLabel.text = LocalizedString(@"bet_mode_combi", nil);
            break;
        case 3:
            betTypeLabel.text = LocalizedString(@"bet_mode_system", nil);
            break;
    }
    UILabel *betDateLabel = [myCell viewWithTag:20];
    betDateLabel.text = currentUserBet.bet_datetime_placed;
    
    [currentBetsHeaderView addSubview:myCell];
    
    return currentBetsHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    UITableViewCell *myCell;
    
    if (indexPath.section < numberOfBetSections)
    {
        if (IsConnected())
        {
            NSMutableArray *currentUserBetsDataArray = [[currentBetsArray objectAtIndex:indexPath.section] userBetsArray];
            UserBetsModel *currentUserBet = [currentUserBetsDataArray objectAtIndex:indexPath.row];

            if (indexPath.row == 0)
            {
                cellIdentifier = @"CurrentBetCellWithOdds";
                myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                
                UILabel *oddsLabel = [myCell viewWithTag:50];
                UILabel *stakeLabel = [myCell viewWithTag:60];
                UILabel *winLabel = [myCell viewWithTag:70];
                UILabel *oddsTextLabel = [myCell viewWithTag:110];
                oddsTextLabel.text = LocalizedString(@"odds", nil);
                UILabel *stakeTextLabel = [myCell viewWithTag:120];
                stakeTextLabel.text = LocalizedString(@"stake", nil);
                UILabel *winTextLabel = [myCell viewWithTag:130];
                winTextLabel.text = LocalizedString(@"potential_profit", nil);
                
                //calculate the average odds for combi bets
                CGFloat combiOdds = 1;
                for (UserBetsModel *k in currentUserBetsDataArray) {
                    combiOdds *= [k.rate floatValue];
                }
                combiOdds /= [currentUserBetsDataArray count];
                
                NSString *currency = @"€";
                
                oddsLabel.text = F(@"%.2f", combiOdds);
                stakeLabel.text = F(@"%@ %@", currentUserBet.stake, currency);
                winLabel.text = F(@"%@ %@", currentUserBet.possible_win, currency);
            }
            else
            {
                cellIdentifier = @"CurrentBetCell";
                myCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                
                //show custom separator line
                UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 1.0, WIDTH(self.view)-20.0, 1.0 / [UIScreen mainScreen].scale)];
                separatorLineView.backgroundColor = [UIColor lightGrayColor];
                [myCell.contentView addSubview:separatorLineView];
            }

            UIImageView *sportImageView = [myCell viewWithTag:10];
            sportImageView.image = [UIImage imageNamed:currentUserBet.bet_sport];
            sportImageView.image = [sportImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            sportImageView.tintColor = [UIColor blackColor];

            UILabel *team1Label = [myCell viewWithTag:20];
            team1Label.text = F(@"-%@", currentUserBet.bet_team1);

            UILabel *team2Label = [myCell viewWithTag:30];
            team2Label.text = F(@"-%@", currentUserBet.bet_team2);

            UILabel *tipLabel = [myCell viewWithTag:40];
            switch ([currentUserBet.bet_winner_team intValue]) {
                case 0:
                    tipLabel.text = LocalizedString(@"draw", nil);
                    break;
                case 1:
                    tipLabel.text = currentUserBet.bet_team1;
                    break;
                case 2:
                    tipLabel.text = currentUserBet.bet_team2;
                    break;
                default:
                    break;
            }

            UILabel *winnerLabel = [myCell viewWithTag:150];
            winnerLabel.text = LocalizedString(@"winner", nil);

            return myCell;
        }
        else
        {
            cellIdentifier = @"NotReachableCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            [progressHud hide:YES];
            
            return cell;
        }
    }
    else //if (indexPath.section == numberOfBetSections)
    {
        if (data_retrievable)
        {
            cellIdentifier = @"LoadMoreCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            UIButton *loadMoreButton = (UIButton *)[cell viewWithTag:10];
            [loadMoreButton setTitle:LocalizedString(@"load_more", nil) forState:UIControlStateNormal];
            [loadMoreButton addTarget:self action:@selector(loadMoreBets) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else
        {
            cellIdentifier = @"NoBetsToDisplayCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            return cell;
        }
    }
}

-(void)loadMoreBets
{
    if (IsConnected())
    {
        betsToLoad += 10;
        
        progressHud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
        progressHud.labelText = @"Loading...";
        [self loadUserBets];
    }
    else
    {
        [self.tableView reloadData];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //todo show details
}

- (IBAction)closePressed:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
