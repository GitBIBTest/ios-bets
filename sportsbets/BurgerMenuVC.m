//
//  BurgerMenuVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 23/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import "BurgerMenuVC.h"
#import "SWRevealViewController.h"

@interface BurgerMenuVC ()

@end

@implementation BurgerMenuVC
{
    NSArray *sportTypes;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView setUserInteractionEnabled:YES];
    sportTypes = @[@"Logo" ,@"SoccerCell", @"BasketballCell", @"HandballCell", @"TennisCell", @"IcehockeyCell", @"FootballCell", @"RugbyCell", @"VolleyballCell"];
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [sportTypes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [sportTypes objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row == 0)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"logo-sportsbetting-white"];
    }
    else if (indexPath.row == 1)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Soccer"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"soccer", nil);
    }
    else if (indexPath.row == 2)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Basketball"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"basketball", nil);
    }
    else if (indexPath.row == 3)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Handball"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"handball", nil);
    }
    else if (indexPath.row == 4)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Tennis"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"tennis", nil);
    }
    else if (indexPath.row == 5)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Icehockey"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"icehockey", nil);
    }
    else if (indexPath.row == 6)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Football"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"football", nil);
    }
    else if (indexPath.row == 7)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Rugby"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"rugby", nil);
    }
    else if (indexPath.row == 8)
    {
        UIImageView *sportIcon = (UIImageView *)[cell viewWithTag:1];
        sportIcon.image = [UIImage imageNamed:@"Volleyball"];
        UILabel *sport = (UILabel *)[cell viewWithTag:2];
        sport.text = LocalizedString(@"volleyball", nil);
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 9)
    {
        return 10;
    }
    else
    {
        return 50;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //disable user interaction to avoid double clicking
    [self.tableView setUserInteractionEnabled:NO];
    if ( [segue isKindOfClass: [SWRevealViewController class]] )
    {
        UIViewController *dvc;
        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
        [navController setViewControllers: @[dvc] animated: NO ];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    }
}

@end