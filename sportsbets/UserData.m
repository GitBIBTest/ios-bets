//
//  UserData.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 05/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserData.h"

@implementation UserData

@synthesize userName;

static UserData *instance = nil;

+(UserData *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance = [UserData new];
        }
    }
    return instance;
}

+(void)resetUserData
{
    instance = nil;
}

@end
