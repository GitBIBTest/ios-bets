//
//  PlaceBetsService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/04/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "PlaceBetsService.h"

@implementation PlaceBetsService
{
    NSMutableData *_downloadedData;
    NSInteger betType; //1 single, 2 combi, 3 system
    NSInteger operationType; //1 check, 2 place
}

- (id)init
{
    if (!self)
    {
        self = [super init];
    }
    return self;
}

-(void)checkSingleBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password
{
    betType = 1;
    operationType = 1;
    
    NSString *betUID = [bet objectAtIndex:0];
    NSString *vendorUID = [bet objectAtIndex:1];
    NSString *vendorName = [bet objectAtIndex:2];
    NSString *selectedTeam = [bet objectAtIndex:3];
    NSString *stake = [bet objectAtIndex:4];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/bet_service/check_single_bet";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&betuid=%@&vendoruid=%@&vendorname=%@&betteam=%@&stake=%@&userid=%@&password=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], betUID, vendorUID, [vendorName urlEncodeUsingEncoding:NSUTF8StringEncoding], selectedTeam, stake, [user urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)checkCombiBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password
{
    betType = 2;
    operationType = 1;
    
    NSArray *bets = [bet objectAtIndex:0];
    NSString *vendorUID = [bet objectAtIndex:1];
    NSString *vendorName = [bet objectAtIndex:2];
    NSString *odd = [bet objectAtIndex:3];
    NSString *stake = [bet objectAtIndex:4];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:bets options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"json %@", jsonString);
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/bet_service/check_combi_bet";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&bets=%@&vendoruid=%@&vendorname=%@&odd=%@&stake=%@&userid=%@&password=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], jsonString, vendorUID, vendorName, odd, stake, user, password);
    NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[postData length]];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [urlRequest setHTTPBody:postData];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)placeSingleBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password
{
    betType = 1;
    operationType = 2;
    
    NSString *betUID = [bet objectAtIndex:0];
    NSString *vendorUID = [bet objectAtIndex:1];
    NSString *vendorName = [bet objectAtIndex:2];
    NSString *selectedTeam = [bet objectAtIndex:3];
    NSString *stake = [bet objectAtIndex:4];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/bet_service/place_single_bet";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&betuid=%@&vendoruid=%@&vendorname=%@&betteam=%@&stake=%@&userid=%@&password=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], betUID, vendorUID, [vendorName urlEncodeUsingEncoding:NSUTF8StringEncoding], selectedTeam, stake, [user urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)placeCombiBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password
{
    betType = 2;
    operationType = 2;
    
    NSArray *bets = [bet objectAtIndex:0];
    NSArray *formData = [bet objectAtIndex:1];
    NSString *vendorUID = [bet objectAtIndex:2];
    NSString *vendorName = [bet objectAtIndex:3];
    NSString *odd = [bet objectAtIndex:4];
    NSString *stake = [bet objectAtIndex:5];
    
    NSError *error = nil;
    NSData *jsonDataBets = [NSJSONSerialization dataWithJSONObject:bets options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonStringBets = [[NSString alloc] initWithData:jsonDataBets encoding:NSUTF8StringEncoding];
    
    NSData *jsonDataFormData = [NSJSONSerialization dataWithJSONObject:formData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonStringFormData = [[NSString alloc] initWithData:jsonDataFormData encoding:NSUTF8StringEncoding];
    
//    NSLog(@"json %@", jsonString);
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/bet_service/place_combi_bet";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&bets=%@&formdata=%@&vendoruid=%@&vendorname=%@&odd=%@&stake=%@&userid=%@&password=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], jsonStringBets, jsonStringFormData, vendorUID, vendorName, odd, stake, user, password);
    NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[postData length]];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [urlRequest setHTTPBody:postData];
    
    NSLog(@"%@", postString);
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonResult = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"response from the server = %@", jsonResult);
    
    [self commitData:jsonResult];
}

-(void)commitData:(NSArray *)result
{
    if (operationType == 1)
    {
        [self.delegate betCheckedWithResult:result forBetOfType:betType];
    }
    else if (operationType == 2)
    {
        [self.delegate betPlacedWithResult:result forBetOfType:betType];
    }
}

@end