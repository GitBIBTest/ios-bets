//
//  PlaceBetsService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/04/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VendorBetsModel.h"
#import "VendorBetsObject.h"
#import "Utils.h"
#import "NSString+UrlEncoding.h"
#import "Macros.h"

@protocol PlaceBetsServiceProtocol <NSObject>

- (void)betCheckedWithResult:(NSArray*)betCheckResult forBetOfType:(NSInteger)betType;
- (void)betPlacedWithResult:(NSArray*)betPlaceResult forBetOfType:(NSInteger)betType;

@end

@interface PlaceBetsService : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, weak) id <PlaceBetsServiceProtocol> delegate;

-(void)checkSingleBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password;
-(void)checkCombiBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password;
-(void)placeSingleBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password;
-(void)placeCombiBet:(NSArray *)bet forUser:(NSString *)user andPassword:(NSString *)password;

@end
