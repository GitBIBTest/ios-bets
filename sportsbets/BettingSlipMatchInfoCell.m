//
//  BettingSlipMatchInfoCell.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 11/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "BettingSlipMatchInfoCell.h"

@implementation BettingSlipMatchInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat vendorNameWidth = screenWidth/2.0;
        CGFloat vendorRateWidth = 120.0;
        
        self.teamNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 15.0, vendorNameWidth, 17.0)];
        [self.teamNameLabel setFont:[UIFont fontWithName:@"Montserrat-bold" size:14.0]];
        self.matchDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth - 10.0 - vendorRateWidth, 11.0, vendorRateWidth, 24.0)];
        [self.matchDateLabel setFont:[UIFont fontWithName:@"Montserrat" size:11.0]];
        [self.matchDateLabel setTextAlignment:NSTextAlignmentRight];
        
        //show custom separator line
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, self.frame.size.height-1.0, screenWidth - 20.0, 1.0/[UIScreen mainScreen].scale)];
        separatorLineView.backgroundColor = [UIColor lightGrayColor];
        
        [self addSubview:self.teamNameLabel];
        [self addSubview:self.matchDateLabel];
        [self addSubview:separatorLineView];
    }
    return self;
}

@end
