//
//  ChangeLimitsVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "ChangeLimitsVC.h"

@implementation ChangeLimitsVC
{
    MBProgressHUD *progressHud;
    UserData *userData;
    ChangeLimitsService *changeLimitsService;
}

@synthesize monthLimitTextField, betLimitTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    //place the done button on the "month limit" textfield
    UIToolbar *toolBarMonthLimitTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarMonthLimitTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *monthLimitTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                   target:self
                                                                                                   action:@selector(monthLimitTextFieldDoneTouched:)];
    [toolBarMonthLimitTextField setItems:[NSArray arrayWithObjects:
                                          [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                        target:nil
                                                                                        action:nil],
                                          monthLimitTextFieldDoneButton, nil]];
    monthLimitTextField.inputAccessoryView = toolBarMonthLimitTextField;
    
    //place the done button on the "bet limit" textfield
    UIToolbar *toolBarBetLimitTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarBetLimitTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *betLimitTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                   target:self
                                                                                                   action:@selector(betLimitTextFieldDoneTouched:)];
    [toolBarBetLimitTextField setItems:[NSArray arrayWithObjects:
                                          [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                        target:nil
                                                                                        action:nil],
                                          betLimitTextFieldDoneButton, nil]];
    betLimitTextField.inputAccessoryView = toolBarBetLimitTextField;
    
    //configure labels and other views
    self.monthLimitLabel.text = LocalizedString(@"change_monthly_limit", nil);
    self.betLimitLabel.text = LocalizedString(@"change_bet_limit", nil);
    [self.confirmButton setTitle:LocalizedString(@"confirm", nil) forState:UIControlStateNormal];
    self.adviceLabel.text = LocalizedString(@"change_limit_advice", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    userData = [UserData getInstance];
    
    changeLimitsService = [[ChangeLimitsService alloc] init];
    changeLimitsService.delegate = self;
    [changeLimitsService downloadCurrentLimitsForUser:userData.userId andPassword:userData.passwordEncrypted];
    
    progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [progressHud hide:YES];
}

- (void)limitsDownloaded:(NSArray *)currentLimits
{
    [progressHud hide:YES];
    
//    NSString *lastChangeString = @"2015-12-07";
//    
//    NSDate *todayDate = [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
//    NSDate *lastChangeDate = [dateFormatter dateFromString:lastChangeString];
//    changePossibleDate = [lastChangeDate dateByAddingTimeInterval:(60*60*24)*30];//30 days
//    if ([todayDate compare:changePossibleDate] != -1)
//    {
//        changeIsPossible = YES;
//    }
//    else
//    {
//        changeIsPossible = NO;
//    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setGroupingSeparator:[[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator]];

    NSString *currency = @"€";
    
    currentMonthLimit = F(@"%.2f", [[[currentLimits objectAtIndex:0] valueForKey:@"bets_limit_month"] floatValue]);
    currentBetLimit = F(@"%.2f", [[[currentLimits objectAtIndex:0] valueForKey:@"bets_limit_bet"] floatValue]);
    
    maxPermittedMonthLimit = F(@"%.2f", [[[currentLimits objectAtIndex:0] valueForKey:@"max_bets_limit_month"] floatValue]);
    maxPermittedBetLimit = F(@"%.2f", [[[currentLimits objectAtIndex:0] valueForKey:@"max_bets_limit_bet"] floatValue]);
    
    monthLimitTextField.placeholder = F(@"%@ %@ %@", LocalizedString(@"current_limit", nil), [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[currentMonthLimit floatValue]]], currency);
    betLimitTextField.placeholder = F(@"%@ %@ %@", LocalizedString(@"current_limit", nil), [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[currentBetLimit floatValue]]], currency);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

//------------------------------------------------------------------------------------
//Actions for buttons and misc functions
//------------------------------------------------------------------------------------

- (void)monthLimitTextFieldDoneTouched:(UIBarButtonItem *)sender
{
    //Convert whatever is written into a floatvalue with 2 decimal places
    monthLimitTextField.text = F(@"%.2f", [monthLimitTextField.text floatValue]);
    [self textFieldShouldReturn:monthLimitTextField];
}

- (void)betLimitTextFieldDoneTouched:(UIBarButtonItem *)sender
{
    //Convert whatever is written into a floatvalue with 2 decimal places
    betLimitTextField.text = F(@"%.2f", [betLimitTextField.text floatValue]);
    [self textFieldShouldReturn:betLimitTextField];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

//if the user taps the other textfield
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self sanitizeTextFields:textField];
    return YES;
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self sanitizeTextFields:sender];
    [self.view endEditing:YES];
}

- (void)sanitizeTextFields:(id)sender
{
    if ([monthLimitTextField isFirstResponder])
    {
        monthLimitTextField.text = F(@"%.2f", [monthLimitTextField.text floatValue]);
        if ([monthLimitTextField.text floatValue] == 0.0)
        {
            monthLimitTextField.text = @"";
        }
    }
    else if ([betLimitTextField isFirstResponder])
    {
        betLimitTextField.text = F(@"%.2f", [betLimitTextField.text floatValue]);
        if ([betLimitTextField.text floatValue] == 0.0)
        {
            betLimitTextField.text = @"";
        }
    }
}

- (IBAction)confirmButtonPressed:(id)sender
{
    CGFloat chosenMonthLimit = [monthLimitTextField.text floatValue];
    CGFloat chosenBetLimit = [betLimitTextField.text floatValue];
    NSMutableString *chosenMonthLimitString = [NSMutableString stringWithString:F(@"%.2f", chosenMonthLimit)];
    NSMutableString *chosenBetLimitString = [NSMutableString stringWithString:F(@"%.2f", chosenBetLimit)];
    
    if (chosenMonthLimit == 0.0 || chosenMonthLimit == [currentMonthLimit floatValue])
    {
        chosenMonthLimit = [currentMonthLimit floatValue];
        chosenMonthLimitString = [NSMutableString stringWithString:@"unchanged"];
    }
    if (chosenBetLimit == 0.0 || chosenBetLimit == [currentBetLimit floatValue])
    {
        chosenBetLimit = [currentBetLimit floatValue];
        chosenBetLimitString = [NSMutableString stringWithString:@"unchanged"];
    }
    if ([chosenMonthLimitString isEqualToString:@"unchanged"] && [chosenBetLimitString isEqualToString:@"unchanged"])
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"invalid input", nil)
                                              message:@"Please write valid limit values, and different from the current ones"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (chosenMonthLimit > [maxPermittedMonthLimit floatValue] || chosenBetLimit > [maxPermittedBetLimit floatValue])
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Invalid input", nil)
                                              message:@"You can't raise the limits this much without proof of liquidation"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else //if we're good to go
    {
        progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [changeLimitsService updateLimitsWithMonthLimit:chosenMonthLimitString andBetLimit:chosenBetLimitString ForUser:userData.userId andPassword:userData.passwordEncrypted];
    }
}

- (void)limitsChangeConfirmed:(NSArray *)results
{
    NSString *monthResult = [[results objectAtIndex:0] valueForKey:@"month"];
    NSString *betResult = [[results objectAtIndex:0] valueForKey:@"bet"];
    
    NSLog(@"3: %@ , %@", monthResult, betResult);
    
    [progressHud hide:YES];
    
    UIAlertController *alertController;
    UIAlertAction *cancelAction;
    
    //in case everything was fine
    if (([monthResult isEqualToString:@"true"] && [betResult isEqualToString:@"true"]) ||
        ([monthResult isEqualToString:@"true"] && [betResult isEqualToString:@"unchanged"]) ||
        ([monthResult isEqualToString:@"unchanged"] && [betResult isEqualToString:@"true"]))
    {
        alertController = [UIAlertController
                           alertControllerWithTitle:LocalizedString(@"Limits updated", nil)
                           message:@"Limits successfully updated!"
                           preferredStyle:UIAlertControllerStyleAlert];
        
        cancelAction = [UIAlertAction
                        actionWithTitle:LocalizedString(@"Ok", nil)
                        style:UIAlertActionStyleCancel
                        handler:^(UIAlertAction *action)
                        {
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
    }
    else
    {
        //in case everything went bad
        if (([monthResult isEqualToString:@"false"] && [betResult isEqualToString:@"false"]) ||
             ([monthResult isEqualToString:@"false"] && [betResult isEqualToString:@"unchanged"]) ||
             ([monthResult isEqualToString:@"unchanged"] && [betResult isEqualToString:@"false"]))
        {
            alertController = [UIAlertController
                               alertControllerWithTitle:LocalizedString(@"Error", nil)
                               message:@"Something went wrong, please retry later."
                               preferredStyle:UIAlertControllerStyleAlert];
        }
        //in case month worked and bet didn't
        else if ([monthResult isEqualToString:@"true"] && [betResult isEqualToString:@"false"])
        {
            alertController = [UIAlertController
                               alertControllerWithTitle:LocalizedString(@"Error", nil)
                               message:@"Month limit updated. There was an error updating the limit per bet. Please try again later."
                               preferredStyle:UIAlertControllerStyleAlert];
        }
        //in case bet worked and month didn't
        else if ([monthResult isEqualToString:@"false"] && [betResult isEqualToString:@"true"])
        {
            alertController = [UIAlertController
                               alertControllerWithTitle:LocalizedString(@"Error", nil)
                               message:@"Bet limit updated. There was an error updating the limit per month. Please try again later."
                               preferredStyle:UIAlertControllerStyleAlert];
        }
        cancelAction = [UIAlertAction
                        actionWithTitle:LocalizedString(@"Ok", nil)
                        style:UIAlertActionStyleCancel
                        handler:^(UIAlertAction *action)
                        {
                        }];
    }
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end