//
//  CreditCardsVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "AppDelegate.h"
#import "UserData.h"
#import "CreditCardCell.h"
#import "ConfirmButtonCell.h"
#import "RemoveCreditCardVC.h"

@interface CreditCardsVC : UITableViewController

@end
