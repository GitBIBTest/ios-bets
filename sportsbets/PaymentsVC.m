//
//  PaymentsVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "PaymentsVC.h"

@implementation PaymentsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.depositNowLabel.text = LocalizedString(@"deposit_now", nil);
    self.depositHistoryLabel.text = LocalizedString(@"deposit_history", nil);
    self.cashOutLabel.text = LocalizedString(@"cash_out", nil);
    self.cashOutHistoryLabel.text = LocalizedString(@"cash_out_history", nil);
    self.changeLimitsLabel.text = LocalizedString(@"change_limits", nil);
    self.myCardsLabel.text = LocalizedString(@"my_cards", nil);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:F(@"payments%d", (int)indexPath.row) sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 12;
    }
    else
    {
        return 44;
    }
}

//return to root view controller if I was in this menu and a log out occured
- (void) returnToRoot {
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
