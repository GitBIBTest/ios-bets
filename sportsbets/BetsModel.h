//
//  BetstModel.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>

// contains all properties for bets
@interface BetsModel : NSObject

@property (strong,nonatomic) NSString *category_name;
@property (strong,nonatomic) NSString *category_uid;
@property (strong,nonatomic) NSString *tournament_name;
@property (strong,nonatomic) NSString *tournament_uid;
@property (strong,nonatomic) NSString *match_uid;
@property (strong,nonatomic) NSString *bets_uid;
@property (strong,nonatomic) NSString *date;
@property (strong,nonatomic) NSString *hometeam;
@property (strong,nonatomic) NSString *awayteam;
@property (strong,nonatomic) NSString *rate1;
@property (strong,nonatomic) NSString *ratex;
@property (strong,nonatomic) NSString *rate2;

@end