//
//  BetStakeCell.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 09/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "Localization.h"

@interface BetStakeCell : UITableViewCell

@property (nonatomic,retain) UITextField *stakeTextField;
@property (nonatomic,retain) UILabel *taxesLabel;

- (void)setTaxes;

@end
