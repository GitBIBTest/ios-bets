//
//  BetsService.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Localization.h"
#import "BetsModel.h"
#import "BetsObject.h"
#import "Macros.h"

@protocol BetsServiceProtocol <NSObject>

- (void)betsDownloaded;

@end


@interface BetsService : NSObject <NSURLConnectionDataDelegate>
{
    int sportidentifier;
}

@property (nonatomic, weak) id <BetsServiceProtocol> delegate;
@property (nonatomic, assign) NSInteger numberOfMatches;

-(void)loadBetsForSport:(int)sportid;
-(void)loadBetsForSport:(int)sportid andForCategory:(NSString *)category;
-(void)loadBetsForSport:(int)sportid andForTournament:(NSString *)tournament inCategory:(NSString *)category;

@end
