//
//  RegisterService.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 26/11/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "RegisterService.h"

//TODO deep linking needed for redirecting the user to the app after the registration process is finished

@interface RegisterService ()
{
    NSMutableData *_downloadedData;
    NSString *currentOperation;
}

@end

@implementation RegisterService

-(void)registerUserWithUserData:(NSArray *)registerUserData
{
    currentOperation = @"registration";
    
    NSString *email = [[registerUserData objectAtIndex:0] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *firstName = [[registerUserData objectAtIndex:1] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *lastName = [[registerUserData objectAtIndex:2] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneNumber = [[registerUserData objectAtIndex:3] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *birthdayDay = [[registerUserData objectAtIndex:4] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *birthdayMonth = [[registerUserData objectAtIndex:5] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *birthdayYear = [[registerUserData objectAtIndex:6] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *addressCity = [[registerUserData objectAtIndex:7] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *addressStreet = [[registerUserData objectAtIndex:8] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *addressHouseNr = [[registerUserData objectAtIndex:9] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *addressZip = [[registerUserData objectAtIndex:10] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/registration/register";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&email=%@&firstname=%@&lastname=%@&phone=%@&birthday_day=%@&birthday_month=%@&birthday_year=%@&city=%@&street=%@&house=%@&zip=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], [email urlEncodeUsingEncoding:NSUTF8StringEncoding], firstName, lastName, phoneNumber, birthdayDay, birthdayMonth, birthdayYear, addressCity, addressStreet, addressHouseNr, addressZip);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)finalizeUserWithUsername:(NSString *)username andPassword:(NSString *)password regHash:(NSString *)regHash
{
    currentOperation = @"finalization";
    
    NSString *url = @"http://dev.betitbest.com/bets/en/api/registration/finalize";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&username=%@&password=%@&hash=%@", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], [username urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], [regHash urlEncodeUsingEncoding:NSUTF8StringEncoding]);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"postString = %@", postString);
    
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    NSLog(@"%@", topLevelArray);
    
    if ([currentOperation isEqualToString:@"registration"])
    {
        [self.delegate registerOutputReceived:topLevelArray];
    }
    else if ([currentOperation isEqualToString:@"finalization"])
    {
        [self.delegate finalizationOutputReceived:topLevelArray];
    }
}
@end
