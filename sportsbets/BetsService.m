//
//  BetsService.m
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 25.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "BetsService.h"

@implementation BetsService
{
    BOOL orderByTournament;
    NSMutableData *_downloadedData;
    NSDateFormatter *_dateFormatter;
}

- (id)init
{
    if (!self)
    {
        self = [super init];
    }
    
    //sets date format 	corresponding to language
    NSString * formatted_date = LocalizedString(@"date", nil);
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:formatted_date];
    
    if (self.numberOfMatches == 0)
    {
        self.numberOfMatches = 10;
    }
    
    return self;
}

-(void)loadBetsForSport:(int)sportid
{
    orderByTournament = NO;
    
    sportidentifier = sportid;
    NSURL *jsonFileUrl;

    jsonFileUrl = [NSURL URLWithString:F(@"http://dev.betitbest.com/bets/en/bets/get_matches/%d/%d",sportidentifier, (int)self.numberOfMatches)];
        
    // Create the request
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:jsonFileUrl cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadBetsForSport:(int)sportid andForCategory:(NSString *)category
{
    orderByTournament = YES;
    
    sportidentifier = sportid;
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"http://dev.betitbest.com/bets/en/bets/get_matches/%d-%@/%d",sportidentifier, category, (int)self.numberOfMatches)];
//    NSLog(@"url bets category = %@", jsonFileUrl);
    // Create the request
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:jsonFileUrl cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void)loadBetsForSport:(int)sportid andForTournament:(NSString *)tournament inCategory:(NSString *)category
{
    orderByTournament = YES;
    
    sportidentifier = sportid;
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"http://dev.betitbest.com/bets/en/bets/get_matches/%d-%@-%@/%d",sportidentifier, category, tournament, (int)self.numberOfMatches)];
//    NSLog(@"url bets tournament = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:jsonFileUrl cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0)
    {
        NSLog(@"received authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:@"betitbest"
                                                                    password:@"SanFrancisco.1904"
                                                                 persistence:NSURLCredentialPersistenceForSession];
        NSLog(@"credential created");
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
        NSLog(@"responded to authentication challenge");
    }
    else
    {
        NSLog(@"previous authentication failure");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSDictionary *contentOfJSON = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    NSArray *loadedBets = [contentOfJSON valueForKey:@"matches"];
    
    if([[BetsObject sharedBetsObject].tournamentArray count] > 0)
    {
        [[BetsObject sharedBetsObject].tournamentArray removeAllObjects];
    }
    
    //find best odds for every matchuid
    
    CGFloat maxRate1 = 0;
    CGFloat maxRateX = 0;
    CGFloat maxRate2 = 0;
    for (int i = 0; i < [loadedBets count]; i++)
    {
        if ([[loadedBets[i] valueForKey:@"bettypeoptions"] isEqualToString:@""] && ([[loadedBets[i] valueForKey:@"bettype"] isEqualToString:@"3W"] || [[loadedBets[i] valueForKey:@"bettype"] isEqualToString:@"2W"]))
        {
            BOOL addBetModel = NO;
            CGFloat currentRate1 = [[loadedBets[i] valueForKey:@"odd1"] floatValue];
            CGFloat currentRateX = [[loadedBets[i] valueForKey:@"oddx"] floatValue];
            CGFloat currentRate2 = [[loadedBets[i] valueForKey:@"odd2"] floatValue];
            NSString *currentUID = [loadedBets[i] valueForKey:@"uid"];
            maxRate1 = MAX(maxRate1, currentRate1);
            maxRateX = MAX(maxRateX, currentRateX);
            maxRate2 = MAX(maxRate2, currentRate2);
            if ( (i == [loadedBets count]-1) || ![currentUID isEqualToString:[loadedBets[i+1] valueForKey:@"uid"]] || ![[loadedBets[i+1] valueForKey:@"bettypeoptions"] isEqualToString:@""])
            {
                addBetModel = YES;
            }
            if (addBetModel)
            {
                NSDictionary *loadedBetsDict = loadedBets[i];
                BetsModel *betsModel = [[BetsModel alloc] init];
                //TODO change importer serviceBets to deliver categoryid
                //distinguish connections for data mapping
                //************
                //betsModel.category_name = loadedBetsDict[@"name"];
                //betsModel.category_uid = loadedBetsDict[@"uid"];
                
                betsModel.date = loadedBetsDict[@"date"];
                betsModel.tournament_name = loadedBetsDict[@"tournamentname"];
                betsModel.tournament_uid = loadedBetsDict[@"tournament_uid"];
                betsModel.category_name = loadedBetsDict[@"categoryname"];
                betsModel.match_uid = currentUID;
                betsModel.bets_uid = loadedBetsDict[@"betuid"];
                betsModel.hometeam = loadedBetsDict[@"team1name"];
                betsModel.awayteam = loadedBetsDict[@"team2name"];
                betsModel.rate1 = F(@"%.2f", maxRate1);
                betsModel.ratex = F(@"%.2f", maxRateX);
                betsModel.rate2 = F(@"%.2f", maxRate2);
                maxRate1 = 0;
                maxRateX = 0;
                maxRate2 = 0;
                
                NSDate *rowdate = [NSDate dateWithTimeIntervalSince1970 :[betsModel.date integerValue]];
                NSString *dateString = [_dateFormatter stringFromDate:rowdate];
                betsModel.date = dateString;
                
                [[BetsObject sharedBetsObject]addBets:betsModel orderByTournament:orderByTournament];
                
//                NSLog(@"%@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@", betsModel.date, betsModel.tournament_name, betsModel.category_name, betsModel.tournament_uid, betsModel.match_uid, betsModel.bets_uid, betsModel.hometeam, betsModel.awayteam, betsModel.rate1, betsModel.ratex, betsModel.rate2);

            }
        }
    }
    
    [self commitData];
}

-(void)commitData
{
    [self.delegate betsDownloaded];
}

@end