//
//  EditCreditCardVC.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 07/01/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "RemoveCreditCardVC.h"

@implementation RemoveCreditCardVC

@synthesize creditCardObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    NSString *encryptedCreditCardNumber = F(@"************%@",[[creditCardObject valueForKey:@"number"] substringFromIndex:12]);
    NSArray *expiration_date = [[creditCardObject valueForKey:@"expiration_date"] componentsSeparatedByString:@" "];
    NSString *expiration_month = [expiration_date objectAtIndex:0];
    NSString *expiration_year = [expiration_date objectAtIndex:1];
    
    self.cardTypeLabel.text = LocalizedString(@"card_type", nil);
    self.cardTypeDisplay.text = [creditCardObject valueForKey:@"type"];
    self.cardNumberLabel.text = LocalizedString(@"card_number", nil);
    self.cardNumberDisplay.text = encryptedCreditCardNumber;
    self.cardCodeLabel.text = LocalizedString(@"card_security_code", nil);
    self.cardCodeDisplay.text = [creditCardObject valueForKey:@"cvc"];
    self.expirationLabel.text = LocalizedString(@"expiration_date", nil);
    self.expirationDisplay.text = F(@"%@ / %@", expiration_month, expiration_year);
    self.cardHolderLabel.text = LocalizedString(@"card_owner", nil);
    self.cardHolderDisplay.text = [creditCardObject valueForKey:@"owner"];
    [self.deleteCreditCardButton setTitle:LocalizedString(@"delete_credit_card", nil) forState:UIControlStateNormal];
}

- (IBAction)deleteCreditCardButtonPressed:(id)sender
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:LocalizedString(@"Are you sure?", nil)
                                          message:@"Do you really want to delete this credit card's data?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:LocalizedString(@"no", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                               }];
    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:LocalizedString(@"yes", nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    NSManagedObject *creditCardToDelete = (NSManagedObject *)creditCardObject;
                                    [self.managedObjectContext deleteObject:creditCardToDelete];
                                    
                                    [self.managedObjectContext save:nil];
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

//------------------------------------------------------------------------------------
// misc functions
//------------------------------------------------------------------------------------

- (CGFloat)widthForTableView
{
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
