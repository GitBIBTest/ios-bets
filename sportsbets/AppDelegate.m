//
//  AppDelegate.m
//  Sports Bets by Bet IT Best
//
//  Created by Dummy on 23.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.idleTimer = [[NSTimer alloc] init];
    self.maxIdleTime = 30;

    //make the letters in the status bar white
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //configure default user preferences
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *autoLogoutOptions = @[@"15", @"20", @"30", @"45", @"60"];
    
    if ([userDefaults objectForKey:@"AutoLogoutOptions"] == nil)
    {
        [userDefaults setObject:autoLogoutOptions forKey:@"AutoLogoutOptions"];
    }
    if ([userDefaults objectForKey:@"AutoLogoutMinutes"] == nil)
    {
        [userDefaults setObject:[autoLogoutOptions objectAtIndex:0] forKey:@"AutoLogoutMinutes"];
    }
    if ([userDefaults objectForKey:@"OddsDisplay"] == nil)
    {
        [userDefaults setInteger:1 forKey:@"OddsDisplay"];
    }
    if ([userDefaults objectForKey:@"NotificationsType1"] == nil)
    {
        [userDefaults setInteger:1 forKey:@"NotificationsType1"];
    }
    if ([userDefaults objectForKey:@"NotificationsType2"] == nil)
    {
        [userDefaults setInteger:1 forKey:@"NotificationsType2"];
    }

    //auto log out in 15 Minutes
    NSDate *lastTimeEnteredBackground = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"timeEnteredBackground"];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss dd:mm"];

    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];

    NSString *stringFromDate = [formatter stringFromDate:lastTimeEnteredBackground];

    NSLog(@"Entered background: %@", stringFromDate);

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSysTimeChanged:)
                                                 name:NSSystemClockDidChangeNotification
                                               object:nil];
    
    self.showBurgerMenu = NO;
    return YES;
}

//method for the deep linking from the mail app
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"Launched with URL: %@", url.absoluteString);
    
    NSDictionary *userDict = [[Utils alloc] urlPathToDictionary:url.absoluteString];
    
    NSLog(@"%@", userDict);
    
    if ([[userDict valueForKey:@"function"] isEqualToString:@"registration_confirm"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"registration_confirm_dictionary"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RegistrationMailConfirmClicked" object:self];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    self.timeEnteredBackground = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:self.timeEnteredBackground forKey:@"timeEnteredBackground"];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    self.showBurgerMenu = YES;
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.betitbest.sportsbets" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"sportsbets" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"sportsbets.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Methods for auto log out //subclassin of AppDelegate required and this messes stuff up

//- (void)sendEvent:(UIEvent *)event {
//    [super sendEvent:event];
//
//    // Only want to reset the timer on a Began touch or an Ended touch, to reduce the number of timer resets.
//    NSSet *allTouches = [event allTouches];
//    if ([allTouches count] > 0) {
//        // allTouches count only ever seems to be 1, so anyObject works here.
//        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
//        if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
//            [self resetIdleTimer];
//    }
//}
//
//- (void)resetIdleTimer {
//    if (self.idleTimer) {
//        [self.idleTimer invalidate];
//
//    }
//
//    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:self.maxIdleTime target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
//}
//
//- (void)idleTimerExceeded {
//    NSLog(@"idle time exceeded");
//}

//- (void) resetTimer {
//    NSLog(@"Event sent");
//}

-(void) handleSysTimeChanged: (NSNotification*) notification
{
    NSLog(@"time changed");
    //currently used as logout
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Login"];
    NSArray *login = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];

    if (login.count != 0)
    {
        NSLog(@"logging out");
        [self.managedObjectContext deleteObject:login[0]];
        [self.managedObjectContext save:nil];
        [UserData resetUserData];
    }
    else
    {
        NSLog(@"error: user was already logged out");
    }
    //[[self navigationController] popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"time log out" object:nil];
}

@end
