//
//  Vendor.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 22/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vendor : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *imageURL;
@property BOOL blacklisted;

@end
