//
//  ChangeLimitsService.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 23/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@protocol ChangeLimitsServiceProtocol <NSObject>

-(void)limitsDownloaded:(NSArray *)currentLimits;
-(void)limitsChangeConfirmed:(NSArray *)success;

@end

@interface ChangeLimitsService : NSObject

@property (nonatomic,retain) id <ChangeLimitsServiceProtocol> delegate;

-(void)downloadCurrentLimitsForUser:(NSString *)user andPassword:(NSString *)password;
-(void)updateLimitsWithMonthLimit:(NSString *)monthLimit andBetLimit:(NSString *)betLimit ForUser:(NSString *)user andPassword:(NSString *)password;

@end
