//
//  AppDelegate.h
//  Sports Bets by Bet IT Best
//
//  Created by Dummy on 23.11.15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
// the place to be

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CategoryTournamentService.h"
#import "UserData.h"
#import "Utils.h"
#import "LoginVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property BOOL showBurgerMenu;

@property (strong,nonatomic) NSTimer* idleTimer;
@property NSTimeInterval maxIdleTime;
@property (strong,nonatomic) NSDate* timeEnteredBackground;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

