//
//  CategoryTournamentService.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 26.11.15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Localization.h"

@protocol CategoryTournamentServiceProtocol <NSObject>

- (void)categoriesTournamentsDownloaded;

@end

@interface CategoryTournamentService : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic,strong) NSFileHandle *fileHandle;
@property (nonatomic, weak) id <CategoryTournamentServiceProtocol> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSURLConnection *sportConnection;
@property (strong, nonatomic) NSURLConnection *categoryConnection;
@property (strong, nonatomic) NSURLConnection *tournamentConnection;

-(void)downloadSport;

@end

