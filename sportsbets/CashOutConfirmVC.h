//
//  CashOutConfirmVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 17/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "Localization.h"
#import "Macros.h"

@interface CashOutConfirmVC : UITableViewController

@property (nonatomic,retain) IBOutlet UILabel *overviewLabel;
@property (nonatomic,retain) IBOutlet UILabel *amountLabel;
@property (nonatomic,retain) IBOutlet UILabel *amountDisplay;

@property (nonatomic,retain) IBOutlet UILabel *ibanDisplay;
@property (nonatomic,retain) IBOutlet UILabel *bicDisplay;
@property (nonatomic,retain) IBOutlet UILabel *accountHolderLabel;
@property (nonatomic,retain) IBOutlet UILabel *accountHolderDisplay;
@property (nonatomic,retain) IBOutlet UIButton *transferConfirmButton;

@property (nonatomic) NSArray *passedData;

@end
