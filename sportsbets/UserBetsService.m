//
//  UserBetsService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserBetsService.h"

#define BET_TYPE_SINGLE @"1"
#define BET_TYPE_COMBI @"2"

@implementation UserBetsService
{
    NSMutableData *_downloadedData;
    NSDateFormatter *_dateFormatter;
}

- (id)init
{
    if (!self)
    {
        self = [super init];
    }
    
    //sets date format 	corresponding to language
    NSString * formatted_date = LocalizedString(@"date", nil);
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:formatted_date];
    
    return self;}

- (void)loadCurrentBetsForUser:(NSString *)user andPassword:(NSString *)password limitTo:(NSInteger)userBetsToLoad
{
    NSString *url = @"http://dev.betitbest.com/bets/en/api/app_get_user_bets/get_user_bets";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&userid=%@&password=%@&betfilter=pending&limit=%d", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], [user urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], (int)userBetsToLoad);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)loadMyBetsForUser:(NSString *)user andPassword:(NSString *)password limitTo:(NSInteger)userBetsToLoad
{
    NSString *url = @"http://dev.betitbest.com/bets/en/api/app_get_user_bets/get_user_bets";
    NSURL *jsonFileUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:jsonFileUrl
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    NSString *postString = F(@"apikey=%@&userid=%@&password=%@&limit=%d", [IOS_DEFINED_APIKEY urlEncodeUsingEncoding:NSUTF8StringEncoding], [user urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], (int)userBetsToLoad);
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    if([[UserBetsObject sharedUserBetsObject].userBetsDataArray count] > 0 && [[topLevelArray valueForKey:@"status"] isEqualToString:@"success"])
    {
        [[UserBetsObject sharedUserBetsObject].userBetsDataArray removeAllObjects];
    }
    
    NSLog(@"%@", topLevelArray);
    
    UserBetsData *userBetsData = [[UserBetsData alloc] init];
    
    NSArray *downloadedBetsArray = [topLevelArray valueForKey:@"bets"];
    
    for (int i = 0; i < [downloadedBetsArray count]; i++)
    {
        NSDictionary *loadedBetsDict = [downloadedBetsArray objectAtIndex:i];
        UserBetsModel *userBetsModel = [[UserBetsModel alloc] init];
        
        NSString *currentUID = loadedBetsDict[@"uid"];
        NSString *currentBetType = loadedBetsDict[@"type_uid"];
        NSString *currentUserBet = [loadedBetsDict[@"user_bet"] isEqualToString:@"x"] ? @"0" : loadedBetsDict[@"user_bet"];
        
        userBetsModel.bet_sport = loadedBetsDict[@"sport_name"];
        userBetsModel.bet_sport_uid = loadedBetsDict[@"sport_uid"];
        userBetsModel.vendor = loadedBetsDict[@"vendor_name"];
        userBetsModel.vendor_uid = loadedBetsDict[@"vendor_uid"];
        userBetsModel.bet_type = currentBetType;
        userBetsModel.bet_status = loadedBetsDict[@"status"]; //"1" for pending bets, "red" for lost and "green" for won
        userBetsModel.uid = currentUID;
        userBetsModel.playerbet_uid = loadedBetsDict[@"playerbet_uid"];;
        userBetsModel.bet_datetime_placed = loadedBetsDict[@"timestamp"];
        userBetsModel.match_datetime = loadedBetsDict[@"match_date"];
        userBetsModel.bet_team1 = loadedBetsDict[@"team1_name"];
        userBetsModel.bet_team2 = loadedBetsDict[@"team2_name"];
        userBetsModel.bet_winner_team = currentUserBet;
        userBetsModel.rate = loadedBetsDict[F(@"rate_%@",loadedBetsDict[@"user_bet"])];
        userBetsModel.stake = loadedBetsDict[@"stake"];
        userBetsModel.possible_win = loadedBetsDict[@"sum"];
        userBetsModel.bet_result = loadedBetsDict[@"result"];
        
        NSDate *rowdate = [NSDate dateWithTimeIntervalSince1970:[userBetsModel.bet_datetime_placed integerValue]];
        NSString *dateString = [_dateFormatter stringFromDate:rowdate];
        userBetsModel.bet_datetime_placed = dateString;
        
        [userBetsData addUserBet:userBetsModel];
        
        if (i == [downloadedBetsArray count] - 1 || ![currentUID isEqualToString:[[downloadedBetsArray objectAtIndex:i+1] valueForKey:@"uid"]]
            || [currentBetType isEqualToString:BET_TYPE_SINGLE] || [[[downloadedBetsArray objectAtIndex:i+1] valueForKey:@"type_uid"] isEqualToString:BET_TYPE_SINGLE])
        {
            [[UserBetsObject sharedUserBetsObject]addUserBetsArray:userBetsData];
            
            userBetsData = [[UserBetsData alloc] init];
        }
    }
    [self.delegate userBetsDownloaded];
}

@end
