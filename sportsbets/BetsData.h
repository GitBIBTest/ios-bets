//
//  BetstData.h
//  Sports Bets by Bet IT Best
//
//  Created by Annegret Ramp on 26/11/15.
//  Copyright (c) 2015 BetITBest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BetsModel.h"
#import "TournamentModel.h"

@interface BetsData : NSObject

@property (strong,nonatomic) NSMutableArray *betsArray;
@property (strong,nonatomic) NSMutableArray *tournamentArray;

-(void)addBets:(BetsModel *)betsData orderByTournament:(BOOL)orderByTournament;
-(void)addBets:(BetsModel *)betsData toTournament:(NSString *)tournamentid;
-(void)addTournament:(NSString *)tournamentid andTournamentName:(NSString *)tournamentname forCategoryName:(NSString *)categoryname;

@end