//
//  RegisterVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 26/11/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

/*--------------------------------------------------------------------------------
 TODO WHEN THE API IS DONE
 - Send the data to the server and ask for a confirmation of the registration
 - Encryption will most likely happen at server-side
 --------------------------------------------------------------------------------*/

#import "RegisterVC.h"

@implementation RegisterVC

@synthesize nameTextField, surnameTextField, birthdateDayTextField, birthdateMonthTextField, birthdateYearTextField, address_streetTextField, address_housenrTextField, address_plzTextField, address_cityTextField, emailTextField, phoneNumberTextField, enroll;

-(void)viewDidLoad
{
    //gesture recognizer to resignFirstResponder when there's a touch outside the keyboard
    UITapGestureRecognizer *tappedOut = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOutside:)];
    [self.tableView addGestureRecognizer:tappedOut];
    
    //load image logo
    //self.navigationItem.titleView = UIImageNavigationLogo;
    
    //place the done button on the "year of birth" textfield
    UIToolbar *toolBarBirthdateYearTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarBirthdateYearTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *birthdateYearTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                               target:self
                                                                                               action:@selector(birthdateYearTextFieldDoneTouched:)];
    [toolBarBirthdateYearTextField setItems:[NSArray arrayWithObjects:
                                      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                    target:nil
                                                                                    action:nil],
                                      birthdateYearTextFieldDoneButton, nil]];
    birthdateYearTextField.inputAccessoryView = toolBarBirthdateYearTextField;
    
    //place the done button on the "phone number" textfield
    UIToolbar *toolBarPhoneNumberTextField = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, WIDTH(self.view), 44)];
    toolBarPhoneNumberTextField.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *phoneNumberTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                                      target:self
                                                                                                      action:@selector(phoneNumberTextFieldDoneTouched:)];
    [toolBarPhoneNumberTextField setItems:[NSArray arrayWithObjects:
                                             [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                           target:nil
                                                                                           action:nil],
                                             phoneNumberTextFieldDoneButton, nil]];
    phoneNumberTextField.inputAccessoryView = toolBarPhoneNumberTextField;
    
    // initialization of labels and text fields
    self.registerLabel.text = LocalizedString(@"register", nil);
    self.nameLabel.text = LocalizedString(@"name", nil);
    nameTextField.placeholder = LocalizedString(@"name", nil);
    self.surnameLabel.text = LocalizedString(@"surname", nil);
    surnameTextField.placeholder = LocalizedString(@"surname", nil);
    self.birthdateLabel.text = LocalizedString(@"birthdate", nil);
    birthdateDayTextField.placeholder = LocalizedString(@"dd", nil);
    birthdateMonthTextField.placeholder = LocalizedString(@"mm", nil);
    birthdateYearTextField.placeholder = LocalizedString(@"yyyy", nil);
    self.address_streetLabel.text = F(@"%@, %@", LocalizedString(@"address_street", nil), LocalizedString(@"address_housenr", nil));
    address_streetTextField.placeholder = LocalizedString(@"address_street", nil);
    address_housenrTextField.placeholder = LocalizedString(@"address_housenr", nil);
    self.address_plzLabel.text = LocalizedString(@"address_plz", nil);
    address_plzTextField.placeholder = LocalizedString(@"address_plz", nil);
    self.address_cityLabel.text = LocalizedString(@"address_city", nil);
    address_cityTextField.placeholder = LocalizedString(@"address_city", nil);
    self.emailLabel.text = LocalizedString(@"email", nil);
    emailTextField.placeholder = LocalizedString(@"email", nil);
    self.phoneNumberLabel.text = LocalizedString(@"phone_number", nil);
    phoneNumberTextField.placeholder = LocalizedString(@"phone_number", nil);
    [self.enroll setTitle:LocalizedString(@"enroll", nil) forState:UIControlStateNormal];
}

-(IBAction)enrollClicked
{
    [self.view endEditing:YES];
    
    BOOL showAlert = NO;
    BOOL showUserAlert = NO;
    
    //sanitize variables
    nameTextField.text =  [nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    surnameTextField.text = [surnameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    emailTextField.text = [emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    phoneNumberTextField.text = [phoneNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumberTextField.text = [phoneNumberTextField.text stringByReplacingOccurrencesOfString:@"+" withString:@"00"];
    
    //name (no symbols, numbers or punctuation)
    if (nameTextField.text == nil || [nameTextField.text length] == 0 ||
        [nameTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound ||
        [nameTextField.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound ||
        [nameTextField.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.nameLabel.textColor = [UIColor redColor];
        nameTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.nameLabel.textColor = [UIColor blackColor];
    }
    //surname (no symbols, numbers or punctuation)
    if (surnameTextField.text == nil || [surnameTextField.text length] == 0 ||
        [surnameTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound ||
        [surnameTextField.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound ||
        [surnameTextField.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound)
    {
        showAlert = YES;
        self.surnameLabel.textColor = [UIColor redColor];
        surnameTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.surnameLabel.textColor = [UIColor blackColor];
    }
    if (birthdateDayTextField.text == nil || [birthdateDayTextField.text intValue] == 0 ||
        birthdateMonthTextField.text == nil || [birthdateMonthTextField.text intValue] == 0 ||
        birthdateYearTextField.text == nil || [birthdateYearTextField.text intValue] == 0)
    {
        self.birthdateLabel.textColor = [UIColor redColor];
        birthdateDayTextField.textColor = [UIColor redColor];
        birthdateMonthTextField.textColor = [UIColor redColor];
        birthdateYearTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.birthdateLabel.textColor = [UIColor blackColor];
    }
    if (address_streetTextField.text == nil || [address_streetTextField.text length] == 0)
    {
        showAlert = YES;
        self.address_streetLabel.textColor = [UIColor redColor];
        address_streetTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.address_streetLabel.textColor = [UIColor blackColor];
    }
    if (address_housenrTextField.textColor == nil || [address_housenrTextField.text length] == 0)
    {
        showAlert = YES;
        self.address_streetLabel.textColor = [UIColor redColor];
        address_housenrTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.address_streetLabel.textColor = [UIColor blackColor];
    }
    if (address_plzTextField.text == nil || [address_plzTextField.text intValue] == 0)
    {
        showAlert = YES;
        self.address_plzLabel.textColor = [UIColor redColor];
        address_plzTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.address_plzLabel.textColor = [UIColor blackColor];
    }
    if (address_cityTextField.textColor == nil || [address_cityTextField.text length] == 0)
    {
        showAlert = YES;
        self.address_cityLabel.textColor = [UIColor redColor];
        address_cityTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.address_cityLabel.textColor = [UIColor blackColor];
    }
    if (![self validateEmail:emailTextField.text])
    {
        showAlert = YES;
        self.emailLabel.textColor = [UIColor redColor];
        emailTextField.textColor = [UIColor redColor];
    }
    else
    {
        self.emailLabel.textColor = [UIColor blackColor];
    }
    
    if (showAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Incomplete Form", nil)
                                              message:@"Please fill in all fields correctly"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    //time to check if the username is acceptable
    else if (showUserAlert)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Incomplete Form", nil)
                                              message:@"The username can only contain letters, numbers and the following symbols . - _"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        NSString *email = [NSString stringWithString:emailTextField.text];
        NSString *firstName = [NSString stringWithString:nameTextField.text];
        NSString *lastName = [NSString stringWithString:surnameTextField.text];
        NSString *phoneNumber = [NSString stringWithString:phoneNumberTextField.text];
        NSString *birthdayDay = [NSString stringWithString:birthdateDayTextField.text];
        NSString *birthdayMonth = [NSString stringWithString:birthdateMonthTextField.text];
        NSString *birthdayYear = [NSString stringWithString:birthdateYearTextField.text];
        NSString *addressCity = [NSString stringWithString:address_cityTextField.text];
        NSString *addressStreet = [NSString stringWithString:address_streetTextField.text];
        NSString *addressHouseNr = [NSString stringWithString:address_housenrTextField.text];
        NSString *addressZip = [NSString stringWithString:address_plzTextField.text];
        
        NSArray *registerArray = @[email, firstName, lastName, phoneNumber, birthdayDay, birthdayMonth, birthdayYear, addressCity, addressStreet, addressHouseNr, addressZip];
        
        RegisterService *registerService = [[RegisterService alloc] init];
        registerService.delegate = self;
        [registerService registerUserWithUserData:registerArray];
    }
}

- (void)registerOutputReceived:(NSArray *)registerData
{
    if ([[registerData valueForKey:@"status"] isEqualToString:@"success"])
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString([registerData valueForKey:@"message_title"], nil)
                                              message:LocalizedString([registerData valueForKey:@"message"], nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else //if ([[registerData valueForKey:@"status"] isEqualToString:@"error"])
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"registration_error", nil)
                                              message:[registerData valueForKey:@"message"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Ok", nil)
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)birthdateYearTextFieldDoneTouched:(id)sender
{
    [self textFieldShouldReturn:birthdateYearTextField];
}

- (void)phoneNumberTextFieldDoneTouched:(id)sender
{
    [self textFieldShouldReturn:phoneNumberTextField];
}

- (BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
}

//dismiss the keyboard
- (void)tappedOutside:(id)sender
{
    [self.view endEditing:YES];
}

@end