//
//  SupportObject.m
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 25/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "SupportObject.h"

@implementation SupportObject

- (id)initWithUID:(NSString*)uid andName:(NSString*)name
{
    self.categoryUid = uid;
    self.categoryName = name;
    
    return self;
}

@end
