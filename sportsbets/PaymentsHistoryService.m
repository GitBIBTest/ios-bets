//
//  PaymentsHistoryService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 24/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "PaymentsHistoryService.h"

@interface PaymentsHistoryService ()
{
    NSMutableData *_downloadedData;
}

@end

@implementation PaymentsHistoryService

-(void)downloadPaymentHistoryOfType:(NSString *)type ForUser:(NSString *)user andPassword:(NSString *)password
{
    NSString * url = @"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/servicePaymentsHistory.php?userid=";
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"%@%@%@%@%@%@", url, user,@"&password=", password, @"&historytype=", type)];
    NSLog(@"url user info = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    [self.delegate historyDownloaded:topLevelArray];
}

@end
