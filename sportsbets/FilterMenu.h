//
//  FilterMenu.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 03/03/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterMaster.h"
#import "AppDelegate.h"
#import "CategoryTournamentService.h"
#import "Macros.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+UrlEncoding.h"

@interface FilterMenu : UIViewController <UITableViewDataSource, UITableViewDelegate, CategoryTournamentServiceProtocol>

@property (nonatomic,retain) IBOutlet UITableView *tableView;

@end
