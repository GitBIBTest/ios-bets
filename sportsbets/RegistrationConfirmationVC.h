//
//  RegistrationConfirmationViewController.h
//  Sports Betting by Bet IT Best
//
//  Created by Tsole on 7/4/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RegisterService.h"
#import "MBProgressHud.h"
#import "Macros.h"

@interface RegistrationConfirmationVC : UITableViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, RegisterServiceProtocol>

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (strong, nonatomic) IBOutlet UIButton *confirmButton;

@end