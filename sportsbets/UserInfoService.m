//
//  UserInfoService.m
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 18/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import "UserInfoService.h"

@interface UserInfoService ()
{
    NSMutableData *_downloadedData;
}

@end

@implementation UserInfoService

-(void)loadUserInfoForUser:(NSString *)user andForPassword:(NSString *)password
{
    NSString * url = @"http://bets-dev.betitbest.com/pool/MobileAppsBets/PHPImporterBets/serviceUserInfo.php?userid=";
    NSURL *jsonFileUrl = [NSURL URLWithString:F(@"%@%@%@%@", url, user, @"&password=", password)];
    NSLog(@"url user info = %@", jsonFileUrl);
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:jsonFileUrl];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Parse the JSON that came in
    NSError *error;
    NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    if ([topLevelArray count] > 0)
    {
        [self.delegate userInfoDownloaded:topLevelArray[0]];
    }
}

@end
