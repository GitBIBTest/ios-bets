//
//  CashOutVC.h
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"
#import "Macros.h"
#import "CashOutConfirmVC.h"
#import "AccountOverviewService.h"
#import "UserData.h"
#import "MBProgressHUD.h"

@interface CashOutVC : UITableViewController <UITextFieldDelegate, AccountOverviewServiceProtocol>
{
    NSString *userDisposableCash;
}

@property (nonatomic,retain) IBOutlet UILabel *amountLabel;
@property (nonatomic,retain) IBOutlet UITextField *amountTextField;
@property (nonatomic,retain) IBOutlet UILabel *maxLabel;
@property (nonatomic,retain) IBOutlet UILabel *ibanLabel;
@property (nonatomic,retain) IBOutlet UITextField *ibanTextField;
@property (nonatomic,retain) IBOutlet UILabel *bicLabel;
@property (nonatomic,retain) IBOutlet UITextField *bicTextField;
@property (nonatomic,retain) IBOutlet UILabel *accountHolderLabel;
@property (nonatomic,retain) IBOutlet UITextField *accountHolderTextField;
@property (nonatomic,retain) IBOutlet UIButton *continueButton;

@end
