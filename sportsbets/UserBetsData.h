//
//  UserBetsData.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserBetsModel.h"

@interface UserBetsData : NSObject

@property (nonatomic,retain) NSMutableArray *userBetsArray;

-(void)addUserBet:(UserBetsModel *)userBetsData;

@end
