//
//  CreditCardsVC.m
//  Sports Bets by Bet IT Best
//
//  Created by Francesco Cappellotto on 15/12/15.
//  Copyright © 2015 Bet IT Best GmbH. All rights reserved.
//

#import "CreditCardsVC.h"

@implementation CreditCardsVC
{
    NSArray *creditCardsArray;
    NSIndexPath *selectedRowForSegue;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CreditCard"];
    NSString *userName = [UserData getInstance].userName;
    NSError *fetchError = nil;
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat:@"toLogin.username LIKE[c] %@", userName]];
    creditCardsArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    
    //give also 1 row for the confirm button
    NSInteger numberOfCreditCards = [creditCardsArray count];
    if (numberOfCreditCards == 0)
    {
        return 2;
    }
    return numberOfCreditCards + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([creditCardsArray count] == 0)
    {
        if (indexPath.row == 0)
        {
            return 80;
        }
        else
        {
            return 140;
        }
    }
    else if (indexPath.row == [creditCardsArray count])
    {
        //size of the button's cell dependant on how many credit cards there are
        if ([creditCardsArray count] * 10 < 90)
        {
            return 150 - [creditCardsArray count] * 10;
        }
        return 60;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    //Resize width of TableView
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.width = [self widthForTableView];
    self.tableView.frame = tableViewFrame;
    
    if (indexPath.row < [creditCardsArray count])
    {
        cellIdentifier = @"CreditCardCell";
        CreditCardCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

        NSString *creditCardNumber = [[creditCardsArray objectAtIndex:indexPath.row] valueForKey:@"number"];
        NSString *encryptedCreditCardNumber = F(@"************%@", [creditCardNumber substringFromIndex:12]);
        
        cell.creditCardTypeLabel.text = [[creditCardsArray objectAtIndex:indexPath.row] valueForKey:@"type"];
        cell.creditCardNumberLabel.text = encryptedCreditCardNumber;
        
        //show custom separator line
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 43.0, WIDTH(self.view) - 20.0, 1.0 / [UIScreen mainScreen].scale)];
        separatorLineView.backgroundColor = [UIColor lightGrayColor];
        [cell.contentView addSubview:separatorLineView];
        
        return cell;
    }
    //say that there are currently no credit cards
    else if (indexPath.row == 0) // means also that [creditCardsArray count] == 0
    {
        cellIdentifier = @"NoCreditCardCell";
        CreditCardCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        UILabel *noCreditCardLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 20.0, WIDTH(self.view) - 30.0, 50.0)];
        [noCreditCardLabel setFont: [UIFont fontWithName:@"Montserrat" size:17.0]];
        [noCreditCardLabel setTextAlignment:NSTextAlignmentCenter];
        noCreditCardLabel.text = LocalizedString(@"no_credit_card", nil);
        noCreditCardLabel.numberOfLines = 2;
        
        [cell addSubview:noCreditCardLabel];
        
        return cell;
    }
    //add new credit card button
    else
    {
        cellIdentifier = @"ConfirmButtonCell";
        
        ConfirmButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        [cell.confirmButton setTitle:LocalizedString(@"new_credit_card", nil) forState:UIControlStateNormal];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [creditCardsArray count])
    {
        selectedRowForSegue = indexPath;
        [self performSegueWithIdentifier:@"removeCreditCardVC" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"removeCreditCardVC"])
    {
        RemoveCreditCardVC *removeCreditCardVC = [segue destinationViewController];
        removeCreditCardVC.creditCardObject = [creditCardsArray objectAtIndex:selectedRowForSegue.row];
    }
}

//------------------------------------------------------------------------------------
// misc functions
//------------------------------------------------------------------------------------

- (CGFloat)widthForTableView
{
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

//to retrieve managed object context
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end