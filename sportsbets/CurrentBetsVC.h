//
//  CurrentBetsVC.h
//  Sports Betting by Bet IT Best
//
//  Created by Francesco Cappellotto on 25/02/16.
//  Copyright © 2016 Bet IT Best GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UserData.h"
#import "Macros.h"
#import "UserBetsService.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "Reachability.h"

@interface CurrentBetsVC : UITableViewController <UITableViewDataSource, UITableViewDelegate, UserBetsServiceProtocol>

@end
